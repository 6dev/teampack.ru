<?php
/**
 * @author Alexandr Kirshin <kirshin.as@gmail.com>
 */
$APPLICATION->AddViewContent('body-class','title');
$APPLICATION->AddViewContent('top-navi-selected','selected');

include_once __DIR__ . '/../.default/header.php';
define('CATALOG_HIDE', true);
?>


<!-- MAIN -->
<section class="main-block main-bottom clearfix">
    <div class="center-block">
        <div class="content-s">
            <?
            $APPLICATION->IncludeFile('/includes/banners.php', Array(), Array(
                "MODE"      => "php",
                "NAME"      => "Баннеры",
                "TEMPLATE"  => "section_include_template.php"
            ));
            ?>
            <div class="content-block title-hide">