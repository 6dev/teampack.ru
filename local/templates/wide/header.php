<?php
/**
 * @author Alexandr Kirshin <kirshin.as@gmail.com>
 */
include_once __DIR__ . '/../.default/header.php';
?>
<!-- MAIN -->
                <section class="main-block main-bottom clearfix">
                    <div class="left-s">
                        <div class="center-s">
                            <?
                            $APPLICATION->IncludeFile('/includes/banners.php', Array(), Array(
                                "MODE"      => "php",
                                "NAME"      => "Баннеры",
                                "TEMPLATE"  => "section_include_template.php"
                            ));
                            ?>
                            <?$APPLICATION->IncludeComponent('bitrix:breadcrumb', 'top')?>
                            <div class="content-block">
                                <h1><?$APPLICATION->ShowTitle(false)?></h1>