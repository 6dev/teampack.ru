<?php
/**
 * @author Alexandr Kirshin <kirshin.as@gmail.com>
 */
include_once __DIR__ . '/../.default/header.php';
define('LEFTSIDE_HIDE', true);
?>
<!-- MAIN -->
<section class="main-block main-bottom clearfix">
    <div class="center-block">
        <div class="content-s">
            <?
            $APPLICATION->IncludeFile('/includes/banners.php', Array(), Array(
                "MODE"      => "php",
                "NAME"      => "Баннеры",
                "TEMPLATE"  => "section_include_template.php"
            ));
            ?>
            <?$APPLICATION->IncludeComponent('bitrix:breadcrumb', 'top')?>

            <div class="content-block">

                <h1 class="page-name"><?$APPLICATION->ShowTitle(false)?></h1>
