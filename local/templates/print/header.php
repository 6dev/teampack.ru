<?php
/**
 * @author Alexandr Kirshin <kirshin.as@gmail.com>
 */
?>
<html dir="ltr" lang="ru" class="no-js index"><!--<![endif]-->
<head>
    <meta charset="UTF-8"/>
    <meta name="cmsmagazine" content="db7a12ffe11c973bc15984842f82d936" />
    <title><? $APPLICATION->ShowTitle(); ?></title>
    <meta name="viewport" content="width=480">
    <!--[if lt IE 9]>
    <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
    <link href='http://fonts.googleapis.com/css?family=Lobster&subset=latin,cyrillic-ext,cyrillic' rel='stylesheet' type='text/css'>
    <? $APPLICATION->ShowHead(); ?>
    <!--[if lte IE 6]>
    <link rel="stylesheet" type="text/css" href="css/ie6.css"><![endif]-->
    <link rel="shortcut icon" href="/assets/favicon.ico" type="image/x-icon"/>
</head>
<body>