<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
}

/**
 * @var CMain $APPLICATION
 */
IncludeTemplateLangFile(__FILE__);
$APPLICATION->SetAdditionalCSS('/assets/css/reset.css');
$APPLICATION->SetAdditionalCSS('/assets/css/style.css');
$APPLICATION->SetAdditionalCSS('/assets/css/anim.css');
$APPLICATION->SetAdditionalCSS('/assets/css/jquery.countdown.css');
$APPLICATION->SetAdditionalCSS('/assets/css/custom.css');

//$APPLICATION->AddHeadScript('/assets/js/jquery-1.8.3.min.js');
$APPLICATION->AddHeadScript('/assets/js/jquery-1.11.1.min.js');
$APPLICATION->AddHeadScript('/assets/js/jquery-migrate-1.2.1.min.js');
$APPLICATION->AddHeadScript('/assets/js/jquery.countdown.js');
$APPLICATION->AddHeadScript('/assets/js/jquery.plugin.min.js');
$APPLICATION->AddHeadScript('/assets/js/jquery.countdown-ru.js');
$APPLICATION->AddHeadScript('/assets/js/jquery.countdown-ru.js');

$APPLICATION->AddHeadScript('/assets/js/jqueryui/jquery-ui.min.js');
$APPLICATION->SetAdditionalCSS('/assets/js/jqueryui/jquery-ui.min.css');
$APPLICATION->SetAdditionalCSS('/assets/js/jqueryui/jquery-ui.structure.min.css');
$APPLICATION->SetAdditionalCSS('/assets/js/jqueryui/jquery-ui.theme.min.css');

$APPLICATION->AddHeadScript('/assets/js/jquery-ui-1.10.3.custom.min.js');
$APPLICATION->AddHeadScript('/assets/js/sliderValues.js');
$APPLICATION->AddHeadScript('/assets/js/jcarousellite-a14.js');
$APPLICATION->AddHeadScript('/assets/fancyapps/source/jquery.fancybox.pack.js');
$APPLICATION->SetAdditionalCSS('/assets/fancyapps/source/jquery.fancybox.css');
$APPLICATION->AddHeadScript('/assets/js/jquery.mask.min.js');
$APPLICATION->AddHeadScript('/assets/js/jquery.form.min.js');
$APPLICATION->SetAdditionalCSS('/assets/jquery.arcticmodal-0.3/jquery.arcticmodal-0.3.css');
$APPLICATION->AddHeadScript('/assets/jquery.arcticmodal-0.3/jquery.arcticmodal-0.3.min.js');
$APPLICATION->AddHeadScript('/assets/js/validation/jquery.validate.min.js');
$APPLICATION->AddHeadScript('/assets/js/validation/additional-methods.min.js');
$APPLICATION->AddHeadScript('/assets/js/index.js');
$APPLICATION->AddHeadScript('/assets/js/app.js');
$APPLICATION->AddHeadScript('/assets/js/custom.js');
?>
<!doctype html>
<!--[if lt IE 7 ]>
<html lang="en" class="no-js ie6"><![endif]-->
<!--[if IE 7 ]>
<html dir="ltr" lang="ru" class="no-js ie7 index"><![endif]-->
<!--[if IE 8 ]>
<html dir="ltr" lang="ru" class="no-js ie8 index"><![endif]-->
<!--[if IE 9 ]>
<html dir="ltr" lang="ru" class="no-js ie9 index"><![endif]-->
<!--[if (gt IE 9)|!(IE)]><!-->
<html dir="ltr" lang="ru" class="no-js index"><!--<![endif]-->
<head>
    <meta charset="UTF-8"/>
    <meta name="cmsmagazine" content="db7a12ffe11c973bc15984842f82d936" />
    <title><? $APPLICATION->ShowTitle(); ?> | «Тимпак»</title>
    <meta name="viewport" content="width=480">
    <!--[if lt IE 9]>
    <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
    <link href='http://fonts.googleapis.com/css?family=Lobster&subset=latin,cyrillic-ext,cyrillic' rel='stylesheet' type='text/css'>
    <? $APPLICATION->ShowHead(); ?>
    <!--[if lte IE 6]>
    <link rel="stylesheet" type="text/css" href="css/ie6.css"><![endif]-->
    <link rel="shortcut icon" href="/assets/favicon.ico" type="image/x-icon"/>

<link rel="canonical" href="/catalog/novogodnyaya-upakovka-podarkov/tekstilnaya-novogodnyaya-upakovka/">
<link rel="canonical" href="/catalog/novogodnyaya-upakovka-podarkov/novogodnyaya-zhestyanaya-upakovka/">
<link rel="canonical" href="/catalog/novogodnyaya-upakovka-podarkov/kartonnye-novogodnie-tuby/">
<link rel="canonical" href="/catalog/novogodnyaya-upakovka-podarkov/derevo/">
<link rel="canonical" href="/catalog/novogodnyaya-upakovka-podarkov/kartonnaya-novogodnyaya-upakovka/khrom_erzats/">
<link rel="canonical" href="/catalog/novogodnyaya-upakovka-podarkov/kartonnaya-novogodnyaya-upakovka/mikrogofrokarton/">
<link rel="canonical" href="/catalog/novogodnie-suveniry/magnity/">
<link rel="canonical" href="/catalog/novogodnie-suveniry/breloki/">
<link rel="canonical" href="/catalog/novogodnie-suveniry/vlozheniya_i_elochnye_ukrasheniya/">
<link rel="canonical" href="/catalog/novogodnie-suveniry/nevalyashki/">
<link rel="canonical" href="/catalog/podarochnaya-upakovka-i-suveniry/paskhalnaya_upakovka_i_suveniry/">
<link rel="canonical" href="/catalog/podarochnaya-upakovka-i-suveniry/upakovka_i_suveniry_na_den_sv_valentina_23_fevralya_8_marta/">
<link rel="canonical" href="/catalog/rasprodazha/">


    <meta name="viewport" content="initial-scale=1, width=device-width, maximum-scale=1, minimum-scale=1, user-scalable=no">


<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-82054386-1', 'auto');
  ga('send', 'pageview');

</script>

<script src='https://www.google.com/recaptcha/api.js'></script>
</head>
<body class="<?=getBodyClass($APPLICATION);?> <?if(IS_ELEMENT===true):?>page-element<?endif;?> <?if(IS_SECTION===true):?>page-section<?endif;?>">
<? $APPLICATION->ShowPanel(); ?>
<!-- Yandex.Metrika counter -->
<script type="text/javascript">
(function (d, w, c) {
    (w[c] = w[c] || []).push(function() {
        try {
            w.yaCounter29171335 = new Ya.Metrika({id:29171335,
                    webvisor:true,
                    clickmap:true,
                    trackLinks:true,
                    accurateTrackBounce:true});
        } catch(e) { }
    });

    var n = d.getElementsByTagName("script")[0],
        s = d.createElement("script"),
        f = function () { n.parentNode.insertBefore(s, n); };
    s.type = "text/javascript";
    s.async = true;
    s.src = (d.location.protocol == "https:" ? "https:" : "http:") + "//mc.yandex.ru/metrika/watch.js";

    if (w.opera == "[object Opera]") {
        d.addEventListener("DOMContentLoaded", f, false);
    } else { f(); }
})(document, window, "yandex_metrika_callbacks");
</script>
<noscript><div><img src="//mc.yandex.ru/watch/29171335" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
<!-- /Yandex.Metrika counter -->
<div id="layer">

    <!-- HEADER -->
    <header id="header">
        <div id="header-top" class="clearfix">
            <div id="header-right">
                <div class="vcard">
                    <div class="lc"><a href="/personal/">Личный кабинет</a></div>
                   <div class="number"><span class="tel">+7 (495) 215-23-05</span>
</div>
                    <div class="callback"><a href="#" onclick="yaCounter29171335.reachGoal('send_call'); return true;">Заказать звонок</a></div>
                </div>

<div class="mail_head"><a href="mailto:info@teampack.ru" onclick="yaCounter29171335.reachGoal('click_mail'); return true;">info@teampack.ru</a></div>

                <div id="hm-top-navi">
                    <ul>
                        <li class="navi1"><a class="<?= $APPLICATION->ShowViewContent('top-navi-selected') ?>" href="javascript:void(0)">Меню</a></li>
                        <li class="navi2"><a href="/search/" <? if ($APPLICATION->GetCurDir() == '/search/'): ?>class="selected"<? endif; ?>>Поиск</a></li>
                        <li class="navi3"><a href="javascript:void(0)">Корзина</a></li>
                    </ul>
                </div>
            </div>
            <!-- LOGO -->
            <div id="logo">
                <? if ($APPLICATION->GetCurPage() != SITE_DIR): ?>
                    <a href="<?= SITE_DIR ?>" title="ТимПак">ТимПак</a>
                <? endif; ?>
            </div >
            <!-- END LOGO -->



            <div id="h2015z">
                <div class="p1"></div>
                <div class="s1"></div>
                <div class="s2"></div>
                <div class="s3"></div>
                <div class="p2" style="background: url(/assets/images/2018y.png) 0 100% no-repeat;"></div>
                <div class="p3"></div>
                <div class="p4"></div>
                <div class="p5"></div>
                <div class="p6"></div>
                <div class="p7"></div>
            </div>


<?php /* summer animation
            <div id="h2015">
                <div class="p1"></div>
                <div class="p2"></div>
                <div class="p3"></div>
                <div class="p4"></div>
                <div class="p5"></div>
            </div>
*/ ?>
			<div id="countdown-block">
				<div class="count-block">
					<div class="legend">До Нового года осталось:</div>
					<span id="countdown"></span>
					<script>
						$(document).ready(function () {
							var newYear = new Date();
							newYear = new Date(newYear.getFullYear() + 1, 1 - 1, 1);
							$('#countdown').countdown({
                                until: new Date(<?=date('Y')+1?>, 0, 1),
								format: 'DHMS',
								layout: '<span class="c-block c-days"><span class="image{d100}"></span><span class="image{d10}"></span><span class="image{d1}"></span><span class="leg">{dl}</span></span>' +
									'<span class="c-block"><span class="image{h10}"></span><span class="image{h1}"></span><span class="leg">{hl}</span></span>' +
									'<span class="imageSpace"></span>' +
									'<span class="c-block"><span class="image{m10}"></span><span class="image{m1}"></span><span class="leg">{ml}</span></span>' +
									'<span class="imageSpace"></span>' +
									'<span class="c-block"><span class="image{s10}"></span><span class="image{s1}"></span><span class="leg">{sl}</span></span>'
							});
						});
                    </script>
                </div>
            </div>
        </div>
        <!-- MAIN MENU -->
        <div id="header-mid">
            <?$APPLICATION->IncludeComponent(
                "bitrix:sale.basket.basket.small",
                "top",
                Array(
                    "PATH_TO_BASKET" => "/personal/basket/", // Страница корзины
                    "PATH_TO_ORDER"  => "/personal/order/", // Страница оформления заказа
                    "SHOW_DELAY"     => "Y", // Показывать отложенные товары
                    "SHOW_NOTAVAIL"  => "Y", // Показывать товары, недоступные для покупки
                    "SHOW_SUBSCRIBE" => "Y", // Показывать товары, на которые подписан покупатель
                ),
                false
            );?>


            <?$APPLICATION->IncludeComponent(
                "bitrix:menu",
                "top",
                array(
                    "ROOT_MENU_TYPE"        => "top",
                    "MENU_CACHE_TYPE"       => "N",
                    "MENU_CACHE_TIME"       => "3600",
                    "MENU_CACHE_USE_GROUPS" => "Y",
                    "MENU_CACHE_GET_VARS"   => array(),
                    "MAX_LEVEL"             => "2",
                    "CHILD_MENU_TYPE"       => "left",
                    "USE_EXT"               => "Y",
                    "DELAY"                 => "N",
                    "ALLOW_MULTI_SELECT"    => "N"
                ),
                false
            );?>
            <!-- END MAIN MENU -->
        </div>
    </header>
    <!-- END HEADER -->

