</div>

<!-- FOOTER -->
<footer id="footer">
    <div id="footer-soc">
        <script type="text/javascript">(function () {
                if (window.pluso)if (typeof window.pluso.start == "function") return;
                var d = document, s = d.createElement('script'), g = 'getElementsByTagName';
                s.type = 'text/javascript';
                s.charset = 'UTF-8';
                s.async = true;
                s.src = ('https:' == window.location.protocol ? 'https' : 'http') + '://share.pluso.ru/pluso-like.js';
                var h = d[g]('head')[0] || d[g]('body')[0];
                h.appendChild(s);
            })();</script>
        <div class="pluso" data-options="small,square,line,horizontal,counter,theme=06" data-services="vkontakte,facebook,twitter,google,livejournal,moimir,odnoklassniki,email,print" data-background="transparent"></div>

    </div>
    <div id="footer-left">
        <div id="footer-copyright">&copy; <?=date('Y')?> ООО &laquo;ТимПак&raquo; &mdash; подарочная упаковка</div>
        <div class="vcard  title-hide">
            <div class="legend">Телефон: <span class="tel">+7 (495) 215-23-05</span></div>
            <div class="adr">129515, <span class="locality">Москва</span>, <span class="street-address">улица Академика Королева, дом 13, корпус 1, подъезд 4, этаж 2, офис 3</span></div>
        </div>
        <div id="made-by-seo">Создание сайта в компании - <a href="http://seo-experts.com/sozdanie-i-soprovozhdenie-saytov/" target="_blank">«СЕО Эксперт»</a></div>
    </div>
    <div id="footer-logo">
        <? if ($APPLICATION->GetCurPage() != SITE_DIR): ?>
            <a href="<?= SITE_DIR ?>"></a>
        <? endif; ?>
    </div>
    <?$APPLICATION->IncludeComponent(
        "bitrix:menu",
        "bottom",
        array(
            "ROOT_MENU_TYPE"        => "footer",
            "MENU_CACHE_TYPE"       => "N",
            "MENU_CACHE_TIME"       => "3600",
            "MENU_CACHE_USE_GROUPS" => "Y",
            "MENU_CACHE_GET_VARS"   => array(),
            "MAX_LEVEL"             => "2",
            "CHILD_MENU_TYPE"       => "left",
            "USE_EXT"               => "Y",
            "DELAY"                 => "N",
            "ALLOW_MULTI_SELECT"    => "N"
        ),
        false
    );?>
</footer>
<!-- END FOOTER -->



<!--LiveInternet counter--><script type="text/javascript"><!--
document.write("<a href='//www.liveinternet.ru/click' "+
"target=_blank><img src='//counter.yadro.ru/hit?t52.18;r"+
escape(document.referrer)+((typeof(screen)=="undefined")?"":
";s"+screen.width+"*"+screen.height+"*"+(screen.colorDepth?
screen.colorDepth:screen.pixelDepth))+";u"+escape(document.URL)+
";"+Math.random()+
"' alt='' title='LiveInternet: показано число просмотров и"+
" посетителей за 24 часа' "+
"border='0' width='1' height='1'><\/a>")
//--></script><!--/LiveInternet-->



<?if(!$md->isMobile()):?>
<!-- BEGIN JIVOSITE CODE {literal} -->
<script type='text/javascript'>
(function(){ var widget_id = '9dHo8FCOuf';
var s = document.createElement('script'); s.type = 'text/javascript'; s.async = true; s.src = '//code.jivosite.com/script/widget/'+widget_id; var ss = document.getElementsByTagName('script')[0]; ss.parentNode.insertBefore(s, ss);})();</script>
<!-- {/literal} END JIVOSITE CODE -->
<?endif;?>

</body>
</html>