<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

$this->setFrameMode(true);
?>
<?foreach($arResult["ITEMS"] as $arItem):?>
    <div class="question-item">
        <?
        $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
        $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
        ?>
        <!--<span class="item-date"><?/*=$arItem["DISPLAY_ACTIVE_FROM"]*/?></span>-->
        <h2><?=$arItem['NAME']?></h2>
        <p><?=$arItem['PREVIEW_TEXT']?></p>
        <a href="<?=$arItem['DETAIL_PAGE_URL']?>" data-toggle="1" class="want-more spoiler_links">Узнать ответ</a>
        <div class="spoiler_body">
            <p><?=$arItem['DETAIL_TEXT']?></p>
        </div>
    </div>
<?endforeach;?>
