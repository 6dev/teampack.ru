$(document).ready(function(){
    $('.spoiler_links').click(function(){
        $(this).parent().children('div.spoiler_body').toggle('normal');
        if ($(this).data('toggle') == '1') {
            $(this).text('Скрыть');
            $(this).data('toggle', '0');
        } else {
            $(this).text('Узнать ответ');
            $(this).data('toggle', '1');
        }
        return false;
    });
});