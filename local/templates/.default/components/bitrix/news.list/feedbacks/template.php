<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
}
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<div class="feedbacks-list">
    <? foreach ($arResult['ITEMS'] as $arItem): ?>

        <div class="feedback-item clearfix">
            <div class="details">
                <div class="date"><?= date('d.m.Y', strtotime($arItem['DATE_CREATE'])) ?></div>
                <div class="company"><?= $arItem['PROPERTIES']['company']['VALUE'] ?></div>
                <div class="name"><?= $arItem['NAME'] ?></div>
            </div>
            <div class="desc">
                <?= $arItem['PROPERTIES']['message']['VALUE'] ?>
                <? if ($arItem['ICON']): ?>
                    <div class="picture"><a href="<?= $arItem['BIG'] ?>" class="fancy-box"><img src="<?= $arItem['ICON'] ?>"></a></div>
                <? endif; ?>
            </div>
            <i class="bant"></i>
        </div>
    <? endforeach; ?>
</div>