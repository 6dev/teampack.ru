<?php
/**
 * @author Alexandr Kirshin <kirshin.as@gmail.com>
 */
use victory\helpers\Image;

foreach ($arResult['ITEMS'] as &$arItem) {
    if ($arItem['PROPERTIES']['file']['VALUE']) {
        $arItem['BIG'] = CFIle::GetPath($arItem['PROPERTIES']['file']['VALUE']);
        if ($arItem['BIG']) {
            $arItem['ICON'] = Image::open($arItem['BIG'])->thumbnail(490, 490)->save();
        }
    }
}