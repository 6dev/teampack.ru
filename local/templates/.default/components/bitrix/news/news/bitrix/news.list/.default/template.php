<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?foreach($arResult["ITEMS"] as $arItem):?>
    <span class="item-date"><?=$arItem["DISPLAY_ACTIVE_FROM"]?></span>
    <h2><?=$arItem['NAME']?></h2>
    <p><?=$arItem['PREVIEW_TEXT']?></p>
    <br/>
    <a href="<?=$arItem['DETAIL_PAGE_URL']?>" class="want-more">Подробнее</a>
    <br/><br/>
<?endforeach;?>
<?if($arParams["DISPLAY_BOTTOM_PAGER"]):?>
	<br /><?=$arResult["NAV_STRING"]?>
<?endif;?>