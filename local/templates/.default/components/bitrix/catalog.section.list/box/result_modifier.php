<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
}
use victory\helpers\Image;

$arMap = array();
foreach ($arResult['SECTIONS'] as $key => $arSection) {
    if ($arSection['UF_HIDE']) {
        unset($arResult['SECTIONS'][$key]);
        continue;
    }
    $arMap[$arSection['ID']] = $key;


    $filePicture = ($arSection['PICTURE']['SRC'] ? $arSection['PICTURE']['SRC'] : '/assets/images/sub-no-photo.png');

    if ($arSection['DEPTH_LEVEL'] == $arParams['CURRENT_LEVEL']) {
        if ($filePicture) {
            $arSection['ICON'] = Image::open($filePicture)->thumbnail(250, 225)->save();
        }
    } else {
        if ($filePicture) {
            $arSection['ICON'] = Image::open($filePicture)->thumbnail(98, 98)->save();
        }
    }

    if ($arSection['IBLOCK_SECTION_ID']) {
        $arResult['SECTIONS'][$arMap[$arSection['IBLOCK_SECTION_ID']]]['CHILDREN'][] = $key;
    }
    $arResult['SECTIONS'][$key] = $arSection;
}
$arResult['MAP'] = $arMap;
