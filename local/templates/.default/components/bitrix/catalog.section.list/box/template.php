<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
} ?>

<div class="catalog-folder clearfix title-hide">
    <? foreach ($arResult['SECTIONS'] as $arSection): ?>
        <? if ($arSection['DEPTH_LEVEL'] == $arParams['CURRENT_LEVEL']): ?>

            <? if ($arSection['UF_IS_TEXT'] ): ?>

                <div class="catalog-item ib text-block">
                    <div class="picture">
                        <?= $arSection['DESCRIPTION'] ?>
                        <i class="bant"></i>
                    </div>
                </div>

            <? else: ?>
                <div class="catalog-item ib">
                    <div class="picture">
                        <i class="bant"></i>
                        <a href="<?= $arSection['SECTION_PAGE_URL'] ?>">
                            <span><img src="<?= $arSection['ICON'] ?>"></span>
                        </a>
                    </div>
                    <div class="name"><a href="<?= $arSection['SECTION_PAGE_URL'] ?>"><?= $arSection['NAME'] ?></a></div>

            <? if (count($arSection['CHILDREN']) > 0): ?>

                    <div class="sub-catalog-list">
                        <? foreach ($arSection['CHILDREN'] as $key): ?>
                            <? $arSubSection = $arResult['SECTIONS'][$key]; ?>
                            <div class="sub-item ib">
                                <div class="pic"><a href="<?= $arSubSection['SECTION_PAGE_URL'] ?>"><span><img src="<?= $arSubSection['ICON'] ?>"></span></a></div>
                                <div class="name"><a href="<?= $arSubSection['SECTION_PAGE_URL'] ?>"><?= $arSubSection['NAME'] ?></a></div>
                                <? if (count($arSubSection['CHILDREN']) > 0): ?>
                                    <ul>
                                        <? foreach ($arSubSection['CHILDREN'] as $key2): ?>
                                            <? $arSubSection2 = $arResult['SECTIONS'][$key2]; ?>
                                            <li><a href="<?= $arSubSection2['SECTION_PAGE_URL'] ?>"><?= $arSubSection2['NAME'] ?></a></li>
                                        <? endforeach; ?>
                                    </ul>
                                <? endif; ?>
                            </div>
                        <? endforeach; ?>
                    </div>

            <? endif; ?>
                </div>
            <?endif; ?>
        <? endif; ?>
    <? endforeach; ?>
</div>