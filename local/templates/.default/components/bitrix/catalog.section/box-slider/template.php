<? if (count($arResult['ITEMS']) > 0): ?>
    <h2>Похожие товары</h2>
    <div class="catalog-list content-slider v3 clearfix">
        <div class="content-overflow">
            <div class="content-slider-overflow">
                <ul class="spec-main">

                    <? foreach ($arResult['ITEMS'] as $arItem): ?>
                        <li class="slide">
                            <div class="catalog-item ib">
                                <i class="bant"></i>

                                <div class="cat-item-left">
                                    <div class="picture"><a href="<?= $arItem['DETAIL_PAGE_URL'] ?>"><img src="<?= $arItem['ICON'] ?>"></a></div>
                                </div>
                                <div class="desc">
                                    <h3><a href="<?= $arItem['DETAIL_PAGE_URL'] ?>"><?= $arItem['NAME'] ?></a></h3>

                                    <p>Артикул: <?= CatalogMapper::getPropertyValue($arItem, 'article'); ?><br/>
                                        <? if (CatalogMapper::getPropertyValue($arItem, 'capacity')>1): ?>
                                            Вес вложения: <?= $arItem['PROPERTIES']['VES_VLOZHENIYA_G_NOMENKLATURA']['VALUE']; ?> г
                                        <? endif; ?>
                                    </p>
                                    <? if (count($arItem['PRICES']) > 0): ?>
                                        <form>
                                            <input name="basket[id]" type="hidden" value="<?= $arItem['ID'] ?>"/>

                                            <div class="col">
                                                <div class="col-block">
                                                    <input name="basket[quantity]" type="text"
                                                           value=""
                                                           data-min="<?= $arItem['PROPERTIES']['KOLICHESTVO_V_KOROBKE_NOMENKLATURA']['VALUE'] ?>"
                                                           data-step="<?= $arItem['PROPERTIES']['KOLICHESTVO_V_KOROBKE_NOMENKLATURA']['VALUE'] ?>"/>
                                                    <a href="#" class="plus" title="добавить"></a>
                                                    <a href="#" class="minus" title="убрать"></a>
                                                </div>
                                                <div class="col-legend">

                                                </div>
                                            </div>
                                            <div class="button"><a href="#" class="vm-basket-add">В корзину</a></div>
                                        <?$ib =  inBasket($arItem['ID']);?>
                                            <?if($ib>0):?>
                                            <div class="in-cart">В корзине: <?=$ib?> <a href="#" data-id="<?=$arItem['ID']?>"><img src="/assets/images/ico-del.png" alt="Удалить"></a></div>
                                            <?endif;?>
                                        </form>
                                    <? endif; ?>

                                </div>

                                <? $cnt = count($arItem['PRICES']); ?>
                                <? if ($cnt > 0): ?>
                                    <div class="price-block">
                                        <div class="name">Цена
                                            <? if ($arItem['ALL_PRICE']  && $cnt > 1): ?>
                                                <span class="ico-q ib"><span class="popup">цена рассчитывается от суммы заказа по всему ассортименту<i class="arrow"></i><i class="close"></i></span></span>
                                            <? endif; ?>
                                        </div>
                                        <table class="price-matrix">
                                            <? foreach ($arItem['PRICES'] as $code => $price): ?>
                                                <tr>
                                                    <td><strong>
                                                            <nobr><?= $price['PRINT_DISCOUNT_VALUE'] ?></nobr>
                                                        </strong></td>
                                                    <td>
                                                        <? if ($arItem['ALL_PRICE']  && $cnt > 1): ?>
                                                            <nobr><?= CatalogMapper::getPriceTypeNameByCode($code) ?></nobr>
                                                        <? endif; ?>
                                                    </td>
                                                </tr>
                                                <?if(!$arItem['ALL_PRICE'] ):?>
                                                    <?break;?>
                                                <?endif;?>
                                            <? endforeach; ?>
                                        </table>
                                    </div>
                                <? endif; ?>
                                <div class="icons">
                                    <? if (CatalogMapper::getPropertyValue($arItem, 'novelty')): ?>
                                        <i class="ico-new"></i>
                                    <? endif; ?>
                                    <? if (CatalogMapper::getPropertyValue($arItem, 'sale')): ?>
                                        <i class="ico-rasp"></i>
                                    <? endif; ?>
                                    <? if (CatalogMapper::getPropertyValue($arItem, 'discount')): ?>
                                        <i class="ico-discont"></i>
                                    <? endif; ?>
                                    <? if (CatalogMapper::getPropertyValue($arItem, 'hit')): ?>
                                        <i class="ico-hit"></i>
                                    <? endif; ?>
                                    <? if (CatalogMapper::getPropertyValue($arItem, 'year')): ?>
                                        <i class="ico-year"></i>
                                    <? endif; ?>
                                    <? if ($arItem['PROPERTIES']['SOLD']['VALUE']): ?>
                                        <i class="ico-sold"></i>
                                    <? endif; ?>
                                </div>
                            </div>
                        </li>
                    <? endforeach; ?>
                </ul>
            </div>
        </div>
        <a href="#" class="content-slider-left"></a>
        <a href="#" class="content-slider-right"></a>

        <div class="paginage">
            <a href="#" class="buttt current">1</a><a href="#" class="buttt">2</a>
        </div>
    </div>
<? endif; ?>