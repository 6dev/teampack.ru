<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
}
use victory\helpers\Image;

$prices = array();
$db_res = CSaleDiscount::GetList(array("SORT" => "ASC"), array("LID" => SITE_ID, "ACTIVE" => "Y"));
while ($ar_res = $db_res->Fetch()) {
$sd = CSaleDiscount::GetByID($ar_res['ID']);
$saleConfig = unserialize($sd['ACTIONS']);
    $ar_res['percent'] = floatval($saleConfig['CHILDREN'][0]['DATA']['Value']);
    $ar_res['PRINT_DISCOUNT_VALUE'] = FormatCurrency($arResult['MIN_PRICE']['VALUE'] * (1 - $ar_res['percent'] / 100), 'RUB');
    $prices[] = $ar_res;
}

$arCatalogSaleGroup = CCatalogGroup::GetByID(COption::GetOptionString('seoexp.teampack', 'sale_price_type'));

foreach ($arResult['ITEMS'] as $key => $arItem) {
    if ($arItem['PROPERTIES']['sale']['VALUE'] == 'Y' && count($arItem['PRICES']) > 0) {
        $arItem['PRICES'] = array($arCatalogSaleGroup['NAME'] => $arItem['PRICES'][$arCatalogSaleGroup['NAME']]);
    }
    $filePicture = ($arItem['DETAIL_PICTURE']['SRC'] ? $arItem['DETAIL_PICTURE']['SRC'] : '/assets/images/sub-no-photo.png');
    $arItem['ICON'] = Image::open($filePicture)->thumbnail(104, 120)->save();
    foreach($prices as $p)
    {
        $p['PRINT_DISCOUNT_VALUE'] = FormatCurrency($arItem['MIN_PRICE']['VALUE'] * (1 - $p['percent'] / 100), 'RUB');
        $arItem['prices'][] = $p;
    }
    $last = false;
    $arItem['ALL_PRICE'] = false;
    foreach ($arItem['PRICES'] as $arPrice) {
        if (!$last) {
            $last = $arPrice['PRINT_DISCOUNT_VALUE'];
        }
        if ($last != $arPrice['PRINT_DISCOUNT_VALUE']) {
            $arItem['ALL_PRICE'] = true;
            break;
        }
    }
    $arResult['ITEMS'][$key] = $arItem;
}
