

<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
if(count($arResult['ITEMS'])==0)
{
    return;
}
?><center><h1>«Тимпак» - производитель новогодней подарочной упаковки</h1></center><br><br>
<div class="text-blocks clearfix">
    <?foreach($arResult['ITEMS'] as $arItem):?>
    <div class="text-prev-block <?=$arItem['PROPERTIES']['size']['VALUE_XML_ID']?> ib">
        <i class="bant"></i>
        <h2 class="" style="text-align: left;"><?=$arItem['NAME']?></h2>
      <?=$arItem['DETAIL_TEXT']?>
    </div>
    <?endforeach;?>
</div>