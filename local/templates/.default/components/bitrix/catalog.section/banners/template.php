<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
}
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);

if (empty($arResult['ITEMS'])) {
    return;
}

?>
<? foreach ($arResult['ITEMS'] as $arItem): ?>
    <div class="top-banner">
        <a href="<?= $arItem['PROPERTIES']['url']['VALUE'] ?>" target="_blank"><img src="<?= $arItem['DETAIL_PICTURE']['SRC'] ?>"/></a>
    </div>
<? endforeach; ?>