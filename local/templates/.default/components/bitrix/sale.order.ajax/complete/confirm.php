<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
} ?>
<div class="notetext">
    <?
    if (!empty($arResult["ORDER"])) {
        ?>
        <b><?= GetMessage("SOA_TEMPL_ORDER_COMPLETE") ?></b><br/><br/>
        <table class="sale_order_full_table">
            <tr>
                <td>
                    <?= GetMessage("SOA_TEMPL_ORDER_SUC", Array("#ORDER_DATE#" => $arResult["ORDER"]["DATE_INSERT"], "#ORDER_ID#" => $arResult["ORDER_BASKET"]["ORDER_ID"])) ?><br/><br/>
                    <?= GetMessage("SOA_TEMPL_ORDER_SUC1", Array("#LINK#" => $arParams["PATH_TO_PERSONAL"])) ?>
                </td>
            </tr>
        </table>
        <br/><br/>
        <?
        if (!empty($arResult["PAY_SYSTEM"])) {
            ?>
            <? if ($arResult["PAY_SYSTEM"]['NAME'] == 'Безналичный'): ?>
                <p>
                    В ближайшее время с Вами свяжется наш сотрудник для уточнения деталей. После подтверждения заказа на Ваш электронный адрес будет отправлен счет на оплату.
                </p>
            <? endif; ?>
            <? if ($arResult["PAY_SYSTEM"]['NAME'] == 'Электронные деньги'): ?>
                <p>
                    В ближайшее время с Вами свяжется наш сотрудник для уточнения деталей. После подтверждения заказа на Ваш электронный адрес будет отправлен ссылка на оплату
                </p>
            <? endif; ?>
        <?
        }
    } else {
        ?>
        <b><?= GetMessage("SOA_TEMPL_ERROR_ORDER") ?></b><br/><br/>

        <table class="sale_order_full_table">
            <tr>
                <td>
                    <?= GetMessage("SOA_TEMPL_ERROR_ORDER_LOST", Array("#ORDER_ID#" => $arResult["ORDER_ID"])) ?>
                    <?= GetMessage("SOA_TEMPL_ERROR_ORDER_LOST1") ?>
                </td>
            </tr>
        </table>
    <?
    }
    ?>
</div>