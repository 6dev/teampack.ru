<?
/**
 * @var $this CBitrixComponentTemplate
 */
$arUrls = Array(
    "delete" => $APPLICATION->GetCurPage() . "?action=delete&id=#ID#",
    "delay"  => $APPLICATION->GetCurPage() . "?action=delay&id=#ID#",
    "add"    => $APPLICATION->GetCurPage() . "?action=add&id=#ID#",
);
?>
<? if (count($arResult['GRID']['ROWS']) == 0): ?>
    <? ShowError('Ваша корзина пуста.'); ?>
<? else: ?>
    <div id="basket-container">
        <form method="post" >
            <input type="hidden" name="BasketRefresh" value="BasketRefresh" id="refresh-basket-input"/>
            <input type="hidden" name="delivery" value="" id="delivery-basket-input"/>
            <input type="submit" value="Обновить" style="display: none;"/>

            <div class="basket-block clearfix">
                <table>
                    <thead>
                    <tr>
                        <th>№</th>
                        <th>Фото</th>
                        <th>Наименование/ артикул</th>
                        <th>Цена</th>
                        <th>Количество</th>
                        <th>Сумма</th>
                        <th>Удалить</th>
                    </tr>
                    </thead>
                    <tbody>
                    <? $p = 1; ?>
                    <? foreach ($arResult['GRID']['ROWS'] as $arBasketItem): ?>
                        <tr <? if ($p % 2): ?>class="zebra"<? endif; ?>>
                            <td class="number"><?= $p ?></td>
                            <td class="picture"><a href="<?= $arBasketItem['DETAIL_PAGE_URL'] ?>"><img src="<?= $arBasketItem['ICON'] ?>"></a></td>
                            <td class="name"><a href="<?= $arBasketItem['DETAIL_PAGE_URL'] ?>"><?= $arBasketItem['NAME'] ?></a><br>
                                Артикул: <?= $arBasketItem['PRODUCT']['PROPERTIES']['CML2_ARTICLE']['VALUE']; ?>
                            </td>
                            <td class="price"><?= $arBasketItem["PRICE_FORMATED"] ?></td>
                            <td class="col">
                                <div class="col-td">
                                    <div class="col-legend">
                                        мин. <?= $arBasketItem['PRODUCT']['PROPERTIES']['KOLICHESTVO_V_KOROBKE_NOMENKLATURA']['VALUE'] ?> шт.<br>
                                        шаг <?= $arBasketItem['PRODUCT']['PROPERTIES']['KOLICHESTVO_V_KOROBKE_NOMENKLATURA']['VALUE'] ?> шт.
                                    </div>
                                    <div class="col-block">
                                        <input name="QUANTITY_<?= $arBasketItem["ID"] ?>" type="text" value="<?= $arBasketItem['QUANTITY'] ?>"
                                               data-min="<?= $arBasketItem['PRODUCT']['PROPERTIES']['KOLICHESTVO_V_KOROBKE_NOMENKLATURA']['VALUE'] ?>"
                                               data-step="<?= $arBasketItem['PRODUCT']['PROPERTIES']['KOLICHESTVO_V_KOROBKE_NOMENKLATURA']['VALUE'] ?>"/>
                                        <a href="#" class="plus" title="добавить"></a>
                                        <a href="#" class="minus" title="убрать"></a>
                                    </div>
                                </div>
                            </td>
                            <td class="price-total"><?= $arBasketItem['SUM'] ?></td>
                            <td class="delete"><a href="<?= str_replace("#ID#", $arBasketItem["ID"], $arUrls["delete"]) ?>"><img src="/assets/images/ico-delete.png" width="32" height="32" alt="#"></a></td>
                        </tr>
                        <? $p++; ?>
                    <? endforeach; ?>
                    </tbody>
                </table>
                <div class="clearfix">
                    <div class="itogo-block">
                        <div class="itogo">Всего <?= $arResult['QUANTITY_ALL'] ?> штук.</div>
                        <br/>

                        <p>Сумма: <?= $arResult['allSum_FORMATED'] ?> Ценовая категория: «<?= $arResult['PRICE_GROUP']['NAME_LANG'] ?>». Скидка: <?= $arResult['DISCOUNT_PRICE_PERCENT_FORMATED'] ?> ,
                            или <?= $arResult['DISCOUNT_PRICE_ALL_FORMATED'] ?><br>
                            Масса: <?= $arResult['allWeight_FORMATED'] ?><br/>
                            Стоимость доставки: <?= $arResult['DELIVERY_FORMATTED'] ?></p><br/>

                        <div class="itogo">К оплате: <?= $arResult['allSum_FORMATED'] ?></div>
                    </div>

                    <div class="buttons">
                        <a href="/katalog/">Перейти в каталог</a> <a href="?clear=y">Очистить</a> <a href="#" class="submit-form-link">Пересчитать</a>
                    </div>
                </div>
            </div>
            <div id="order-button"><a href="#">Перейти к оформлению</a></div>
        </form>
    </div>
<?endif; ?>