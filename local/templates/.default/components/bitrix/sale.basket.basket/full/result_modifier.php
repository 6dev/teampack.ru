<?php
/**
 * @author Alexandr Kirshin <kirshin.as@gmail.com>
 */
use victory\helpers\Image;

$quantityAll = 0;
foreach ($arResult['GRID']['ROWS'] as $key => $arBasketItem) {

    $quantityAll += $arBasketItem['QUANTITY'];
    $arProduct = GetIBlockElement($arBasketItem['PRODUCT_ID']);
    $filePicture = ($arBasketItem['DETAIL_PICTURE_SRC'] ? $arBasketItem['DETAIL_PICTURE_SRC'] : '/assets/images/sub-no-photo.png');
    $arResult['GRID']['ROWS'][$key]['PRODUCT'] = $arProduct;
    $arResult['GRID']['ROWS'][$key]['ICON'] = Image::open($filePicture)->thumbnail(58, 58)->save();
    $arResult['DISCOUNT_PRICE_PERCENT_FORMATED'] = $arBasketItem['DISCOUNT_PRICE_PERCENT_FORMATED'];
}
$arResult['QUANTITY_ALL'] = $quantityAll;
if ($_GET['clear'] == 'y') {
    CSaleBasket::DeleteAll(CSaleBasket::GetBasketUserID());
    LocalRedirect($APPLICATION->GetCurPage());
}

$arResult['PRICE_GROUP'] = CCatalogProductProviderCustom::getCurrentPriceGroup();

list($delivery, $mass) = DeliveryCalculator::calc($_POST['DELIVERY_ID'], $arResult['GRID']['ROWS'], $_POST['ORDER_PROP_26']);

$arResult['DELIVERY_FORMATTED'] = CurrencyFormat($delivery, 'RUB');
$arResult['allWeight_FORMATED'] = number_format($mass,2, '.', ' ') . ' кг.';
$arResult['allSum_FORMATED'] = CurrencyFormat($arResult['allSum']+$delivery, 'RUB');