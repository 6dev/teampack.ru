<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
}
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */



?>
<form name="<? echo $arResult["FILTER_NAME"] . "_form" ?>" action="/catalog/" method="get">
    <div class="catalog-filter title-hide">

        <input type="hidden" name="set_filter" value="y"/>
        <? foreach ($arResult["HIDDEN"] as $arItem): ?>
            <input
                type="hidden"
                name="<? echo $arItem["CONTROL_NAME"] ?>"
                id="<? echo $arItem["CONTROL_ID"] ?>"
                value="<? echo $arItem["HTML_VALUE"] ?>"
                />
        <? endforeach; ?>

        <div class="name">Подбор товаров</div>
        <? foreach ($arResult["ITEMS"] as $arItem): ?>
            <? if ($arItem['PRICE']): ?>
                <p><strong>Цена</strong></p>

                <div class="slider-block">
                    <div id="slider-range1" class="slider"></div>
                    <div class="inputs">
                        <span>От </span>
                        <input name="<?= $arItem['VALUES']['MIN']['CONTROL_NAME'] ?>" type="text"
                               value="<?= $arItem['VALUES']['MIN']['HTML_VALUE'] ?>" id="amount-1-min"/>
                        <span>До </span>
                        <input name="<?= $arItem['VALUES']['MAX']['CONTROL_NAME'] ?>" type="text"
                               value="<?= $arItem['VALUES']['MAX']['HTML_VALUE'] ?>" id="amount-1-max"/>
                    </div>
                </div>

                <script>
                    $(function () {
                        $("#slider-range1").sliderValue({
                            'min_input': '#amount-1-min',
                            'max_input': '#amount-1-max',
                            'min_size': <?=floor($arItem['VALUES']['MIN']['VALUE'])?>,
                            'max_size': <?=ceil($arItem['VALUES']['MAX']['VALUE'])?>,
                            'step': 1,
                            'sync_inputs': false
                        });
                        var min_size = <?=floor($arItem['VALUES']['MIN']['VALUE'])?>;
                        var max_size = <?=ceil($arItem['VALUES']['MAX']['VALUE'])?>;

                        $('#amount-1-min').keyup(function (e) {
                            if (e.keyCode == 13) {
                                $('#amount-1-max').focus();
                                return false;
                            }
                            /*   var min = parseInt($(this).val());
                             var max = parseInt($('#amount-1-max').val());
                             max = isNaN(max) ? max_size : max;
                             if (max < min) {
                             max = min;
                             }
                             $('#amount-1-max').val(max).trigger('change');*/
                        });
                        $('#amount-1-max').keyup(function () {
                            /*
                             var max = parseInt($(this).val());
                             var min = parseInt($('#amount-1-min').val());
                             min = isNaN(min) ? min_size : min;
                             if (min > max) {
                             min = max;
                             }
                             $('#amount-1-min').val(min).trigger('change')*/
                        });
                    });
                </script>

            <? elseif (!empty($arItem["VALUES"])): ?>
                <p><strong><?= $arItem['NAME'] ?></strong></p>
                <? foreach ($arItem['VALUES'] as $val => $ar): ?>
                    <p>
                        <input <?if ($ar[HIDDEN]=='Y') echo "style='display:none;'";?>
                            name="<?= $ar['CONTROL_NAME'] ?>"
                            type="checkbox"
                            value="<?= $ar['HTML_VALUE'] ?>"
                            <? echo $ar["CHECKED"] ? 'checked="checked"' : '' ?>
                            id="<?= $ar['CONTROL_ID'] ?>">
                        <label <?if ($ar[HIDDEN]=='Y') echo "style='display:none;'";?> for="<?= $ar['CONTROL_ID'] ?>"><?= $ar['VALUE'] ?></label>
                    </p>
                <? endforeach; ?>
            <? endif; ?>
        <? endforeach; ?>
        <div class="button"><a href="#" class="submit-form-link filter-button">Подобрать</a>&nbsp;&nbsp;&nbsp;<a href="<?= $APPLICATION->GetCurDir(); ?>">Очистить</a></div>

    </div>
</form>

<div class="contacts-block-left">
    <div class="cont-item">
        <strong><a href="#">Офис</a></strong>

        <p>Адрес: 129515, Москва, улица Академика Королева, дом 13, корпус 1, подъезд 4, этаж 2, офис 3</p>
    </div>
    <div class="cont-item">
        <strong><a href="#">Склад</a></strong>

        <p>141051, Московская область, Мытищинский район, п/о Красная Горка, пос. Птицефабрики, дом 5</p>
    </div>
</div>