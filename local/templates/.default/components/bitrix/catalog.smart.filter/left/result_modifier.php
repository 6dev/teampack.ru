<?php
/**
 * @author Alexandr Kirshin <kirshin.as@gmail.com>
 */



global ${$arParams['FILTER_NAME']};
$arFilter = ${$arParams['FILTER_NAME']};
$arFilter['IBLOCK_ID'] = $arParams['IBLOCK_ID'];
$arFilter['ACTIVE'] = 'Y';
$arFilter['INCLUDE_SUBSECTIONS'] = 'Y';
$arFilter['SECTION_ID'] = 19;

$groups = array();
$resGroups = CIBlockElement::GetList(array(), $arFilter, array('PROPERTY_MATERIAL_NOMENKLATURA'));
while ($arGroup = $resGroups->GetNext()) {
    if ($arGroup['PROPERTY_MATERIAL_NOMENKLATURA_ENUM_ID']) {
        $groups[] = $arGroup['PROPERTY_MATERIAL_NOMENKLATURA_ENUM_ID'];
    }
}
foreach ($arResult["ITEMS"] as $key=>$arItem) {
    if ($arItem['CODE'] == 'MATERIAL_NOMENKLATURA') {

        $visibleValues = array();
        foreach ($arItem['VALUES'] as $val => $ar) {
            if (in_array($val, $groups)) {
                $visibleValues[$val] = $ar;
                $visibleValues[$val][HIDDEN]='N';
            } 


            if (!in_array($val, $groups)) {
                $visibleValues[$val] = $ar;
                $visibleValues[$val][HIDDEN]='N'; // Поменять на Y если нужно прятать лишние
            } 


        }
        $arResult["ITEMS"][$key]['VALUES'] = $visibleValues;        
        break;
    }
}


 // if (CUser::GetID()==1) print_r($arResult);
