<?php

use \Bitrix\Catalog\CatalogViewedProductTable as CatalogViewedProductTable;
CatalogViewedProductTable::refresh($arResult['ID'], CSaleBasket::GetBasketUserID());

addBodyClass('page-element', $APPLICATION);
if(!defined('IS_ELEMENT')) define('IS_ELEMENT', true);