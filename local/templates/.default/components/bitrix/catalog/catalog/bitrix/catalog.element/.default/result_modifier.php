<?
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) {
    die();
}

use victory\helpers\Image;
use victory\helpers\IBlockHelper;

$filePicture = ($arResult['DETAIL_PICTURE']['SRC'] ? $arResult['DETAIL_PICTURE']['SRC'] : '/assets/images/sub-no-photo.png');

$arResult['ORIGINAL'][] = $filePicture;
$arResult['BIG'][] = Image::open($filePicture)->thumbnail(250, 250)->save();
$arResult['SMALL'][] = Image::open($filePicture)->thumbnail(58, 58)->save();

foreach ($arResult['PROPERTIES']['MORE_PHOTO']['VALUE'] as $fileId) {
    $filePicture = CFIle::GetPath($fileId);
    $arResult['ORIGINAL'][] = $filePicture;
    $arResult['BIG'][] = Image::open($filePicture)->thumbnail(250, 250)->save();
    $arResult['SMALL'][] = Image::open($filePicture)->thumbnail(58, 58)->save();
}
$quantity = '';
$arSection = CIBlockSection::GetByID($arResult['IBLOCK_SECTION_ID'])->Fetch();
while ($arSection) {
    $notSync = IBlockHelper::getUserFieldSection(CATALOG_IBLOCK_ID, $arSection['ID'], 'UF_SYNC');
    if ($notSync) {
        $quantity = $arResult['CATALOG_QUANTITY'] ? $arResult['CATALOG_QUANTITY'] . ' шт.' : 'Продано';
        break;
    }

    if ($arSection['IBLOCK_SECTION_ID']) {
        $arSection = CIBlockSection::GetByID($arSection['IBLOCK_SECTION_ID'])->Fetch();
    } else {
        break;
    }
}

$arResult['SHOW_ONLY'] = IBlockHelper::getUserField('IBLOCK_2_SECTION', $arSection['ID'], 'UF_SHOW_ONLY') == 1;

$quantity = $quantity ? $quantity : ($arResult['PROPERTIES']['QUANTITY']['VALUE'] ? $arResult['PROPERTIES']['QUANTITY']['VALUE'] : 'Много');
$arResult['CATALOG_QUANTITY_LABEL'] = $quantity;


$arCatalogSaleGroup = CCatalogGroup::GetByID(COption::GetOptionString('seoexp.teampack', 'sale_price_type'));
if ($arResult['PROPERTIES']['sale']['VALUE'] == 'Y' && count($arResult['PRICES']) > 0) {
    $arResult['PRICES'] = array($arCatalogSaleGroup['NAME'] => $arResult['PRICES'][$arCatalogSaleGroup['NAME']]);
}

$last = false;
$arResult['ALL_PRICE'] = false;
foreach ($arResult['PRICES'] as $arPrice) {
    if (!$last) {
        $last = $arPrice['PRINT_DISCOUNT_VALUE'];
    }
    if ($last != $arPrice['PRINT_DISCOUNT_VALUE']) {
        $arResult['ALL_PRICE'] = true;
        break;
    }
}
