<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
}

$arPrices = array();
foreach ($arResult['PRICES'] as $code => $price) {
    $arPrices[] = $price["VALUE"];
}
?>
    <div class="oneclick-buy-container product-block popup-container">
        <div class="oneclick-buy-content popup-content">
            <div class="close-popup">
                <a onclick="closePopup()"><img src="/assets/images/close.png" /></a>
            </div>
            <form method="POST" id="oneclick-form" action="javascript:void(null);" onsubmit="call()" novalidate>
                <div class="oneclick-form-title">Покупка в один клик</div>
                <div class="form-control">
                    <label for="name">ФИО*:</label><input id="oneclick-name" name="name" value="" type="text" required>
                </div>
                <div class="form-control">
                    <label for="phone">Телефон*:</label><input class="phone-mask" id="oneclick-phone" name="phone" value="" type="text" required>
                </div>
                <div class="form-control">
                    <label for="email">E-mail*:</label><input id=oneclick-email" name="email" value="" type="email" required>
                </div>
                <input type="hidden" name="product" id="element_id_input" value="">
                <div class="col form-control">
                    <label for="quantity">Количество:</label>
                    <div class="col-block oneclick-col">
                        <input name="quantity" id="oneclick-quanity" type="text"
                               value=""
                               data-min=""
                               data-step="" required/>
                        <a href="#" class="plus" title="добавить"></a>
                        <a href="#" class="minus" title="убрать"></a>
                    </div>
                    <div class="col-legend">
                    </div>
                </div>
                <div class="form-control">
                    <input value="Отправить" class="button" type="submit">
                </div>
            </form>
            <div id="oneclick-results"></div>
        </div>
    </div>

    <div class="popup-container question-popup">
        <div class="popup-content">
            <div class="close-popup">
                <a onclick="closePopup()"><img src="/assets/images/close.png" /></a>
            </div>
            <?$APPLICATION->IncludeComponent(
                "bitrix:form.result.new",
                "question",
                Array(
                    "WEB_FORM_ID" => "1",
                    "IGNORE_CUSTOM_TEMPLATE" => "N",
                    "USE_EXTENDED_ERRORS" => "N",
                    "SEF_MODE" => "N",
                    "VARIABLE_ALIASES" => Array(
                        "WEB_FORM_ID" => "WEB_FORM_ID",
                        "RESULT_ID" => "RESULT_ID"
                    ),
                    "CACHE_TYPE" => "A",
                    "CACHE_TIME" => "3600",
                    "LIST_URL" => "",
                    "EDIT_URL" => "",
                    "SUCCESS_URL" => "",
                    "CHAIN_ITEM_TEXT" => "",
                    "CHAIN_ITEM_LINK" => ""
                )
            );?>
        </div>
    </div>

    <div class="popup-container delivery-popup">
        <div class="popup-content">
            <div class="close-popup">
                <a onclick="closePopup()"><img src="/assets/images/close.png" /></a>
            </div>
            <h2>Условия доставки</h2>
            Самовывоз из офиса при сумме заказа до 10 000 руб. (<a href="/contacts/" title="Адрес офиса">Адрес офиса</a>);<br>
            Самовывоз со склада при сумме заказа от 10 000 руб. (<a href="/contacts/" title="Адрес склада">Адрес склада</a>);<br>
            Доставка по Москве (расчет от объема, рассчитывается при оформлении заказа);<br>
            Доставка по МО (расчет от объема + км, рассчитывается при оформлении заказа).<br>
        </div>
    </div>

    <div class="popup-container pay-popup">
        <div class="popup-content">
            <div class="close-popup">
                <a onclick="closePopup()"><img src="/assets/images/close.png" /></a>
            </div>
            <h2>Условия оплаты</h2>
            <ul>
                <li>50% предоплаты в течении 3-х банковских дней со дня выставления счета и 50% за 2 дня до отгрузки товара;</li>
                <li>При наличии товара на складе возможна 100% предоплата и немедленная отгрузка</li>
            </ul>
            <div class="pay-popup-list">
                Способы оплаты:
                <ul>
                    <li>Безналичный расчет</li>
                    <li>Наличный расчет</li>
                    <li>Электронные деньги</li>
                </ul>
            </div>
        </div>
    </div>

    <div class="product-block clearfix" itemscope itemtype="http://schema.org/Product">
        <div class="product-pictures">
            <? foreach ($arResult['ORIGINAL'] as $i => $original): ?>
                <div class="picture" <? if ($i > 0): ?>style="display: none;" <? endif; ?>>
                    <a href="<?= $original ?>" class="fancy-box" rel="gallery"><img src="<?= $arResult['BIG'][$i] ?>" itemprop="image"><i class="zoom"></i>
                        <i class="loc">

                            <i class="x"><?= $arResult['PROPERTIES']['DLINA_NOMENKLATURA']['VALUE'] ? $arResult['PROPERTIES']['DLINA_NOMENKLATURA']['VALUE'] : $arResult['PROPERTIES']['DIAMETR_MM_NOMENKLATURA']['VALUE'] ?></i>
                            <i class="y"><?= $arResult['PROPERTIES']['VYSOTA_NOMENKLATURA']['VALUE'] ?></i>
                            <i class="z"><?= $arResult['PROPERTIES']['SHIRINA_NOMENKLATURA']['VALUE'] ?></i>
                        </i>
                        <? if ($arResult['PROPERTIES']['SOLD']['VALUE']): ?>
                            <i class="ico-sold"></i>
                        <? endif; ?>
                        <? if (CatalogMapper::getPropertyValue($arResult, 'novelty')): ?>
                            <i class="ico-new"></i>
                        <? endif; ?>
                          <? if ((CatalogMapper::getPropertyValue($arResult, 'sale')) || ($arResult[IBLOCK_SECTION_ID]==436) ) : ?>
                            <i class="ico-rasp"></i>
                        <? endif; ?>
                        <? if (CatalogMapper::getPropertyValue($arResult, 'discount')): ?>
                            <i class="ico-discont"></i>
                        <? endif; ?>
                        <? if (CatalogMapper::getPropertyValue($arResult, 'hit')): ?>
                            <i class="ico-hit"></i>
                        <? endif; ?>
                        <? if ($arItem['PROPERTIES']['SYMBOL']['VALUE']): ?>
                            <i class="ico-year"></i>
                        <? endif; ?>
                    </a>
                    <i class="bant"></i>
                </div>
            <? endforeach ?>

            <div class="more-pics">
                <? foreach ($arResult['SMALL'] as $i => $small): ?>
                    <div class="more-item"><a href="<?= $arResult['ORIGINAL'][$i] ?>" class="fancy-box" rel="gallery-small"><img src="<?= $small ?>"></a></div>
                <? endforeach ?>
            </div>
        </div>
        <div class="product-details">
            <div class="params-block">
                <h2 class="hidden-val" itemprop="name"><?= $arResult['NAME'] ?></h2>
                <div class="params">
                    <div class="h2">Параметры</div>
                    <? if ($arResult['PROPERTIES']['CML2_ARTICLE']['VALUE']): ?>
                        Артикул: <strong><?= $arResult['PROPERTIES']['CML2_ARTICLE']['VALUE'] ?></strong><br>
                    <? endif; ?>
                    <? if (!$arResult['SHOW_ONLY']): ?>
                        <? if ($arResult['PROPERTIES']['VID_OTDELKI_NOMENKLATURA']['VALUE']): ?>
                            Вид отделки: <strong><?= $arResult['PROPERTIES']['VID_OTDELKI_NOMENKLATURA']['VALUE'] ?></strong><br>
                        <? endif; ?>
                        <? if ($arResult['PROPERTIES']['VES_VLOZHENIYA_G_NOMENKLATURA']['VALUE'] > 1): ?>
                            Вес вложения: <strong><?= $arResult['PROPERTIES']['VES_VLOZHENIYA_G_NOMENKLATURA']['VALUE'] ?> г</strong><br>
                        <? endif; ?>
                        <? if ($arResult['PROPERTIES']['MATERIAL_NOMENKLATURA']['VALUE']): ?>
                            Материал: <strong><?= $arResult['PROPERTIES']['MATERIAL_NOMENKLATURA']['VALUE'] ?></strong><br>
                        <? endif; ?>
                        <? if ($arResult['PROPERTIES']['KOLICHESTVO_V_KOROBKE_NOMENKLATURA']['VALUE']): ?>
                            Количество в коробке: <strong><?= $arResult['PROPERTIES']['KOLICHESTVO_V_KOROBKE_NOMENKLATURA']['VALUE'] ?> шт.</strong><br>
                        <? endif; ?>

                        Наличие: <strong><?= $arResult['CATALOG_QUANTITY_LABEL']; ?></strong>
                    <? endif; ?>
                </div>

                <div class="params">
                    <? if ($arResult['PROPERTIES']['DLINA_NOMENKLATURA']['VALUE']
                        || $arResult['PROPERTIES']['DLINA_NOMENKLATURA']['VALUE']
                        || $arResult['PROPERTIES']['VYSOTA_NOMENKLATURA']['VALUE']
                    ): ?>
                        <div class="h2">Размеры</div>
                        <? if ($arResult['PROPERTIES']['DLINA_NOMENKLATURA']['VALUE']): ?>
                            Длина: <?= $arResult['PROPERTIES']['DLINA_NOMENKLATURA']['VALUE'] ?>&nbsp;мм<br>
                        <? endif; ?>
                        <? if ($arResult['PROPERTIES']['SHIRINA_NOMENKLATURA']['VALUE']): ?>
                            Ширина: <?= $arResult['PROPERTIES']['SHIRINA_NOMENKLATURA']['VALUE'] ?>&nbsp;мм<br>
                        <? endif; ?>
                        <? if ($arResult['PROPERTIES']['VYSOTA_NOMENKLATURA']['VALUE']): ?>
                            Высота: <?= $arResult['PROPERTIES']['VYSOTA_NOMENKLATURA']['VALUE'] ?>&nbsp;мм<br/>
                        <? endif; ?>
                        <? if ($arResult['PROPERTIES']['DIAMETR_MM_NOMENKLATURA']['VALUE']): ?>
                            Диаметр: <?= $arResult['PROPERTIES']['DIAMETR_MM_NOMENKLATURA']['VALUE'] ?>&nbsp;мм<br/>
                        <? endif; ?>

                    <? endif; ?>
                </div>

            </div>



            <div class="detail-text" itemprop="description">
                <p><?= $arResult['PROPERTIES']['OPISANIE_UPAKOVKI_NOMENKLATURA']['VALUE'] ?></p>
                <? if ($_GET['print'] != 'y'): ?>

                <? endif; ?>
            </div>


            <? if (!$arResult['SHOW_ONLY']): ?>

                <? if (!$arResult['PROPERTIES']['SOLD']['VALUE']): ?>
                    <div class="eaist-block">
                        <a href="/eaist/?article=<?= $arResult['PROPERTIES']['CML2_ARTICLE']['VALUE'] ?>">Создать оферту ЕАИСТ</a>
			
<?php
$pos = mb_strpos($arResult['NAME'], 'с анимацией', 0, 'UTF-8');
if ($pos!==false) {

if ($arResult['PROPERTIES']['MATERIAL_NOMENKLATURA']['VALUE']) {
   $mat=$arResult['PROPERTIES']['MATERIAL_NOMENKLATURA']['VALUE'];
} else $mat='';

$cont4d = '
<span class="ico-q ib" style="background: url(/images/question.png) 0 0 no-repeat;
    width: 25px;
    height: 40px;
    font-size: 0;
    vertical-align: middle;
    margin: 0 0 2px 5px;
    cursor: pointer;
margin-left: 10px;
    position: relative;"><span class="popup" style="left:-25px;bottom:55px;width: 300px;line-height: 18px;">Упаковка с изображением щенка – символа года, который оживает и поздравляет всех с Новым годом, стоит только навести камеру мобильного телефона или планшета на изображение. Для этого нужно скачать бесплатное приложение 4D PODAROK в GOOGLE PLAY или APP STORE. <a onclick="window.location=this.href" href="http://www.teampack.ru/news/besplatnoe-prilozhenie-4d-podarok/">Подробнее...</a><i class="arrow"></i><i class="close"></i></span></span>
';

$APPLICATION->AddViewContent("fourD", $cont4d, 500);

}


?>


                        <a id="question" onclick="showQuestion('<?=$arResult["NAME"]?>')">Задать вопрос</a>
                    </div>
                <? endif; ?>

                <? $cnt = count($arResult['PRICES']); ?>
                <? if ($cnt > 0): ?>
                    <div class="price-block" itemprop="offers" itemscope itemtype="http://schema.org/Offer">
                        <div class="name">Цена
                            <? if ($arResult['ALL_PRICE'] && $cnt > 1): ?>
                                <span class="ico-q ib"><span class="popup">цена рассчитывается от суммы заказа по всему ассортименту<i class="arrow"></i><i class="close"></i></span></span>
                            <? endif; ?>
                        </div>
                        <table class="price-matrix">
                            <? foreach ($arResult['PRICES'] as $code => $price): ?>
                                <tr>
                                    <td><strong itemprop="price"><?= $price['PRINT_DISCOUNT_VALUE'] ?></strong><span class="hidden-val" itemprop="priceCurrency">RUB</span></td>
                                    <td>
                                        <? if ($arResult['ALL_PRICE'] && $cnt > 1): ?>
                                            <?= CatalogMapper::getPriceTypeNameByCode($code) ?>
                                        <? endif; ?>
                                    </td>
                                </tr>
                                <? if (!$arResult['ALL_PRICE']): ?>
                                    <? break; ?>
                                <? endif; ?>
                            <? endforeach; ?>
                        </table>
                    </div>

                    <? if (!$arResult['PROPERTIES']['SOLD']['VALUE']): ?>
                        <form method="post">
                            <?
                            if (!$arResult['PROPERTIES']['KOLICHESTVO_V_KOROBKE_NOMENKLATURA']['VALUE']) {
                                $arResult['PROPERTIES']['KOLICHESTVO_V_KOROBKE_NOMENKLATURA']['VALUE'] = 1;
                            }
                            ?>
                            <input name="basket[id]" type="hidden" value="<?= $arResult['ID'] ?>"/>
                            <? if ($_GET['print'] != 'y'): ?>
                                <div class="col relative-col">
                                    <div class="quanity-attention arrow_box">Укажите количество</div>
                                    <div class="col-legend">

                                    </div>
                                    <div class="col-block">
                                        <input class="quantity__input" name="basket[quantity]" type="text"
                                               value=""
                                               data-min="<?= $arResult['PROPERTIES']['KOLICHESTVO_V_KOROBKE_NOMENKLATURA']['VALUE'] ?>"
                                               data-step="<?= $arResult['PROPERTIES']['KOLICHESTVO_V_KOROBKE_NOMENKLATURA']['VALUE'] ?>"/>
                                        <a href="#" class="plus" title="добавить"></a>
                                        <a href="#" class="minus" title="убрать"></a>
                                    </div>

                                    <div class="button button-basket"><a href="#" class="vm-basket-add">В корзину</a></div>
                                    <div class="button oneclick-btn button-basket"><a href="" onclick="return oneclickBuyShow('<?= $arResult['ID'] ?>', '<?= $arResult['PROPERTIES']['KOLICHESTVO_V_KOROBKE_NOMENKLATURA']['VALUE'] ?>', '<?= $arResult['PROPERTIES']['KOLICHESTVO_V_KOROBKE_NOMENKLATURA']['VALUE'] ?>',  $(this).closest('form').find('.quantity__input').val())" class="btn-oneclick-buy">Купить в 1 клик</a></div>
                                    <? $ib = inBasket($arResult['ID']); ?>
                                    <? if ($ib > 0): ?>
                                        <div class="in-cart">

                                            <input type="hidden" name="basket[delete][<?= $arResult['ID'] ?>]" value="N"/>
                                            В корзине: <?= $ib ?> <a href="#" data-id="<?= $arResult['ID'] ?>"><img src="/assets/images/ico-del.png" alt="Удалить"></a>

                                        </div>
                                    <? endif; ?>

                                </div>
                            <? endif; ?>
                        </form>
                        <div class="conditions-links-block"><a id="delivery-info-link">Условия доставки</a><a id="pay-info-link">Условия оплаты</a></div>
                    <? endif; ?>
                <? endif; ?>
            <? endif; ?>
        </div>
    </div>

<? if ($_GET['print'] != 'y'): ?>
    <?
    global $arrFilterMore;
    $arrFilterMore = array(
        'IBLOCK_ID'                      => $arParams['IBLOCK_ID'],
        'ACTIVE'                         => 'Y',
        '!ID'                            => $arResult['ID'],
        'PROPERTY_MATERIAL_NOMENKLATURA' => $arResult['PROPERTIES']['MATERIAL_NOMENKLATURA']['VALUE_ENUM_ID'],
        'PROPERTY_weight_attach'         => $arResult['PROPERTIES']['weight_attach']['VALUE'],
        'PROPERTY_SOLD'                  => false,
    );


    $arArchiveSection = CIBlockSection::GetByID(28)->GetNext();
    $resArchSection = CIBlockSection::GetList(
        array(),
        array('IBLOCK_ID' => $arParams['IBLOCK_ID'], '>LEFT_MARGIN' => $arArchiveSection['LEFT_MARGIN'], '<RIGHT_MARGIN' => $arArchiveSection['RIGHT_MARGIN']),
        false,
        array('ID', 'CODE', 'SECTION_PAGE_URL')
    );
    while ($arSection = $resArchSection->GetNext()) {
        $arrFilterMore['!SECTION_ID'][] = $arSection['ID'];
    }

    ?>
    <? $APPLICATION->IncludeComponent(
        "seoexp:catalog.section",
        "box-slider",
        Array(
            "IBLOCK_TYPE"                     => "1c_catalog", // Тип инфоблока
            "IBLOCK_ID"                       => "2", // Инфоблок
            "SECTION_ID"                      => "", // ID раздела
            "SECTION_CODE"                    => "", // Код раздела
            "SECTION_USER_FIELDS"             => array( // Свойства раздела
                0 => "",
                1 => "",
            ),
            "ELEMENT_SORT_FIELD"              => "PROPERTY_SOLD",
            "ELEMENT_SORT_ORDER"              => "asc",
            "ELEMENT_SORT_FIELD2"             => "CATALOG_PRICE_4", // Поле для второй сортировки элементов
            "ELEMENT_SORT_ORDER2"             => "asc", // Порядок второй сортировки элементов
            "ELEMENT_SORT_FIELD3"             => "name", // Поле для второй сортировки элементов
            "ELEMENT_SORT_ORDER3"             => "asc", // Порядок второй сортировки элементов
            "FILTER_NAME"                     => "arrFilterMore", // Имя массива со значениями фильтра для фильтрации элементов
            "INCLUDE_SUBSECTIONS"             => "Y", // Показывать элементы подразделов раздела
            "SHOW_ALL_WO_SECTION"             => "Y", // Показывать все элементы, если не указан раздел
            "HIDE_NOT_AVAILABLE"              => "N", // Не отображать товары, которых нет на складах
            "PAGE_ELEMENT_COUNT"              => "30", // Количество элементов на странице
            "LINE_ELEMENT_COUNT"              => "3", // Количество элементов выводимых в одной строке таблицы
            "PROPERTY_CODE"                   => array( // Свойства
                0 => "weight_attach",
                1 => "SOLD",
                2 => "MATERIAL_NOMENKLATURA",
            ),
            "OFFERS_LIMIT"                    => "5", // Максимальное количество предложений для показа (0 - все)
            "TEMPLATE_THEME"                  => "blue", // Цветовая тема
            "ADD_PICT_PROP"                   => "-", // Дополнительная картинка основного товара
            "LABEL_PROP"                      => "-", // Свойство меток товара
            "PRODUCT_SUBSCRIPTION"            => "N", // Разрешить оповещения для отсутствующих товаров
            "SHOW_DISCOUNT_PERCENT"           => "N", // Показывать процент скидки
            "SHOW_OLD_PRICE"                  => "N", // Показывать старую цену
            "MESS_BTN_BUY"                    => "Купить", // Текст кнопки "Купить"
            "MESS_BTN_ADD_TO_BASKET"          => "В корзину", // Текст кнопки "Добавить в корзину"
            "MESS_BTN_SUBSCRIBE"              => "Подписаться", // Текст кнопки "Уведомить о поступлении"
            "MESS_BTN_DETAIL"                 => "Подробнее", // Текст кнопки "Подробнее"
            "MESS_NOT_AVAILABLE"              => "Нет в наличии", // Сообщение об отсутствии товара
            "SECTION_URL"                     => "", // URL, ведущий на страницу с содержимым раздела
            "DETAIL_URL"                      => "", // URL, ведущий на страницу с содержимым элемента раздела
            "SECTION_ID_VARIABLE"             => "SECTION_ID", // Название переменной, в которой передается код группы
            "AJAX_MODE"                       => "N", // Включить режим AJAX
            "AJAX_OPTION_JUMP"                => "N", // Включить прокрутку к началу компонента
            "AJAX_OPTION_STYLE"               => "Y", // Включить подгрузку стилей
            "AJAX_OPTION_HISTORY"             => "N", // Включить эмуляцию навигации браузера
            "CACHE_TYPE"                      => "A", // Тип кеширования
            "CACHE_TIME"                      => "36000000", // Время кеширования (сек.)
            "CACHE_GROUPS"                    => "Y", // Учитывать права доступа
            "SET_META_KEYWORDS"               => "Y", // Устанавливать ключевые слова страницы
            "META_KEYWORDS"                   => "-", // Установить ключевые слова страницы из свойства
            "SET_META_DESCRIPTION"            => "Y", // Устанавливать описание страницы
            "META_DESCRIPTION"                => "-", // Установить описание страницы из свойства
            "BROWSER_TITLE"                   => "-", // Установить заголовок окна браузера из свойства
            "ADD_SECTIONS_CHAIN"              => "N", // Включать раздел в цепочку навигации
            "DISPLAY_COMPARE"                 => "N", // Выводить кнопку сравнения
            "SET_TITLE"                       => "Y", // Устанавливать заголовок страницы
            "SET_STATUS_404"                  => "N", // Устанавливать статус 404, если не найдены элемент или раздел
            "CACHE_FILTER"                    => "N", // Кешировать при установленном фильтре
            "PRICE_CODE"                      => $arParams['PRICE_CODE'],
            "USE_PRICE_COUNT"                 => "N", // Использовать вывод цен с диапазонами
            "SHOW_PRICE_COUNT"                => "1", // Выводить цены для количества
            "PRICE_VAT_INCLUDE"               => "Y", // Включать НДС в цену
            "CONVERT_CURRENCY"                => "N", // Показывать цены в одной валюте
            "BASKET_URL"                      => "/personal/basket.php", // URL, ведущий на страницу с корзиной покупателя
            "ACTION_VARIABLE"                 => "action", // Название переменной, в которой передается действие
            "PRODUCT_ID_VARIABLE"             => "id", // Название переменной, в которой передается код товара для покупки
            "USE_PRODUCT_QUANTITY"            => "N", // Разрешить указание количества товара
            "ADD_PROPERTIES_TO_BASKET"        => "Y", // Добавлять в корзину свойства товаров и предложений
            "PRODUCT_PROPS_VARIABLE"          => "prop", // Название переменной, в которой передаются характеристики товара
            "PARTIAL_PRODUCT_PROPERTIES"      => "N", // Разрешить добавлять в корзину товары, у которых заполнены не все характеристики
            "PRODUCT_PROPERTIES"              => "", // Характеристики товара
            "PAGER_TEMPLATE"                  => ".default", // Шаблон постраничной навигации
            "DISPLAY_TOP_PAGER"               => "N", // Выводить над списком
            "DISPLAY_BOTTOM_PAGER"            => "Y", // Выводить под списком
            "PAGER_TITLE"                     => "Товары", // Название категорий
            "PAGER_SHOW_ALWAYS"               => "Y", // Выводить всегда
            "PAGER_DESC_NUMBERING"            => "N", // Использовать обратную навигацию
            "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000", // Время кеширования страниц для обратной навигации
            "PAGER_SHOW_ALL"                  => "Y", // Показывать ссылку "Все"
            "AJAX_OPTION_ADDITIONAL"          => "", // Дополнительный идентификатор
            "PRODUCT_QUANTITY_VARIABLE"       => "quantity", // Название переменной, в которой передается количество товара
        ),
        false
    ); ?>
    <?$APPLICATION->IncludeComponent(
        "bitrix:catalog.viewed.products",
        "box-slider",
        Array(
            "IBLOCK_TYPE" => "1c_catalog",
            "IBLOCK_ID" => "2",
            "SHOW_FROM_SECTION" => "Y",
            "HIDE_NOT_AVAILABLE" => "N",
            "SHOW_DISCOUNT_PERCENT" => "Y",
            "PRODUCT_SUBSCRIPTION" => "N",
            "SHOW_NAME" => "Y",
            "SHOW_IMAGE" => "Y",
            "MESS_BTN_BUY" => "Купить",
            "MESS_BTN_DETAIL" => "Подробнее",
            "MESS_BTN_SUBSCRIBE" => "Подписаться",
            "PAGE_ELEMENT_COUNT" => "5",
            "LINE_ELEMENT_COUNT" => "3",
            "TEMPLATE_THEME" => "blue",
            "DETAIL_URL" => "",
            "CACHE_TYPE" => "A",
            "CACHE_TIME" => "36000000",
            "CACHE_GROUPS" => "Y",
            "SHOW_OLD_PRICE" => "N",
            "PRICE_CODE" => $arParams['PRICE_CODE'],
            "SHOW_PRICE_COUNT" => "1",
            "PRICE_VAT_INCLUDE" => "Y",
            "CONVERT_CURRENCY" => "N",
            "BASKET_URL" => "/personal/basket.php",
            "ACTION_VARIABLE" => "action",
            "PRODUCT_ID_VARIABLE" => "id",
            "ADD_PROPERTIES_TO_BASKET" => "Y",
            "PRODUCT_PROPS_VARIABLE" => "prop",
            "PARTIAL_PRODUCT_PROPERTIES" => "N",
            "USE_PRODUCT_QUANTITY" => "Y",
            "SHOW_PRODUCTS_2" => "Y",
            "SECTION_ID" => $GLOBALS["CATALOG_CURRENT_SECTION_ID"],
            "SECTION_CODE" => "",
            "SECTION_ELEMENT_ID" => $GLOBALS["CATALOG_CURRENT_ELEMENT_ID"],
            "SECTION_ELEMENT_CODE" => "",
            "DEPTH" => "2",
            "PRODUCT_QUANTITY_VARIABLE" => "quantity",
            "PROPERTY_CODE_2" => array(),
            "CART_PROPERTIES_2" => array(),
            "ADDITIONAL_PICT_PROP_2" => "",
            "LABEL_PROP_2" => "",
            "PROPERTY_CODE_3" => array(""),
            "CART_PROPERTIES_3" => array(""),
            "ADDITIONAL_PICT_PROP_3" => "",
            "OFFER_TREE_PROPS_3" => array("")
        )
    );?>
<? endif; ?>