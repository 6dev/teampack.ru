$(document).ready(function () {
    var popupContainer = $('.popup-container');
    $(document).mouseup(function (e){
        var div = $(".popup-content");
        if (!div.is(e.target)
            && div.has(e.target).length === 0) {
            popupContainer.hide();
        }
    });

    $('#delivery-info-link').click(function(){
        $('.delivery-popup').show();
    });

    $('#pay-info-link').click(function(){
        $('.pay-popup').show();
    });

    $('input').click(function(){
        $('.quanity-attention').hide();
        $(this).parents('form').find(".col-block").css('border', 'none');
    });
});

function showQuestion(element_name) {
    $("input[name$='form_text_4']").val(element_name);
    $("input[name$='form_text_4']").attr('disabled', 'disabled');
    $('.question-popup').show();
}

function oneclickBuyShow(element_id, min, step, value) {
    $('#oneclick-form').show();
    $('#oneclick-form input[type="text"]').val('');
    $('#oneclick-form input[type="email"]').val('');
    $('#oneclick-form input[type="phone"]').val('');
    $('#oneclick-results').html('');
    $('#element_id_input').val(element_id);
    $('#oneclick-quanity').data('min', min);
    $('#oneclick-quanity').data('step', step);
    $('#oneclick-quanity').val(value);
    $('.oneclick-buy-container').show();
    return false;
}

function closePopup() {
    $('.popup-container').hide();
}

function call() {

    if($('#oneclick-form').valid()) {
        var msg = $('#oneclick-form').serialize();
        $.ajax({
            type: 'POST',
            url: '/ajax/oneclick_buy.php',
            data: msg,
            success: function (data) {
                yaCounter29171335.reachGoal('bye_one_click');
                $('#oneclick-form').hide();
                $('#oneclick-results').html(data);
            },
            error: function (xhr, str) {
                alert('Возникла ошибка: ' + xhr.responseCode);
            }
        });
    }
}