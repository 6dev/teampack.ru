<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
}
/**
 * @var \victory\helpers\Sort   $sort
 * @var \victory\helpers\Paging $paging
 */
$sort = $arParams['sort'];
$paging = $arParams['paging'];
?>

<? if (count($arResult['ITEMS']) > 0): ?>
    <div class="sort-block clearfix">
        <div class="sort-right">
            <?= $arResult['NAV_STRING'] ?>
            <div class="show">
                <span class="legend">Показывать по:</span>
                <? foreach ($paging->fields as $field): ?>
                    <? if ($field['active']): ?>
                        <span><?= $field['label'] ?></span>
                    <? else: ?>
                        <a href="<?= $field['link'] ?>"><?= $field['label'] ?></a>
                    <? endif; ?>
                <? endforeach; ?>
            </div>
        </div>
        <div class="sort-left">
            <div class="sort">
                <span class="legend">Сортировать по:</span>
                <? foreach ($sort->fields as $field): ?>
                    <a href="<?= $field['link'] ?>" class="<?= $field['active'] ? 'active' : ''; ?> <?= $field['order'] ?>"><?= $field['label'] ?></a>
                <? endforeach; ?>
            </div>
        </div>
    </div>


    <div class="catalog-list clearfix">
        <? foreach ($arResult['ITEMS'] as $arItem): ?>
            <div class="catalog-item ib">
                <i class="bant"></i>

                <div class="cat-item-left">
                    <div class="picture"><a href="<?= $arItem['DETAIL_PAGE_URL'] ?>"><img src="<?= $arItem['ICON'] ?>"></a></div>
                </div>
                <div class="desc">
                    <h3><a href="<?= $arItem['DETAIL_PAGE_URL'] ?>"><?= $arItem['NAME'] ?></a></h3>

                    <p>Артикул: <?= CatalogMapper::getPropertyValue($arItem, 'article'); ?><br/>
                        <? if (!$arResult['SHOW_ONLY']): ?>
                            <? if (CatalogMapper::getPropertyValue($arItem, 'capacity') > 1): ?>
                                Вес вложения: <?= CatalogMapper::getPropertyValue($arItem, 'capacity'); ?> г
                            <? endif; ?>
                        <? endif; ?>
                    </p>

                    <? if (!$arResult['SHOW_ONLY']): ?>

                        <? if (count($arItem['PRICES']) > 0): ?>
                            <form method="post">
                                <input name="basket[id]" type="hidden" value="<?= $arItem['ID'] ?>"/>

                                <div class="col">
                                    <div class="col-block">
                                        <input name="basket[quantity]" type="text"
                                               value=""
                                               data-min="<?= $arItem['PROPERTIES']['KOLICHESTVO_V_KOROBKE_NOMENKLATURA']['VALUE'] ?>"
                                               data-step="<?= $arItem['PROPERTIES']['KOLICHESTVO_V_KOROBKE_NOMENKLATURA']['VALUE'] ?>"/>
                                        <a href="#" class="plus" title="добавить"></a>
                                        <a href="#" class="minus" title="убрать"></a>
                                    </div>
                                    <div class="col-legend">

                                    </div>
                                </div>
                                <div class="button"><a href="#" class="vm-basket-add">В корзину</a></div>
                                <? $ib = inBasket($arItem['ID']); ?>
                                <? if ($ib > 0): ?>
                                    <div class="in-cart">
                                        <input type="hidden" name="basket[delete][<?= $arItem['ID'] ?>]" value="Y"/>
                                        В корзине: <?= $ib ?> <a href="#" data-id="<?= $arItem['ID'] ?>"><img src="/assets/images/ico-del.png" alt="Удалить"></a>
                                    </div>
                                <? endif; ?>
                            </form>
                        <? endif; ?>

                    <? endif; ?>

                </div>

                <? if (!$arResult['SHOW_ONLY']): ?>
                    <? $cnt = count($arItem['PRICES']); ?>
                    <? if ($cnt > 0): ?>
                        <div class="price-block">
                            <div class="name">Цена
                                <? if ($arItem['ALL_PRICE'] && $cnt > 1): ?>
                                    <span class="ico-q ib"><span class="popup">цена рассчитывается от суммы заказа по всему ассортименту<i class="arrow"></i><i class="close"></i></span></span>
                                <? endif; ?>
                            </div>
                            <table class="price-matrix">
                                <? foreach ($arItem['PRICES'] as $code => $price): ?>
                                    <tr>
                                        <td><strong>
                                                <nobr><?= $price['PRINT_DISCOUNT_VALUE'] ?></nobr>
                                            </strong></td>
                                        <td>
                                            <? if ($arItem['ALL_PRICE'] && $cnt > 1): ?>
                                                <nobr><?= CatalogMapper::getPriceTypeNameByCode($code) ?></nobr>
                                            <? endif; ?>
                                        </td>
                                    </tr>
                                    <? if (!$arItem['ALL_PRICE']): ?>
                                        <? break; ?>
                                    <? endif; ?>
                                <? endforeach; ?>
                            </table>
                        </div>
                    <? endif; ?>
                <? endif; ?>
                <div class="icons">
                    <? if (CatalogMapper::getPropertyValue($arItem, 'novelty')): ?>
                        <i class="ico-new"></i>
                    <? endif; ?>
                    <? if (CatalogMapper::getPropertyValue($arItem, 'sale')): ?>
                        <i class="ico-rasp"></i>
                    <? endif; ?>
                    <? if (CatalogMapper::getPropertyValue($arItem, 'discount')): ?>
                        <i class="ico-discont"></i>
                    <? endif; ?>
                    <? if (CatalogMapper::getPropertyValue($arItem, 'hit')): ?>
                        <i class="ico-hit"></i>
                    <? endif; ?>
                    <? if ($arItem['PROPERTIES']['SYMBOL']['VALUE']): ?>
                        <i class="ico-year"></i>
                    <? endif; ?>
                    <? if ($arItem['PROPERTIES']['SOLD']['VALUE']): ?>
                        <a href="<?= $arItem['DETAIL_PAGE_URL'] ?>"><i class="ico-sold"></i></a>
                    <? endif; ?>
                </div>
            </div>
        <? endforeach; ?>

    </div>

    <div class="sort-block clearfix">
        <div class="sort-right">
            <?= $arResult['NAV_STRING'] ?>
            <div class="show">
                <span class="legend">Показывать по:</span>
                <? foreach ($paging->fields as $field): ?>
                    <? if ($field['active']): ?>
                        <span><?= $field['label'] ?></span>
                    <? else: ?>
                        <a href="<?= $field['link'] ?>"><?= $field['label'] ?></a>
                    <? endif; ?>
                <? endforeach; ?>
            </div>
        </div>
        <div class="sort-left">
            <div class="sort">
                <span class="legend">Сортировать по:</span>
                <? foreach ($sort->fields as $field): ?>
                    <a href="<?= $field['link'] ?>" class="<?= $field['active'] ? 'active' : ''; ?> <?= $field['order'] ?>"><?= $field['label'] ?></a>
                <? endforeach; ?>
            </div>
        </div>
    </div>
<? else: ?>
    В разделе нет товаров, <a href="/catalog/">посмотреть все разделы</a>
<? endif; ?>


