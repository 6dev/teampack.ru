<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
}
use victory\helpers\Image;
use victory\helpers\IBlockHelper;


$section = CIBlockSection::GetByID($arResult['ID'])->Fetch();
$sectionId = $section['IBLOCK_SECTION_ID'];
while ($sectionId) {
    $section = CIBlockSection::GetByID($sectionId)->Fetch();
    $sectionId = $section['IBLOCK_SECTION_ID'];
}
$arResult['SHOW_ONLY'] = IBlockHelper::getUserField('IBLOCK_2_SECTION', $section['ID'], 'UF_SHOW_ONLY') == 1;
$prices = array();
$db_res = CSaleDiscount::GetList(array("SORT" => "ASC"), array("LID" => SITE_ID, "ACTIVE" => "Y"));
while ($ar_res = $db_res->Fetch()) {
    $sd = CSaleDiscount::GetByID($ar_res['ID']);
    $saleConfig = unserialize($sd['ACTIONS']);
    $ar_res['percent'] = floatval($saleConfig['CHILDREN'][0]['DATA']['Value']);
    $ar_res['PRINT_DISCOUNT_VALUE'] = FormatCurrency($arResult['MIN_PRICE']['VALUE'] * (1 - $ar_res['percent'] / 100), 'RUB');
    $prices[] = $ar_res;
}
$arHolidays = array();
foreach ($arResult['ITEMS'] as $key => $arItem) {
    $filePicture = ($arItem['DETAIL_PICTURE']['SRC'] ? $arItem['DETAIL_PICTURE']['SRC'] : '/assets/images/sub-no-photo.png');
    $arItem['ICON'] = Image::open($filePicture)->thumbnail(104, 120)->save();
    foreach ($prices as $p) {
        $p['PRINT_DISCOUNT_VALUE'] = FormatCurrency($arItem['MIN_PRICE']['VALUE'] * (1 - $p['percent'] / 100), 'RUB');
        $arItem['prices'][] = $p;
    }
    if ($arItem['PROPERTIES']['holiday']['VALUE']) {
        if (!$arHolidays[$arItem['PROPERTIES']['holiday']['VALUE']]) {
            $arHolidays[$arItem['PROPERTIES']['holiday']['VALUE']] = GetIBlockElement($arItem['PROPERTIES']['holiday']['VALUE']);
            $arHolidays[$arItem['PROPERTIES']['holiday']['VALUE']]['ICON'] = CFile::GetPath($arHolidays[$arItem['PROPERTIES']['holiday']['VALUE']]['PREVIEW_PICTURE']);
        }
        $arItem['PROPERTIES']['holiday']['ELEMENT'] = $arHolidays[$arItem['PROPERTIES']['holiday']['VALUE']];
    }



    $last = false;
    $arItem['ALL_PRICE'] = false;
    foreach ($arItem['PRICES'] as $arPrice) {
        if (!$last) {
            $last = $arPrice['PRINT_DISCOUNT_VALUE'];
        }
        if ($last != $arPrice['PRINT_DISCOUNT_VALUE']) {
            $arItem['ALL_PRICE'] = true;
            break;
        }
    }

    $arResult['ITEMS'][$key] = $arItem;
}

if ($arParams['SECTION_NOT_FILTER_ID'] && $arParams['SECTION_NOT_FILTER_ID'] != $arParams['SECTION_ID']) {
    $arSection = GetIBlockSection($arParams['SECTION_NOT_FILTER_ID']);
    $arResult['NAME'] = $arSection['NAME'];
    $APPLICATION->AddChainItem($arSection['NAME']);
    $arResult['DESCRIPTION'] = $arSection['DESCRIPTION'];
}