<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
}
//define('LEFTSIDE_HIDE', true);

if(!defined('IS_SECTION')) define('IS_SECTION', true);

use victory\helpers\Sort;
use victory\helpers\Paging;

$arSection = CIBlockSection::GetByID($arResult["VARIABLES"]["SECTION_ID"])->Fetch();
//print_r($arSection); 
?>
    

<?

  $arFilter = Array('IBLOCK_ID'=>$arParams[IBLOCK_ID],"ID"=>$arResult["VARIABLES"]["SECTION_ID"]);
  $db_list = CIBlockSection::GetList(Array($by=>$order), $arFilter, false,array("UF_*"));
  while($ar_result = $db_list->GetNext())
  {
$d1=$ar_result['DESCRIPTION'];
$d2=$ar_result['UF_TEXT'];
  }



?>



<?
if ($_GET['set_filter'] != 'y') {
    $APPLICATION->IncludeComponent(
        "bitrix:catalog.section.list",
        "box",
        array(
            "IBLOCK_TYPE"         => $arParams["IBLOCK_TYPE"],
            "IBLOCK_ID"           => $arParams["IBLOCK_ID"],
            "SECTION_ID"          => $arResult["VARIABLES"]["SECTION_ID"],
            "SECTION_CODE"        => $arResult["VARIABLES"]["SECTION_CODE"],
            "CACHE_TYPE"          => $arParams["CACHE_TYPE"],
            "CACHE_TIME"          => $arParams["CACHE_TIME"],
            "CACHE_GROUPS"        => $arParams["CACHE_GROUPS"],
            "COUNT_ELEMENTS"      => $arParams["SECTION_COUNT_ELEMENTS"],
            "TOP_DEPTH"           => $arParams["SECTION_TOP_DEPTH"],
            "SECTION_URL"         => $arResult["FOLDER"] . $arResult["URL_TEMPLATES"]["section"],
            "VIEW_MODE"           => $arParams["SECTIONS_VIEW_MODE"],
            "SHOW_PARENT_NAME"    => $arParams["SECTIONS_SHOW_PARENT_NAME"],
            "HIDE_SECTION_NAME"   => (isset($arParams["SECTIONS_HIDE_SECTION_NAME"]) ? $arParams["SECTIONS_HIDE_SECTION_NAME"] : "N"),
            "ADD_SECTIONS_CHAIN"  => (isset($arParams["ADD_SECTIONS_CHAIN"]) ? $arParams["ADD_SECTIONS_CHAIN"] : ''),
            "SECTION_FIELDS"      => array(
                0 => "",
                1 => "PICTURE",
                2 => "",
            ),
            "SECTION_USER_FIELDS" => array(
                0 => "UF_SYNC",
                1 => "UF_FILTER",
                2 => "UF_LINK",
                3 => "UF_HIDE",
            ),
            "CURRENT_LEVEL"       => $arSection['DEPTH_LEVEL'] + 1
        ),
        $component
    );
}

?>


<?


global ${$arParams["FILTER_NAME"]};
$arrFilter = ${$arParams["FILTER_NAME"]};
$sectionId = $arResult["VARIABLES"]["SECTION_ID"];
if ($arResult["VARIABLES"]["SECTION_CODE"] == 'simvol-novogo-goda') {

    $arrFilter['PROPERTY_SYMBOL_VALUE'] = 'Y';
    $arResult["VARIABLES"]["SECTION_CODE"] = false;
    $arResult["VARIABLES"]["SECTION_ID"] = false;
}

$this->SetViewTarget('smart');

$APPLICATION->IncludeComponent("bitrix:catalog.smart.filter", "left", array(
        "IBLOCK_TYPE" => "1c_catalog",
        "IBLOCK_ID" => "2",
        //"SECTION_ID" => $sectionId,
        "FILTER_NAME" => "arrFilter",
        "HIDE_NOT_AVAILABLE" => "N",
        "CACHE_TYPE" => "A",
        "CACHE_TIME" => "36000000",
        "CACHE_GROUPS" => "Y",
        "SAVE_IN_SESSION" => "N",
        "INSTANT_RELOAD" => "N",
        "PRICE_CODE" => array(
            0 => "Розница до 10 тыс руб",
        ),
        "XML_EXPORT" => "N",
        "SECTION_TITLE" => "-",
        "SECTION_DESCRIPTION" => "-"
    ),
    false
);
$this->EndViewTarget();


$sort = new Sort('sort', 'order', 'novelty', 'desc');
$sort->add('novelty', 'PROPERTY_novelty', 'новизне');
$sort->add('price', 'CATALOG_PRICE_4', 'цене');
$sort->add('weight', 'PROPERTY_VES_VLOZHENIYA_G_NOMENKLATURA', 'весу вложения');

$paging = new Paging('count', 12);
$paging->add(12, 12);
$paging->add(30, 30);
$paging->add(120, 120);

?>

<?if(isset($d2) && trim($d2)):?>
<div class="folder-text pink">
<?= $d2; ?>
</div> <br>
<?endif;?>

<?

$intSectionID = $APPLICATION->IncludeComponent(
    "seoexp:catalog.section",
    'catalog',
    array(
        "IBLOCK_TYPE"                     => $arParams["IBLOCK_TYPE"],
        "IBLOCK_ID"                       => $arParams["IBLOCK_ID"],
        "ELEMENT_SORT_FIELD2"             => $sort->getField(),
        "ELEMENT_SORT_ORDER2"             => $sort->getOrder(),
        "ELEMENT_SORT_FIELD"              => $arParams["ELEMENT_SORT_FIELD"],
        "ELEMENT_SORT_ORDER"              => $arParams["ELEMENT_SORT_ORDER"],
        "ELEMENT_SORT_FIELD3"              => 'name',
        "ELEMENT_SORT_ORDER3"              => 'asc',
        "PROPERTY_CODE"                   => $arParams["LIST_PROPERTY_CODE"],
        "META_KEYWORDS"                   => $arParams["LIST_META_KEYWORDS"],
        "META_DESCRIPTION"                => $arParams["LIST_META_DESCRIPTION"],
        "BROWSER_TITLE"                   => $arParams["LIST_BROWSER_TITLE"],
        "INCLUDE_SUBSECTIONS"             => $arParams["INCLUDE_SUBSECTIONS"],
        "BASKET_URL"                      => $arParams["BASKET_URL"],
        "ACTION_VARIABLE"                 => $arParams["ACTION_VARIABLE"],
        "PRODUCT_ID_VARIABLE"             => $arParams["PRODUCT_ID_VARIABLE"],
        "SECTION_ID_VARIABLE"             => $arParams["SECTION_ID_VARIABLE"],
        "PRODUCT_QUANTITY_VARIABLE"       => $arParams["PRODUCT_QUANTITY_VARIABLE"],
        "PRODUCT_PROPS_VARIABLE"          => $arParams["PRODUCT_PROPS_VARIABLE"],
        "FILTER_NAME"                     => $arParams["FILTER_NAME"],
        "CACHE_TYPE"                      => $arParams["CACHE_TYPE"],
        "CACHE_TIME"                      => $arParams["CACHE_TIME"],
        "CACHE_FILTER"                    => $arParams["CACHE_FILTER"],
        "CACHE_GROUPS"                    => $arParams["CACHE_GROUPS"],
        "SET_TITLE"                       => $arParams["SET_TITLE"],
        "SET_STATUS_404"                  => $arParams["SET_STATUS_404"],
        "DISPLAY_COMPARE"                 => $arParams["USE_COMPARE"],
        "PAGE_ELEMENT_COUNT"              => $paging->getCount(),
        "LINE_ELEMENT_COUNT"              => $arParams["LINE_ELEMENT_COUNT"],
        "PRICE_CODE"                      => $arParams["PRICE_CODE"],
        "USE_PRICE_COUNT"                 => $arParams["USE_PRICE_COUNT"],
        "SHOW_PRICE_COUNT"                => $arParams["SHOW_PRICE_COUNT"],
        "PRICE_VAT_INCLUDE"               => $arParams["PRICE_VAT_INCLUDE"],
        "USE_PRODUCT_QUANTITY"            => $arParams['USE_PRODUCT_QUANTITY'],
        "ADD_PROPERTIES_TO_BASKET"        => (isset($arParams["ADD_PROPERTIES_TO_BASKET"]) ? $arParams["ADD_PROPERTIES_TO_BASKET"] : ''),
        "PARTIAL_PRODUCT_PROPERTIES"      => (isset($arParams["PARTIAL_PRODUCT_PROPERTIES"]) ? $arParams["PARTIAL_PRODUCT_PROPERTIES"] : ''),
        "PRODUCT_PROPERTIES"              => $arParams["PRODUCT_PROPERTIES"],
        "DISPLAY_TOP_PAGER"               => $arParams["DISPLAY_TOP_PAGER"],
        "DISPLAY_BOTTOM_PAGER"            => $arParams["DISPLAY_BOTTOM_PAGER"],
        "PAGER_TITLE"                     => $arParams["PAGER_TITLE"],
        "PAGER_SHOW_ALWAYS"               => $arParams["PAGER_SHOW_ALWAYS"],
        "PAGER_TEMPLATE"                  => $arParams["PAGER_TEMPLATE"],
        "PAGER_DESC_NUMBERING"            => $arParams["PAGER_DESC_NUMBERING"],
        "PAGER_DESC_NUMBERING_CACHE_TIME" => $arParams["PAGER_DESC_NUMBERING_CACHE_TIME"],
        "PAGER_SHOW_ALL"                  => $arParams["PAGER_SHOW_ALL"],
        "OFFERS_CART_PROPERTIES"          => $arParams["OFFERS_CART_PROPERTIES"],
        "OFFERS_FIELD_CODE"               => $arParams["LIST_OFFERS_FIELD_CODE"],
        "OFFERS_PROPERTY_CODE"            => $arParams["LIST_OFFERS_PROPERTY_CODE"],
        "OFFERS_SORT_FIELD"               => $arParams["OFFERS_SORT_FIELD"],
        "OFFERS_SORT_ORDER"               => $arParams["OFFERS_SORT_ORDER"],
        "OFFERS_SORT_FIELD2"              => $arParams["OFFERS_SORT_FIELD2"],
        "OFFERS_SORT_ORDER2"              => $arParams["OFFERS_SORT_ORDER2"],
        "OFFERS_LIMIT"                    => $arParams["LIST_OFFERS_LIMIT"],
        "SECTION_ID"                      => $arResult["VARIABLES"]["SECTION_ID"],
        "SECTION_CODE"                    => $arResult["VARIABLES"]["SECTION_CODE"],
        "SECTION_URL"                     => $arResult["FOLDER"] . $arResult["URL_TEMPLATES"]["section"],
        "DETAIL_URL"                      => $arResult["FOLDER"] . $arResult["URL_TEMPLATES"]["element"],
        'CONVERT_CURRENCY'                => $arParams['CONVERT_CURRENCY'],
        'CURRENCY_ID'                     => $arParams['CURRENCY_ID'],
        'HIDE_NOT_AVAILABLE'              => $arParams["HIDE_NOT_AVAILABLE"],
        'LABEL_PROP'                      => $arParams['LABEL_PROP'],
        'ADD_PICT_PROP'                   => $arParams['ADD_PICT_PROP'],
        'PRODUCT_DISPLAY_MODE'            => $arParams['PRODUCT_DISPLAY_MODE'],
        'OFFER_ADD_PICT_PROP'             => $arParams['OFFER_ADD_PICT_PROP'],
        'OFFER_TREE_PROPS'                => $arParams['OFFER_TREE_PROPS'],
        'PRODUCT_SUBSCRIPTION'            => $arParams['PRODUCT_SUBSCRIPTION'],
        'SHOW_DISCOUNT_PERCENT'           => $arParams['SHOW_DISCOUNT_PERCENT'],
        'SHOW_OLD_PRICE'                  => $arParams['SHOW_OLD_PRICE'],
        'MESS_BTN_BUY'                    => $arParams['MESS_BTN_BUY'],
        'MESS_BTN_ADD_TO_BASKET'          => $arParams['MESS_BTN_ADD_TO_BASKET'],
        'MESS_BTN_SUBSCRIBE'              => $arParams['MESS_BTN_SUBSCRIBE'],
        'MESS_BTN_DETAIL'                 => $arParams['MESS_BTN_DETAIL'],
        'MESS_NOT_AVAILABLE'              => $arParams['MESS_NOT_AVAILABLE'],
        'TEMPLATE_THEME'                  => (isset($arParams['TEMPLATE_THEME']) ? $arParams['TEMPLATE_THEME'] : ''),
        "ADD_SECTIONS_CHAIN"              => "N",
        'sort'                            => $sort,
        'paging'                          => $paging,
        "SHOW_ALL_WO_SECTION"             => "Y",
        'SECTION_NOT_FILTER_ID'           => $sectionId
    ),
    $component
);


?>
<?if(isset($d1) && trim($d1)):?>
<div class="folder-text pink">
<?= $d1; ?>
</div> <br>
<?endif;?>