<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
}

if ($_GET['set_filter'] == 'y') {

    global $arrFilter;

    if ($_GET['text']) {
        $filter = array();
        $filter['PROPERTY_CML2_ARTICLE'] = '%' . $_GET['text'] . '%';
        $filter['NAME'] = '%' . $_GET['text'] . '%';
        $arrFilter[] = array(
            'LOGIC' => 'OR',
            array('PROPERTY_CML2_ARTICLE' => '%' . $_GET['text'] . '%'),
            array('NAME' => '%' . $_GET['text'] . '%')
        );
    }
    include 'section.php';
} else {

    $this->SetViewTarget('smart');
    $APPLICATION->IncludeFile('/includes/smart.php');
    $this->EndViewTarget();

    $APPLICATION->IncludeComponent(
        "bitrix:catalog.section.list",
        "box",
        array(
            "IBLOCK_TYPE"         => $arParams["IBLOCK_TYPE"],
            "IBLOCK_ID"           => $arParams["IBLOCK_ID"],
            "CACHE_TYPE"          => $arParams["CACHE_TYPE"],
            "CACHE_TIME"          => $arParams["CACHE_TIME"],
            "CACHE_GROUPS"        => $arParams["CACHE_GROUPS"],
            "COUNT_ELEMENTS"      => $arParams["SECTION_COUNT_ELEMENTS"],
            "TOP_DEPTH"           => 1,//$arParams["SECTION_TOP_DEPTH"],
            "SECTION_URL"         => $arResult["FOLDER"] . $arResult["URL_TEMPLATES"]["section"],
            "VIEW_MODE"           => $arParams["SECTIONS_VIEW_MODE"],
            "SHOW_PARENT_NAME"    => $arParams["SECTIONS_SHOW_PARENT_NAME"],
            "HIDE_SECTION_NAME"   => (isset($arParams["SECTIONS_HIDE_SECTION_NAME"]) ? $arParams["SECTIONS_HIDE_SECTION_NAME"] : "N"),
            "ADD_SECTION_CHAIN"   => (isset($arParams["ADD_SECTION_CHAIN"]) ? $arParams["ADD_SECTION_CHAIN"] : ''),
            "SECTION_FIELDS"      => array(
                0 => "",
                1 => "PICTURE",
                2 => "",
            ),
            "SECTION_USER_FIELDS" => array(
                0 => "UF_SYNC",
                1 => "UF_FILTER",
                2 => "UF_LINK",
                3 => "UF_HIDE",
                4=>'UF_IS_TEXT'
            ),
            'HIDE_FILTER'         => 'Y',
            'CURRENT_LEVEL'       => 1
        ),
        $component
    );
}