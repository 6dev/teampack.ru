<?php
/**
 * @author Alexandr Kirshin <kirshin.as@gmail.com>
 */
?>
<div class="catalog-menu <? if (defined('CATALOG_HIDE') && CATALOG_HIDE === true): ?>title-hide<? endif; ?>">

    <? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
        die();
    } ?>
    <? if (!empty($arResult)): ?>

        <? $level = 0; ?>
        <? $hide = false; ?>
        <? foreach ($arResult as $n => $arItem): ?>

            <? if ($arItem['DEPTH_LEVEL'] == $level): ?>
                <?= '</li>'; ?>
            <? elseif ($arItem['DEPTH_LEVEL'] > $level): ?>
                <?= '<ul ' . ($hide ? 'style="display:none;"' : '') . '>'; ?>
            <?
            else: ?>
                <?= '</li>'; ?>
                <? for ($i = $level - $arItem['DEPTH_LEVEL']; $i; $i--): ?>
                    <?= '</ul>'; ?>
                    <?= '</li>'; ?>
                <? endfor; ?>
            <?endif; ?>
            <?= '<li ' . ($arItem['SELECTED'] ? 'class="active"' : '') . '>' ?>
            <a href="<?= $arItem["LINK"] ?>"><?= $arItem["TEXT"] ?></a>
            <? if ($arItem['IS_PARENT']): ?>
                <span class="open"></span>
            <? endif; ?>
            <? $level = $arItem['DEPTH_LEVEL'] ?>
            <? $hide = $arItem['PARAMS']['UF_HIDE_MENU'] && !$arItem['SELECTED']; ?>
        <? endforeach; ?>
        <? for ($i = $level; $i; $i--): ?>
            <?= '</li>'; ?>
            <?= '</ul>'; ?>
        <? endfor; ?>

    <? endif ?>

    
</div>