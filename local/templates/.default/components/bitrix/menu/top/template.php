<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
} ?>
<? if (!empty($arResult)): ?>
    <nav id="header-menu">
        <? $level = 0; ?>
        <? foreach ($arResult as $n => $arItem): ?>

        <? if ($arItem['DEPTH_LEVEL'] == $level): ?>
            <?= '</li>'; ?>
        <? elseif ($arItem['DEPTH_LEVEL'] > $level): ?>
            <?= '<ul>'; ?>
        <?
        else: ?>
            <?= '</li>'; ?>
            <? for ($i = $level - $arItem['DEPTH_LEVEL']; $i; $i--): ?>
                <?= '</ul>'; ?>
                <?= '</div>'; ?>
                <?= '</li>'; ?>
            <? endfor; ?>
        <?endif; ?>
        <?= '<li class="' . ($arItem['SELECTED'] ? 'active' : '') . ($arItem['IS_PARENT'] ? ' have-sub' : '') . ' '.$arItem['PARAMS']['class'] . '">' ?>
        <? if ($arItem['IS_PARENT']): ?>
        <div>
            <? endif; ?>
            <? if ($arItem['IS_PARENT']): ?>
                <a href="javascript:void(0)" class="not-click"><?= $arItem["TEXT"] ?></a>
            <? else: ?>
                <a href="<?=$arItem["LINK"] ?>"><?= $arItem["TEXT"] ?></a>
            <?endif; ?>
            <? if ($arItem['IS_PARENT']): ?>
                <span class="open"></span>
            <? endif; ?>
            <? $level = $arItem['DEPTH_LEVEL'] ?>
            <? endforeach; ?>
            <? for ($i = $level; $i; $i--): ?>
                <?= '</li>'; ?>
                <?= '</ul>'; ?>
            <? endfor; ?>
    </nav>
<? endif ?>