<?php
/**
 * @author Alexandr Kirshin <kirshin.as@gmail.com>
 */
?>
<div class="catalog-menu <? if (defined('CATALOG_HIDE') && CATALOG_HIDE === true): ?>title-hide<? endif; ?>">

    <div class="search-block title-hide">
        <div class="name">Поиск товаров</div>


        <div class="search-inp clearfix">
            <form name="_form" action="/catalog/" method="get">
                <input type="hidden" name="set_filter" value="y">
                <input name="text" type="text" id="title-search-input">

                <div class="button"><a href="#" class="submit-form-link">Найти</a></div>
            </form>
        </div>

    </div>

    <div class="name title-hide">Каталог товаров</div>

    <? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
        die();
    } ?>
    <? if (!empty($arResult)): ?>

        <? $level = 0; ?>
        <? foreach ($arResult as $n => $arItem): ?>
            <? if ($arItem['DEPTH_LEVEL'] == $level): ?>
                <?= '</li>'; ?>
            <? elseif ($arItem['DEPTH_LEVEL'] > $level): ?>
                <?= '<ul>'; ?>
                <?
            else: ?>
                <?= '</li>'; ?>
                <? for ($i = $level - $arItem['DEPTH_LEVEL']; $i; $i--): ?>
                    <?= '</ul>'; ?>
                    <?= '</li>'; ?>
                <? endfor; ?>
            <?endif; ?>
            <? $hide = $arItem['PARAMS']['UF_HIDE_MENU']; ?>
            <!--            --><?//= '<li class="'.($hide?'show-always':'').'">' ?>
            <?= '<li>' ?>
            <a href="<?= $arItem["LINK"] ?>" class="<?=$arItem['SELECTED'] ? 'selected' : ''?>"><?= $arItem["TEXT"] ?></a>
            <? if ($arItem['IS_PARENT']): ?>
                <span class="open"></span>
            <? endif; ?>
            <? $level = $arItem['DEPTH_LEVEL'] ?>
        <? endforeach; ?>
        <? for ($i = $level; $i; $i--): ?>
            <?= '</li>'; ?>
            <?= '</ul>'; ?>
        <? endfor; ?>

    <? endif ?>

    <div class="button title-hide"><? $APPLICATION->IncludeFile('/includes/price.php'); ?></div>
</div>