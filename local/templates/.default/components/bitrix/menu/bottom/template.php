<?php
/**
 * @author Alexandr Kirshin <kirshin.as@gmail.com>
 */
$open = false;
?>
<!-- FOOTER MENU -->
<nav id="footer-menu">
    <? foreach ($arResult as $n => $arItem): ?>
        <?if ($arItem['DEPTH_LEVEL'] == 1) {
            if ($open) {
                echo '</ul>';
            }
            $open = true;
            echo '<ul>';
        }
        ?>
        <li class="<?=$arItem['DEPTH_LEVEL'] == 1?'bold':'';?>"><a href="<?=$arItem['LINK']?>"><?=$arItem['TEXT']?></a></li>
    <? endforeach; ?>
    <?
    if ($open) {
        echo '</ul>';
    }
    ?>
</nav>
<!-- END FOOTER MENU -->
