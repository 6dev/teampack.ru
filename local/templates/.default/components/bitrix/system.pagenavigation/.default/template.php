<?php
/**
 * @author Alexandr Kirshin <kirshin.as@gmail.com>
 */

if (!$arResult["NavShowAlways"]) {
    if ($arResult["NavRecordCount"] == 0 || ($arResult["NavPageCount"] == 1 && $arResult["NavShowAll"] == false)) {
        return;
    }
}
$strNavQueryString = ($arResult["NavQueryString"] != "" ? $arResult["NavQueryString"] . "&amp;" : "");
$strNavQueryStringFull = ($arResult["NavQueryString"] != "" ? "?" . $arResult["NavQueryString"] : "");
// to show always first and last pages
$arResult["nStartPage"] = 1;
$arResult["nEndPage"] = $arResult["NavPageCount"];

$firstDisabled = true;
$prevDisabled = true;
$nextDisabled = true;
$lastDisabled = true;

$sFirstHref = $arResult["sUrlPath"] . '?' . $strNavQueryString . 'PAGEN_' . $arResult["NavNum"] . '=' . 1;
$sPrevHref = '';
$sNextHref = '';
$sLastHref = $arResult["sUrlPath"] . '?' . $strNavQueryString . 'PAGEN_' . $arResult["NavNum"] . '=' . $arResult["nEndPage"];
if ($arResult["NavPageNomer"] > 1) {
    $firstDisabled = !($arResult["NavPageNomer"] > 2);
    $prevDisabled = false;
    if ($arResult["bSavePage"] || $arResult["NavPageNomer"] > 2) {
        $sPrevHref = $arResult["sUrlPath"] . '?' . $strNavQueryString . 'PAGEN_' . $arResult["NavNum"] . '=' . ($arResult["NavPageNomer"] - 1);
    } else {
        $sPrevHref = $arResult["sUrlPath"] . $strNavQueryStringFull;
    }
}

if ($arResult["NavPageNomer"] < $arResult["NavPageCount"]) {
    $lastDisabled = ($arResult["NavPageCount"] - $arResult["NavPageNomer"]) < 2;
    $nextDisabled = false;
    $sNextHref = $arResult["sUrlPath"] . '?' . $strNavQueryString . 'PAGEN_' . $arResult["NavNum"] . '=' . ($arResult["NavPageNomer"] + 1);
}

?>
<div class="paginator">
    <span class="legend">Страницы:</span>
    <? if (!$firstDisabled): ?>
        <a href="<?= $sFirstHref ?>" class="first"></a>
    <? endif; ?>
    <? if (!$prevDisabled): ?>
        <a href="<?= $sPrevHref ?>" class="prev"></a>
    <? endif; ?>
    <?
    $bFirst = true;
    $bPoints = false;

    do {
        ?>
        <? if ($arResult["nStartPage"] <= 2 || $arResult["nEndPage"] - $arResult["nStartPage"] <= 1 || abs($arResult['nStartPage'] - $arResult["NavPageNomer"]) <= 2) : ?>
            <? if ($arResult["nStartPage"] == $arResult["NavPageNomer"]): ?>
                <span class="current"><?= $arResult["NavPageNomer"] ?></span>
            <?
            elseif ($arResult["nStartPage"] == 1 && $arResult["bSavePage"] == false):
                ?>
                <a href="<?= $arResult["sUrlPath"] ?><?= $strNavQueryStringFull ?>"><?= $arResult["nStartPage"] ?></a>
            <?
            else: ?>
                <a href="<?= $arResult["sUrlPath"] ?>?<?= $strNavQueryString ?>PAGEN_<?= $arResult["NavNum"] ?>=<?= $arResult["nStartPage"] ?>"><?= $arResult["nStartPage"] ?></a>
            <?endif; ?>
            <?
            $bFirst = false;
            $bPoints = true;
            ?>
        <? else: ?>
            <? if ($bPoints) : ?>
                <span>...</span>
                <? $bPoints = false; ?>
            <? endif; ?>
        <?endif; ?>
        <?
        $arResult["nStartPage"]++;
    } while ($arResult["nStartPage"] <= $arResult["nEndPage"]);
    ?>

    <? if (!$nextDisabled): ?>
        <a href="<?= $sNextHref ?>" class="next"></a>
    <? endif; ?>
    <? if (!$lastDisabled): ?>
        <a href="<?= $sLastHref ?>" class="last"></a>
    <? endif; ?>
</div>