<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
} ?>
<?
use victory\helpers\FormatHelper;

?>
<div id="header-basket">
    <div class="button"><a href="/basket/">Оформить</a></div>
    <div class="basket-icon">
        <a href="/basket/"><img src="/assets/images/ico-basket.png" alt="корзина" /></a>
    </div>
    <p>В корзине <?= $arResult['COUNT'] ?> <?= FormatHelper::formatPlural($arResult['COUNT'], 'товар', 'товара', 'товаров') ?>; всего <?= $arResult['QUANTITY'] ?> шт.<br>
        на сумму <strong><?= $arResult['SUM'] ?></strong> р.</p>

    <div class="popup-block">

    </div>
    <div class="popup-block">
        <div id="callback">
            <form method="post" id="callback-form">
                <table>
                    <tr>
                        <td class="legend">Как Вас зовут?:</td>
                        <td class="input"><input required aria-required="true" name="name" value="" class="required" size="30" type="text"></td>
                    </tr>
                    <tr>
                        <td class="legend">Телефон:</td>
                        <td class="input"><input required aria-required="true" name="phone" value="" class="required phone-mask" size="15" type="tel"></td>
                    </tr>
                    <tr>
                        <td class="legend">Удобное время для звонка:</td>
                        <td class="select">
                            <select name="time">
                                <option value="9:00-11:00">9:00-11:00</option>
                                <option value="11:00-13:00">11:00-13:00</option>
                                <option value="13:00-15:00">13:00-15:00</option>
                                <option value="15:00-17:00">15:00-17:00</option>
                                <option value="17:00-19:00">17:00-19:00</option>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td class="legend"></span></td>
                        <td class="input">
                            <div class="privacy-accept">
                                Нажимая на кнопку "Отправить" Вы соглашаетесь на <a href="/privacy/" style="color: #fff;">обработку персональных данных.</a>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td></td>
                        <td class="input">
                            <div class="g-recaptcha" data-sitekey="6Lc5xxMUAAAAAGzyUjuCoCgJ9ze9Hpc3lZxORdZC"></div>
                            <?php /*
                            <?php $capCode = $GLOBALS["APPLICATION"]->CaptchaGetCode();  ?>
                            <input type="hidden" name="captcha_sid" value="<?= htmlspecialchars($capCode) ?>">
                            <input size="40" value="" name="cap" />
                            <img src="/bitrix/tools/captcha.php?captcha_sid=<?= htmlspecialchars($capCode) ?>" width="180" height="40"><br>
                            */?>
                        </td>
                    </tr>
                    <tr>
                        <td></td>
                        <td class="input"><div class="error-block">Пожалуйста, заполните все поля</div></td>
                    </tr>
                    <tr>
                        <td></td>
                        <td class="button"><a href="#" id="callback-submit-form">Отправить</a></td>
                    </tr>
                </table>
                <div id="hide-callback-main" class="arrow-hide">▲</div>
            </form>
        </div>
    </div>
</div>