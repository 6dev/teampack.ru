<?php
/**
 * @author Alexandr Kirshin <kirshin.as@gmail.com>
 */


$arItems = array();
$arResult['ITEMS'] = DiscountCalculator::getDiscounts($arResult['ITEMS']);

foreach ($arResult['ITEMS'] as $arItem) {
    if ($arItem['CAN_BUY'] != 'Y') {
        continue;
    }
    $arResult['QUANTITY'] += $arItem['QUANTITY'];
    $arResult['SUM'] += $arItem['QUANTITY'] * $arItem['PRICE'];
    $arItems[] = $arItem;
}
$arResult['ITEMS'] = $arItems;
$arResult['QUANTITY'] = number_format($arResult['QUANTITY'], 0, '.', ' ');
$arResult['SUM'] = number_format($arResult['SUM'], 2, '.', ' ');
$arResult['COUNT'] = count($arResult['ITEMS']);