<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
}
/**
 * @var \victory\helpers\Sort   $sort
 * @var \victory\helpers\Paging $paging
 */
$sort = $arParams['sort'];
$paging = $arParams['paging'];
?>
<div class="oneclick-buy-container product-block popup-container">
    <div class="oneclick-buy-content popup-content">
        <div class="close-popup">
            <a onclick="closePopup()"><img src="/assets/images/close.png" /></a>
        </div>
        <form method="POST" id="oneclick-form" action="javascript:void(null);" onsubmit="call()" novalidate>
            <div class="oneclick-form-title">Покупка в один клик</div>
            <div class="form-control">
                <label for="name">ФИО*:</label><input id="oneclick-name" name="name" value="" type="text" required>
            </div>
            <div class="form-control">
                <label for="phone">Телефон*:</label><input class="phone-mask" id="oneclick-phone" name="phone" value="" type="text" required>
            </div>
            <div class="form-control">
                <label for="email">E-mail*:</label><input id=oneclick-email" name="email" value="" type="email" required>
            </div>
            <input type="hidden" name="product" id="element_id_input" value="">
            <div class="col form-control">
                <label for="quantity">Количество:</label>
                <div class="col-block oneclick-col">
                    <input name="quantity" id="oneclick-quanity" type="text"
                           value=""
                           data-min=""
                           data-step="" required/>
                    <a href="#" class="plus" title="добавить"></a>
                    <a href="#" class="minus" title="убрать"></a>
                </div>
                <div class="col-legend">
                </div>
            </div>
            <div class="form-control">
                <div class="privacy-accept">
                    Нажимая на кнопку "Отправить" Вы соглашаетесь на <a href="/privacy/">обработку персональных данных.</a>
                </div>
            </div>
            <div class="form-control">
                <input value="Отправить" class="button" type="submit">
            </div>
        </form>
        <div id="oneclick-results"></div>
    </div>
</div>

<? if (count($arResult['ITEMS']) > 0): ?>
    <div class="sort-block clearfix">
        <div class="sort-right">
            <?= $arResult['NAV_STRING'] ?>
            <div class="show">
                <span class="legend">Показывать по:</span>
                <? foreach ($paging->fields as $field): ?>
                    <? if ($field['active']): ?>
                        <span><?= $field['label'] ?></span>
                    <? else: ?>
                        <a href="<?= $field['link'] ?>"><?= $field['label'] ?></a>
                    <? endif; ?>
                <? endforeach; ?>
            </div>
        </div>
        <div class="sort-left">
            <div class="sort">
                <span class="legend">Сортировать по:</span>
                <? foreach ($sort->fields as $field): ?>
                    <a href="<?= $field['link'] ?>" class="<?= $field['active'] ? 'active' : ''; ?> <?= $field['order'] ?>"><?= $field['label'] ?></a>
                <? endforeach; ?>
            </div>
        </div>
    </div>


    <div class="catalog-list clearfix">
        <? foreach ($arResult['ITEMS'] as $arItem): ?>
            <div class="catalog-item ib">
                <i class="bant"></i>

                <div class="cat-item-left">
                    <div class="picture"><a href="<?= $arItem['DETAIL_PAGE_URL'] ?>"><img src="<?= $arItem['ICON'] ?>"></a></div>
                </div>
                <div class="desc">
                    <h3><a href="<?= $arItem['DETAIL_PAGE_URL'] ?>"><?= $arItem['NAME'] ?></a></h3>

                    <p>Артикул: <?= CatalogMapper::getPropertyValue($arItem, 'article'); ?><br/>
                        <? if (!$arResult['SHOW_ONLY']): ?>
                            <? if (CatalogMapper::getPropertyValue($arItem, 'capacity') > 1): ?>
                                Вес вложения: <?= CatalogMapper::getPropertyValue($arItem, 'capacity'); ?> г
                            <? endif; ?>
                        <? endif; ?>
                    </p>

                    <? if (!$arResult['SHOW_ONLY']): ?>

                        <? if (count($arItem['PRICES']) > 0): ?>
                            <form method="post">
                                <input name="basket[id]" type="hidden" value="<?= $arItem['ID'] ?>"/>

                                <div class="col">
                                    <div class="quanity-attention arrow_box">Укажите количество</div>
                                    <div class="col-block">
                                        <input class="quantity__input" name="basket[quantity]" type="text"
                                               value=""
                                               data-min="<?= $arItem['PROPERTIES']['KOLICHESTVO_V_KOROBKE_NOMENKLATURA']['VALUE'] ?>"
                                               data-step="<?= $arItem['PROPERTIES']['KOLICHESTVO_V_KOROBKE_NOMENKLATURA']['VALUE'] ?>"/>
                                        <a href="#" class="plus" title="добавить"></a>
                                        <a href="#" class="minus" title="убрать"></a>
                                    </div>
                                    <div class="col-legend">

                                    </div>
                                </div>
                                <div class="button"><a href="#" class="vm-basket-add">В корзину</a></div>
                                <div class="button oneclick-list-btn button-basket"><a href="" onclick="return oneclickBuyShow('<?= $arItem['ID'] ?>', '<?= $arItem['PROPERTIES']['KOLICHESTVO_V_KOROBKE_NOMENKLATURA']['VALUE'] ?>', '<?= $arItem['PROPERTIES']['KOLICHESTVO_V_KOROBKE_NOMENKLATURA']['VALUE'] ?>', $(this).closest('form').find('.quantity__input').val())" class="btn-oneclick-buy">Купить в 1 клик</a></div>
                                <? $ib = inBasket($arItem['ID']); ?>
                                <? if ($ib > 0): ?>
                                    <div class="in-cart">
                                        <input type="hidden" name="basket[delete][<?= $arItem['ID'] ?>]" value="Y"/>
                                        В корзине: <?= $ib ?> <a href="#" data-id="<?= $arItem['ID'] ?>"><img src="/assets/images/ico-del.png" alt="Удалить"></a>
                                    </div>
                                <? endif; ?>
                            </form>
                        <? endif; ?>

                    <? endif; ?>

                </div>

                <? if (!$arResult['SHOW_ONLY']): ?>
                    <? $cnt = count($arItem['PRICES']); ?>
                    <? if ($cnt > 0): ?>
                        <div class="price-block">
                            <div class="name">Цена
                                <? if ($arItem['ALL_PRICE'] && $cnt > 1): ?>
                                    <span class="ico-q ib"><span class="popup">цена рассчитывается от суммы заказа по всему ассортименту<i class="arrow"></i><i class="close"></i></span></span>
                                <? endif; ?>
                            </div>
                            <table class="price-matrix">
<?php

if (isset($_GET['adm'])) {
print_r($arItem);
}
 
?>
                                <? foreach ($arItem['PRICES'] as $code => $price): ?>
                                    <tr>
                                        <td><strong>
                                                <nobr><?= $price['PRINT_DISCOUNT_VALUE'] ?></nobr>
                                            </strong></td>
                                        <td>
                                            <? if ($arItem['ALL_PRICE'] && $cnt > 1): ?>
                                                <nobr><?= CatalogMapper::getPriceTypeNameByCode($code) ?></nobr>
                                            <? endif; ?>
                                        </td>
                                    </tr>
                                    <? if (!$arItem['ALL_PRICE']): ?>
                                        <? break; ?>
                                    <? endif; ?>
                                <? endforeach; ?>
                            </table>
                        </div>
                    <? endif; ?>
                <? endif; ?>
                <div class="icons">
                    <? if (CatalogMapper::getPropertyValue($arItem, 'novelty')): ?>
                        <i class="ico-new"></i>
                    <? endif; ?>
                      <? if ((CatalogMapper::getPropertyValue($arItem, 'sale')=='Y') || ($arItem[IBLOCK_SECTION_ID]==436) ) : ?>
                        <i class="ico-rasp"></i>
                    <? endif; ?>
                    <? if (CatalogMapper::getPropertyValue($arItem, 'discount')): ?>
                        <i class="ico-discont"></i>
                    <? endif; ?>
                    <? if (CatalogMapper::getPropertyValue($arItem, 'hit')): ?>
                        <i class="ico-hit"></i>
                    <? endif; ?>
                    <? if ($arItem['PROPERTIES']['SYMBOL']['VALUE']): ?>
                        <i class="ico-year"></i>
                    <? endif; ?>
                    <? if ($arItem['PROPERTIES']['SOLD']['VALUE']): ?>
                        <a href="<?= $arItem['DETAIL_PAGE_URL'] ?>"><i class="ico-sold"></i></a>
                    <? endif; ?>
<?php
$pos = mb_strpos($arItem['NAME'], 'с анимацией', 0, 'UTF-8');
if ($pos!==false) {
echo '<i class="ico-new" style="left:-10px;background: url(/images/4d.png) 0 0 no-repeat;top:-15px;"></i>';
}
?>
                </div>
            </div>
        <? endforeach; ?>

    </div>

    <div class="sort-block clearfix">
        <div class="sort-right">
            <?= $arResult['NAV_STRING'] ?>
            <div class="show">
                <span class="legend">Показывать по:</span>
                <? foreach ($paging->fields as $field): ?>
                    <? if ($field['active']): ?>
                        <span><?= $field['label'] ?></span>
                    <? else: ?>
                        <a href="<?= $field['link'] ?>"><?= $field['label'] ?></a>
                    <? endif; ?>
                <? endforeach; ?>
            </div>
        </div>
        <div class="sort-left">
            <div class="sort">
                <span class="legend">Сортировать по:</span>
                <? foreach ($sort->fields as $field): ?>
                    <a href="<?= $field['link'] ?>" class="<?= $field['active'] ? 'active' : ''; ?> <?= $field['order'] ?>"><?= $field['label'] ?></a>
                <? endforeach; ?>
            </div>
        </div>
    </div>
<? else: ?>
    <?if(!defined('SHOW_ITEMS') || (defined('SHOW_ITEMS') && SHOW_ITEMS)):?>

    В разделе нет товаров, <a href="/catalog/">посмотреть все разделы</a>
        <?endif;?>
<? endif; ?>


