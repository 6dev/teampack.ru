<?php
/**
 * @author Alexandr Kirshin <kirshin.as@gmail.com>
 */


foreach ($arResult["ORDER_PROP"]["USER_PROPS_N"] as $arProperty) {
    $arResult['PROP'][$arProperty['CODE']] = $arProperty;
}
foreach ($arResult["ORDER_PROP"]["USER_PROPS_Y"] as $arProperty) {
    $arResult['PROP'][$arProperty['CODE']] = $arProperty;
}


$required = array('form', 'legal_name', 'inn', 'legal_address', 'rs', 'ks', 'bank', 'bik', 'director');
foreach ($arResult['PROP'] as $code => $property) {
    if (in_array($code, $required)) {
        $arResult['PROP'][$code]['REQUIED'] = 'Y';
    }
}

use victory\helpers\Image;

if ($_GET['clear'] == 'y') {
    CSaleBasket::DeleteAll(CSaleBasket::GetBasketUserID());
    LocalRedirect($APPLICATION->GetCurPage());
}

$quantityAll = 0;
$sumAll = 0;
foreach ($arResult['GRID']['ROWS'] as $key => $arData) {
    $arBasketItem = $arData['data'];
    $quantityAll += $arBasketItem['QUANTITY'];
    $arProduct = GetIBlockElement($arBasketItem['PRODUCT_ID']);
    $filePicture = ($arBasketItem['DETAIL_PICTURE_SRC'] ? $arBasketItem['DETAIL_PICTURE_SRC'] : '/assets/images/sub-no-photo.png');
    $arResult['GRID']['ROWS'][$key] = $arBasketItem;
    $arResult['GRID']['ROWS'][$key]['PRODUCT'] = $arProduct;

    $arResult['GRID']['ROWS'][$key]['ICON'] = Image::open($filePicture)->thumbnail(58, 58)->save();

    $arResult['DISCOUNT_PRICE_PERCENT_FORMATED'] = $arBasketItem['DISCOUNT_PRICE_PERCENT_FORMATED'];
    $sumAll += $arBasketItem['PRICE'] * $arBasketItem['QUANTITY'];
    $basePrice = CPrice::GetBasePrice($arBasketItem['PRODUCT_ID']);
    $arResult['GRID']['ROWS'][$key]['BASE_PRICE'] = $basePrice['PRICE'];

    $discountAll += $arBasketItem['DISCOUNT_PRICE'] * $arBasketItem['QUANTITY'];


}

$arResult['QUANTITY_ALL'] = $quantityAll;

$arResult['PRICE_GROUP'] = CCatalogProductProviderCustom::getCurrentPriceGroup();

list($delivery, $mass) = DeliveryCalculator::calc($_POST['DELIVERY_ID'], $arResult['GRID']['ROWS'], $_POST['ORDER_PROP_26']);
$volume = DeliveryCalculator::calcVolume($arResult['GRID']['ROWS']);

$arResult['SUM'] = $sumAll;
$arResult['SUM_FORMATED'] = CurrencyFormat($sumAll, 'RUB');

$arResult['DELIVERY_FORMATTED'] = CurrencyFormat($delivery, 'RUB');
$arResult['allWeight_FORMATED'] = number_format($mass, 2, '.', ' ') . ' кг.';
$arResult['allSum'] = $sumAll + $delivery;
$arResult['allSum_FORMATED'] = CurrencyFormat($sumAll + $delivery, 'RUB');
$arResult['allVolume_FORMATED'] = number_format($volume, 2, '.', ' ') . ' м<sup>3</sup>';
$arResult['DISCOUNT_PRICE_ALL_FORMATED'] = CurrencyFormat($discountAll, 'RUB');