<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
}

if ($USER->IsAuthorized() || $arParams["ALLOW_AUTO_REGISTER"] == "Y") {
    if ($arResult["USER_VALS"]["CONFIRM_ORDER"] == "Y" || $arResult["NEED_REDIRECT"] == "Y") {
        if (strlen($arResult["REDIRECT_URL"]) > 0) {
            $APPLICATION->RestartBuffer();
            LocalRedirect($arResult["REDIRECT_URL"]);

            die();
        }

    }
}
if (!$USER->IsAuthorized() && $arParams["ALLOW_AUTO_REGISTER"] == "N") {
    if (!empty($arResult["ERROR"])) {
        foreach ($arResult["ERROR"] as $v) {
            echo ShowError($v);
        }
    } elseif (!empty($arResult["OK_MESSAGE"])) {
        foreach ($arResult["OK_MESSAGE"] as $v) {
            echo ShowNote($v);
        }
    }
    LocalRedirect(SITE_DIR);
    include($_SERVER["DOCUMENT_ROOT"].$templateFolder."/auth.php");
} else {
    if ($arResult["USER_VALS"]["CONFIRM_ORDER"] == "Y" || $arResult["NEED_REDIRECT"] == "Y") {
        if (strlen($arResult["REDIRECT_URL"]) == 0) {
            include($_SERVER["DOCUMENT_ROOT"].$templateFolder."/confirm.php");
        }
    } else {
        ?>
        <div id="vm-basket-full">
            <form action="<?= $APPLICATION->GetCurPage(); ?>" method="POST" id="vm-basket-full-form" enctype="multipart/form-data">

                <div class="left-s">
                    <div class="center-s">
                        <? $APPLICATION->IncludeComponent('bitrix:breadcrumb', 'top') ?>
                        <div class="content-block">

                            <h1 class="page-name"><? $APPLICATION->ShowTitle(false) ?></h1>

                            <div class="basket-block clearfix">
                                <div class="table-width-fix">
                                    <table>
                                        <thead>
                                        <tr>
                                            <th>№</th>
                                            <th>Фото</th>
                                            <th>Наименование/ артикул</th>
                                            <th>Цена</th>
                                            <th>Количество</th>
                                            <th>Сумма</th>
                                            <th>Удалить</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <? $p = 1; ?>
                                        <? foreach ($arResult['GRID']['ROWS'] as $arBasketItem): ?>
                                            <tr <? if ($p % 2): ?>class="zebra"<? endif; ?>>
                                                <td class="number"><?= $p ?></td>
                                                <td class="picture"><a href="<?= $arBasketItem['DETAIL_PAGE_URL'] ?>"><img src="<?= $arBasketItem['ICON'] ?>"></a></td>
                                                <td class="name"><a href="<?= $arBasketItem['DETAIL_PAGE_URL'] ?>"><?= $arBasketItem['NAME'] ?></a><br>
                                                    Артикул: <?= $arBasketItem['PRODUCT']['PROPERTIES']['CML2_ARTICLE']['VALUE']; ?>
                                                </td>
                                                <td class="price"><?= $arBasketItem["PRICE_FORMATED"] ?>
                                                    <? if ($arBasketItem['BASE_PRICE'] != $arBasketItem['PRICE']): ?><br/>
                                                        <del><?= CurrencyFormat($arBasketItem['BASE_PRICE'], 'RUB'); ?></del>
                                                    <? endif; ?>
                                                </td>
                                                <td class="col">
                                                    <div class="col-td">
                                                        <div class="col-legend">
                                                            мин. <?= $arBasketItem['PRODUCT']['PROPERTIES']['KOLICHESTVO_V_KOROBKE_NOMENKLATURA']['VALUE'] ?>&nbsp;шт.<br>
                                                            шаг <?= $arBasketItem['PRODUCT']['PROPERTIES']['KOLICHESTVO_V_KOROBKE_NOMENKLATURA']['VALUE'] ?>&nbsp;шт.
                                                        </div>
                                                        <div class="col-block">
                                                            <input class="quantity_input" name="basket[quantity][<?= $arBasketItem["ID"] ?>]" type="text" value="<?= $arBasketItem['QUANTITY'] ?>"
                                                                   data-min="<?= $arBasketItem['PRODUCT']['PROPERTIES']['KOLICHESTVO_V_KOROBKE_NOMENKLATURA']['VALUE'] ?>"
                                                                   data-step="<?= $arBasketItem['PRODUCT']['PROPERTIES']['KOLICHESTVO_V_KOROBKE_NOMENKLATURA']['VALUE'] ?>"/>
                                                            <a href="#" class="plus" title="добавить"></a>
                                                            <a href="#" class="minus" title="убрать"></a>
                                                        </div>
                                                    </div>
                                                </td>
                                                <td class="price-total">
                                                    <?= $arBasketItem['SUM'] ?>
                                                    <? if ($arBasketItem['BASE_PRICE'] != $arBasketItem['PRICE']): ?><br/>
                                                        <del><?= CurrencyFormat($arBasketItem['BASE_PRICE'] * $arBasketItem['QUANTITY'], 'RUB'); ?></del>
                                                    <? endif; ?>
                                                </td>
                                                <td class="delete">
                                                    <input name="basket[delete][<?= $arBasketItem["ID"] ?>]" type="hidden" value="N"/>
                                                    <a href="javascript:void(0)" class="vm-basket-delete"><img src="/assets/images/ico-delete.png" width="32" height="32" alt="#"></a>
                                                </td>
                                            </tr>
                                            <? $p++; ?>
                                        <? endforeach; ?>
                                        </tbody>
                                    </table>
                                </div>
                                <div class="clearfix itogo_and_button">
                                    <div class="itogo-block">
                                        <div class="itogo itogo-small">Всего <?= $arResult['QUANTITY_ALL'] ?> штук.</div>
                                        <br/>

                                        <p>Сумма: <?= $arResult['SUM_FORMATED'] ?> Скидка: <?= $arResult['DISCOUNT_PRICE_ALL_FORMATED'] ?>
                                            <!--(<?= $arResult['DISCOUNT_PRICE_PERCENT_FORMATED']; ?>)--><br>
                                            Масса: <?= $arResult['allWeight_FORMATED'] ?><br/>
                                            Объем: <?= $arResult['allVolume_FORMATED'] ?><br/>
                                            Стоимость доставки: <?= $arResult['DELIVERY_FORMATTED'] ?></p><br/>

                                        <div class="itogo">К оплате: <?= $arResult['allSum_FORMATED'] ?></div>
                                    </div>

                                    <div class="buttons">
                                        <a href="javascript:void(0)" class="basket_recalc">Пересчитать</a>
                                        <a href="/catalog/">Перейти в каталог</a> <a href="?clear=y">Очистить</a>
                                    </div>
                                </div>
                            </div>

                            <div id="order-button"><a href="#">Перейти к оформлению</a></div>

                        </div>
                        <!-- /content-block -->
                    </div>
                    <!-- /center-s -->
                </div>
                <!-- /left-s -->

                <aside class="right-side">
                    <div class="order-block">
                        <div id="order-block">

                            <input type="hidden" name="confirmorder" id="confirmorder" value="Y"/>
                            <input type="hidden" name="profile_change" id="profile_change" value="N"/>
                            <input type="hidden" name="is_ajax_post" id="is_ajax_post" value="N"/>
                            <input type="hidden" name="PERSON_TYPE" value="<?= $arResult["USER_VALS"]["PERSON_TYPE_ID"] ?>"/>
                            <input type="hidden" name="PERSON_TYPE_OLD" value="<?= $arResult["USER_VALS"]["PERSON_TYPE_ID"] ?>"/>
                            <input name="<?= $arResult['PROP']['location']['FIELD_NAME'] ?>" type="hidden" value="<?= $arResult['PROP']['location']['VALUE'] ?>"/>

                            <div class="order-block">
                                <h2>Оформление заказа</h2>
                                <?
                                if (!empty($arResult["ERROR"]) && $arResult["USER_VALS"]["FINAL_STEP"] == "Y") {
                                    foreach ($arResult["ERROR"] as $v) {
                                        echo ShowError($v);
                                    }
                                }
                                ?>
                                <div class="error-block">
                                    Заполните реквизиты для выписки счета.
                                </div>
                                <div class="pay-type">
                                    <div class="name">Способ оплаты</div>
                                    <div class="radio-group">

                                        <? foreach ($arResult['PAY_SYSTEM'] as $arPaySystem): ?>
                                            <div class="radio-item">
                                                <div class="radio"><input name="PAY_SYSTEM_ID" type="radio" class="pay-system" value="<?= $arPaySystem['ID'] ?>"
                                                                          id="ps<?= $arPaySystem['ID'] ?>" <?= $arPaySystem['CHECKED'] ? 'checked' : '' ?>></div>
                                                <div class="desc">
                                                    <label for="ps<?= $arPaySystem['ID'] ?>"><?= $arPaySystem['NAME'] ?></label>
                                                    <? if ($arPaySystem['ID'] == 1): ?>
                                                        <div class="buttons">
                                                            <input type="file" name="<?= $arResult['PROP']['file']['FIELD_NAME'] ?>[0]" style="display: none" class="file-input"/>
                                                            <input name="" type="button" value="Прикрепить реквизиты " class="file-select">
                                                            <input name="" type="button" value="Заполнить реквизиты" data-rel="details-block" class="arcticmodal-open">

                                                            <div class="detail-block-info" style="display: none;">
                                                                Реквизиты заполнены
                                                            </div>
                                                        </div>
                                                    <? endif; ?>
                                                    <? if ($arPaySystem['ID'] == 3): ?>
                                                        После подтверждения заказа менеджером компании Вам будет отправлена ссылка на оплату заказа
                                                    <? endif; ?>
                                                </div>
                                            </div>
                                        <? endforeach; ?>

                                        <div class="hidden">
                                            <div id="details-block" class="popup_block">
                                                <div class="popup-back clearfix">
                                                    <a href="#" class="close arcticmodal-close"><img src="/assets/images/popup-close.png" class="btn_close" title="Close Window" alt="Close"/></a>

                                                    <p class="h1">Заполните реквизиты</p>

                                                    <p>Поля помеченные <span class="red">красным</span> обязательны для заполнения</p>
                                                    <br>

                                                    <div id="error-block">Заполните все обязательные поля</div>
                                                    <table class="legal-block">
                                                        <tr>
                                                            <td class="legend red">Полное наименование организации</td>
                                                            <td class="input-min">
                                                                <select name="<?= $arResult['PROP']['form']['FIELD_NAME'] ?>" data-required="1">
                                                                    <option value=""></option>
                                                                    <? foreach ($arResult['PROP']['form']['VARIANTS'] as $v): ?>
                                                                        <option value="<?= $v['VALUE'] ?>"><?= $v['NAME'] ?></option>
                                                                    <? endforeach; ?>
                                                                </select>
                                                                <input name="<?= $arResult['PROP']['legal_name']['FIELD_NAME'] ?>" type="text" value="<?= $arResult['PROP']['legal_name']['VALUE'] ?>" data-required="1"/>
                                                            </td>
                                                        </tr>
                                                        <? foreach ($arResult['PROP'] as $code => $property): ?>
                                                            <? if ($property['PROPS_GROUP_ID'] == 3 && $code != 'form' && $code != 'legal_name' && $code != 'file'): ?>
                                                                <tr>
                                                                    <td class="legend <?= $property['REQUIED'] == 'Y' ? 'red' : '' ?>"><?= $property['NAME'] ?></td>
                                                                    <td class="input"><input name="<?= $property['FIELD_NAME'] ?>" type="text" value="<?= $property['VALUE'] ?>" data-required="<?= $property['REQUIED'] == 'Y' ? '1' : '' ?>"/>
                                                                    </td>
                                                                </tr>
                                                            <? endif; ?>
                                                        <? endforeach; ?>
                                                        <tr>
                                                            <td></td>
                                                            <td class="button"><input name="" class="legal-props" type="button" value="Сохранить"></td>
                                                        </tr>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                </div>

                                <div class="delivery-type">
                                    <div class="name">Способ доставки</div>
                                    <div class="radio-group">
                                        <? $deliveryId = ''; ?>
                                        <? foreach ($arResult['DELIVERY_SYSTEMS'] as $arDelivery): ?>
                                            <? if ($arDelivery['CHECKED']) {
                                                $deliveryId = $arDelivery['ID'];
                                            }
                                            ?>
                                            <div class="radio-item <? if (!$arDelivery['AVAILABLE']): ?>disabled<? endif; ?>">
                                                <div class="radio"><input class="delivery-input" name="DELIVERY_ID" type="radio" value="<?= $arDelivery['ID'] ?>"
                                                                          id="d<?= $arDelivery['ID'] ?>" <?= $arDelivery['CHECKED'] ? 'checked' : '' ?> <? if (!$arDelivery['AVAILABLE']): ?>disabled<? endif; ?>></div>
                                                <div class="desc">
                                                    <label for="d<?= $arDelivery['ID'] ?>"><?= $arDelivery['NAME'] ?><? if ($arDelivery['DESCRIPTION']): ?> (<?= htmlspecialchars_decode(
                                                            $arDelivery['DESCRIPTION']
                                                        ) ?>);<? endif; ?></label>
                                                </div>
                                                <? if ($arDelivery['ID'] == 1): ?>
                                                    <div class="desc" <? if (!$arDelivery['CHECKED']): ?>style="display: none"<? endif; ?>>
                                                        <label><?= $arResult['DELIVERY_FORMATTED'] ?> в пределах МКАД</label>
                                                    </div>
                                                <? endif; ?>
                                                <? if ($arDelivery['ID'] == 2): ?>
                                                    <div class="desc" <? if (!$arDelivery['CHECKED']): ?>style="display: none"<? endif; ?> id="delivery-distance-row">
                                                        <label><?= $arResult['DELIVERY_FORMATTED'] ?> + 25 р. за каждый километр от МКАД</label>
                                                        <label>Укажите километраж: <input style="width: 70px;" class="distance-input" name="<?= $arResult['PROP']['distance']['FIELD_NAME'] ?>"
                                                                                          value="<?= $arResult['PROP']['distance']['VALUE'] ?>"
                                                                                          type="text"/> <a href="javascript:void(0)" class="delivery-calc">Пересчитать</a></label>
                                                    </div>
                                                <? endif; ?>
                                            </div>
                                        <? endforeach; ?>
                                    </div>
                                    <h3><b>Стоимость доставки: <?= $arResult['DELIVERY_FORMATTED'] ?></b></h3>
                                </div>

                                <div class="address-form">
                                    <div class="name">Личные данные</div>
                                    <div class="input-block clearfix">
                                        <label for="i1">ФИО<span class="required">*</span></label>

                                        <div class="input">
                                            <input name="<?= $arResult['PROP']['fio']['FIELD_NAME'] ?>" value="<?= $arResult['PROP']['fio']['VALUE'] ?>" type="text" id="i1" class="required-input">
                                        </div>
                                    </div>
                                    <div class="input-block clearfix">
                                        <label for="i2">E-mail<span class="required">*</span></label>

                                        <div class="input-2"><input name="<?= $arResult['PROP']['email']['FIELD_NAME'] ?>" value="<?= $arResult['PROP']['email']['VALUE'] ?>" id="i2" class="required-input"></div>
                                    </div>
                                    <div class="input-block clearfix">
                                        <label for="i3">Телефон<span class="required">*</span></label>

                                        <div class="input-2"><input name="<?= $arResult['PROP']['phone']['FIELD_NAME'] ?>" value="<?= $arResult['PROP']['phone']['VALUE'] ?>" id="i3" placeholder="+7 (999) 999-99-99" class="phone-mask required-input"></div>
                                    </div>
                                    <div class="input-block clearfix" id="delivery-address-row" <? if ($deliveryId == 3): ?>style="display: none;"<? endif; ?>>
                                        <label for="i4">Адрес доставки</label>

                                        <div class="input i-r"><input name="<?= $arResult['PROP']['address']['FIELD_NAME'] ?>" value="<?= $arResult['PROP']['address']['VALUE'] ?>" id="i4" placeholder="Город, улица, дом, офис/квартира">
                                        </div>
                                    </div>
                                    <div class="input-block clearfix">
                                        <label for="i5">Комментарий к заказу</label><br/><br/>
                                        <div class="input"><textarea name="ORDER_DESCRIPTION" style="min-width: 270px; max-width:100%;min-height:120px"><?=$arResult["USER_VALS"]["ORDER_DESCRIPTION"]?></textarea></div>
                                    </div>
                                    <div class="input-block clearfix input-block-privacy">
                                        <div class="privacy-accept">
                                            Нажимая на кнопку "Отправить заказ" Вы соглашаетесь на <a href="/privacy/">обработку персональных данных.</a>
                                        </div>
                                    </div>
                                </div>


                                <div class="itogo">К оплате: <?= $arResult['allSum_FORMATED'] ?></div>
                                <br/>

                                <div class="big-button"><a href="#" onclick="yaCounter29171335.reachGoal('send_order');" id="confirm-order-btn" class="submit-form-link">Отправить заказ</a></div>
                            </div>


                        </div>
                    </div>
                </aside>

                <!-- /right-side -->
            </form>
        </div>
        <?
    }
} ?>
