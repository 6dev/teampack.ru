<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
} ?>
<div id="vm-basket-full">

    <div class="left-s">
        <div class="center-s">
            <? $APPLICATION->IncludeComponent('bitrix:breadcrumb', 'top') ?>
            <div class="content-block">


                <div class="notetext confirm-order-block">
                    <?
                    if (!empty($arResult["ORDER"])) {
                        ?>

                        <table class="sale_order_full_table">
                            <tr>
                                <td>
                                    <img src="/upload/galka.png" alt="заказ успешно создан" />
                                </td>
                            </tr>
                            <tr>
                                <td>
									<b><h2><?= GetMessage("SOA_TEMPL_ORDER_SUC", Array("#ORDER_DATE#" => $arResult["ORDER"]["DATE_INSERT"], "#ORDER_ID#" => $arResult["ORDER"]["ACCOUNT_NUMBER"])) ?></h2></b>
                                </td>
                            </tr>
                        </table>
                        
                        <?
                        if (!empty($arResult["PAY_SYSTEM"])) {
                            ?>
                            <? if ($arResult["PAY_SYSTEM"]['NAME'] == 'Безналичный'): ?>
                                <p>
                                    <?= GetMessage("SOA_TEMPL_ORDER_SUC1", Array("#LINK#" => $arParams["PATH_TO_PERSONAL"])) ?><br/>
                                    Письмо с логином и паролем было отправлено Вам на электронную почту.<br/> В ближайшее время с Вами свяжется наш сотрудник для уточнения деталей.
                                    После подтверждения заказа на Ваш электронный адрес будет отправлен счет на оплату.
                                </p>

                            <? elseif ($arResult["PAY_SYSTEM"]['NAME'] == 'Электронные деньги'): ?>
                                <p>
                                    <?= GetMessage("SOA_TEMPL_ORDER_SUC1", Array("#LINK#" => $arParams["PATH_TO_PERSONAL"])) ?><br/>
                                    Письмо с логином и паролем было отправлено Вам на электронную почту.<br/> В ближайшее время с Вами свяжется наш сотрудник для уточнения деталей.
                                    После подтверждения заказа на Ваш электронный адрес будет отправлен ссылка на оплату
                                </p>
					<?else:?>
<p>
                                    <?= GetMessage("SOA_TEMPL_ORDER_SUC1", Array("#LINK#" => $arParams["PATH_TO_PERSONAL"])) ?><br/>
                                    Письмо с логином и паролем было отправлено Вам на электронную почту.<br/> В ближайшее время с Вами свяжется наш сотрудник для уточнения деталей.

                                </p>
                            <? endif; ?>
                        <?
                        }
                    } else {
                        ?>
                    <?
                    }
                    ?>
                </div>
                <div class="btn-wrapper">
                    <a href="/catalog/" id="catalog-return">Продолжить покупки</a>
                </div>

            </div>
            <!-- /content-block -->
        </div>
        <!-- /center-s -->
    </div>
    <!-- /left-s -->
    <aside class="right-side">

    </aside>
    <!-- /right-side -->

</div>
