<?php
/**
 * @author Alexandr Kirshin <kirshin.as@gmail.com>
 */
?>

    <div class="feedback-form">
        <? foreach ($arResult['errors'] as $error): ?>
            <?= ShowError($error); ?>
        <? endforeach; ?>
        <? if ($_GET['success'] == 'y'): ?>
            <div class="success">Ваш отзыв отправлен.</div>
        <? else: ?>
            <form method="post" enctype="multipart/form-data">
                <table>
                    <tbody>
                    <tr>
                        <td class="legend">Имя <span class="red">*</span></td>
                        <td class="input"><input name="feedback[name]" type="text" value="<?= $arResult['data']['name'] ?>"></td>
                    </tr>
                    <tr>
                        <td class="legend">E-mail или телефон <span class="red">*</span></td>
                        <td class="input"><input name="feedback[contact]" type="text" value="<?= $arResult['data']['contact'] ?>"></td>
                    </tr>
                    <tr>
                        <td class="legend">Ваш отзыв <span class="red">*</span></td>
                        <td class="textarea"><textarea name="feedback[message]" cols="" rows=""><?= $arResult['data']['message'] ?></textarea></td>
                    </tr>
                    <tr>
                        <td></td>
                        <td class="file" style="text-align:left;"><input name="file" type="file" title="Прикрепить файл"></td>
                    </tr>
                    <tr>
                        <td></td>
                        <td class="input">
                        	<div class="g-recaptcha" data-sitekey="6Lc5xxMUAAAAAGzyUjuCoCgJ9ze9Hpc3lZxORdZC"></div>
                    		<?php /*
                    		<?php $capCode = $GLOBALS["APPLICATION"]->CaptchaGetCode();  ?>
                    		<input type="hidden" name="captcha_sid" value="<?= htmlspecialchars($capCode) ?>">
                    		<input size="40" value="" name="cap" />
                     		<img src="/bitrix/tools/captcha.php?captcha_sid=<?= htmlspecialchars($capCode) ?>" width="180" height="40"><br>
                     		*/?>
                        </td>
                    </tr>
                    <tr>
                        <td class="legend"></td>
                        <td class="input">
                            <div class="privacy-accept">
                                Нажимая на кнопку "Отправить отзыв" Вы соглашаетесь на <a href="/privacy/">обработку персональных данных.</a>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td></td>
                        <td class="button"><a href="javascript:;" class="submit-form-link">Отправить отзыв</a></td>
                    </tr>
                    </tbody>
                </table>
            </form>
        <?endif; ?>
        <i class="bant"></i>
    </div>

<?$APPLICATION->IncludeComponent("bitrix:news.list", "feedbacks", array(
	"IBLOCK_TYPE" => "content",
	"IBLOCK_ID" => "9",
	"NEWS_COUNT" => "20",
	"SORT_BY1" => "ID",
	"SORT_ORDER1" => "DESC",
	"SORT_BY2" => "SORT",
	"SORT_ORDER2" => "ASC",
	"FILTER_NAME" => "",
	"FIELD_CODE" => array(
		0 => "",
		1 => "DATE_CREATE",
		2 => "",
	),
	"PROPERTY_CODE" => array(
		0 => "contact",
		1 => "message",
		2 => "company",
		3 => "",
	),
	"CHECK_DATES" => "Y",
	"DETAIL_URL" => "",
	"AJAX_MODE" => "N",
	"AJAX_OPTION_JUMP" => "N",
	"AJAX_OPTION_STYLE" => "Y",
	"AJAX_OPTION_HISTORY" => "N",
	"CACHE_TYPE" => "A",
	"CACHE_TIME" => "36000000",
	"CACHE_FILTER" => "N",
	"CACHE_GROUPS" => "Y",
	"PREVIEW_TRUNCATE_LEN" => "",
	"ACTIVE_DATE_FORMAT" => "d.m.Y",
	"SET_STATUS_404" => "N",
	"SET_TITLE" => "N",
	"INCLUDE_IBLOCK_INTO_CHAIN" => "N",
	"ADD_SECTIONS_CHAIN" => "N",
	"HIDE_LINK_WHEN_NO_DETAIL" => "N",
	"PARENT_SECTION" => "",
	"PARENT_SECTION_CODE" => "",
	"INCLUDE_SUBSECTIONS" => "Y",
	"PAGER_TEMPLATE" => ".default",
	"DISPLAY_TOP_PAGER" => "N",
	"DISPLAY_BOTTOM_PAGER" => "Y",
	"PAGER_TITLE" => "Новости",
	"PAGER_SHOW_ALWAYS" => "N",
	"PAGER_DESC_NUMBERING" => "N",
	"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
	"PAGER_SHOW_ALL" => "N",
	"AJAX_OPTION_ADDITIONAL" => ""
	),
	false
);?>