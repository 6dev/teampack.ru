<?php

/**
 * @author Alexandr Kirshin <kirshin.as@gmail.com>
 */
class FeedbackComponent extends CBitrixComponent
{
    public function executeComponent()
    {
        global $APPLICATION;
        $data = $_POST['feedback'];
        if (isset($data)) {


            if (!isset($_REQUEST['g-recaptcha-response']) || !($_REQUEST['g-recaptcha-response'])) {
                $captcha_error = true;
            } else {
                $captcha_error = true;
                $myCurl = curl_init();
                curl_setopt_array($myCurl, array(
                    CURLOPT_URL => 'https://www.google.com/recaptcha/api/siteverify',
                    CURLOPT_RETURNTRANSFER => true,
                    CURLOPT_POST => true,
                    CURLOPT_POSTFIELDS => http_build_query(array('secret' => '6Lc5xxMUAAAAAHhDbFajaskR_fMYyl1BwnJTXCJ0', 'response' => $_REQUEST['g-recaptcha-response'], 'remoteip'=>$_SERVER['REMOTE_ADDR'],))
                ));
                $response = curl_exec($myCurl);
                curl_close($myCurl);

                if ($response) {
                    $res = json_decode($response);
                    if ($res && isset($res->success)) {
                       $captcha_error = ! $res->success;
                    }
                }
            }

            if ($captcha_error)
            {
                $errors[] = 'Необходимо правильно заполнить «Проверочный код».';
            }

            if (!$data['name']) {
                $errors[] = 'Необходимо заполнить «Имя».';
            }
            if (!$data['contact']) {
                $errors[] = 'Необходимо заполнить «E-mail или телефон».';
            }
            if (!$data['contact']) {
                $errors[] = 'Необходимо заполнить «Ваш отзыв».';
            }
            if ($_FILES['file']['name']) {

                $info = pathinfo($_FILES['file']['name']);
                $ext = array('png', 'gif', 'jpg', 'jpeg');
                if (!in_array($info['extension'], $ext)) {
                    $errors[] = 'Файл должен иметь расширение ' . implode(', ', $ext);
                } elseif ($_FILES['file']['size'] > 1024 * 1024 * 2) {
                    $errors[] = 'Размер файла не должен привышать 2Мб';
                }
            }
            if (count($errors) == 0) {
                $arFields = array(
                    'ACTIVE'          => 'N',
                    'NAME'            => $data['name'],
                    'IBLOCK_ID'       => $this->arParams['IBLOCK_ID'],
                    'PROPERTY_VALUES' => array(
                        'contact' => $data['contact'],
                        'message' => $data['message']
                    )
                );
                if ($_FILES['file']) {
                    $arFields['PROPERTY_VALUES']['file'] = $_FILES['file'];
                }
                $el = new CIBlockElement();
                if ($el->Add($arFields)) {
                    $arFields['EMAIL'] = $data['contact'];
                    $arFields['MESSAGE'] = $data['message'];
                    CEvent::Send('SEND_TO_FEEDBACK', SITE_ID, $arFields);
                    LocalRedirect($APPLICATION->GetCurPageParam('success=y'));
                } else {
                    $this->arResult['errors'] = $el->LAST_ERROR;
                }
            } else {
                $this->arResult['errors'] = $errors;
            }
        }
        $this->arResult['data'] = $data;
        $this->includeComponentTemplate();
    }

}