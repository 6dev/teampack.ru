<?php
/**
 * @author Alexandr Kirshin <kirshin.as@gmail.com>
 */

use Bitrix\Main\Loader;
use seoexp\data\Element;
use seoexp\helpers\SessionHelper;

class ElementFormComponent extends CBitrixComponent
{
    public function executeComponent()
    {
        global $APPLICATION;
        if(Loader::includeModule('app')) {
            $model = new Element($this->arParams['IBLOCK_ID']);
            $model->setAttributeLabel('name', $this->arParams['NAME_LABEL']);
            $model->addValidateRule('email', 'property_email', array('message' => 'Неверный email'));

            if ($this->arParams['USE_CAPTCHA'] == 'Y') {
                $model->addAttribute('captcha_word', 'Символы с картинки', array(array('required'), array('captcha')));
                $model->addAttribute('captcha_code', 'Капча');
            }

            if ($model->load($_POST) && $model->save()) {

                if ($this->arParams['EMAIL_EVENT']) {
                    CEvent::Send($this->arParams['EMAIL_EVENT'], SITE_ID, $model->getAttributeValues());
                }
                SessionHelper::setFlash('success', $this->arParams['SUCCESS_MESSAGE']);

            }
            if ($this->arParams['USE_CAPTCHA'] == 'Y') {
                $model->setAttributeValue('captcha_code', $APPLICATION->CaptchaGetCode());
            }
            $this->arResult['model'] = $model;
            $this->includeComponentTemplate();
        }
    }

}