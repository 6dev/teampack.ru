<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
}
/** @var array $arCurrentValues */
/** @global CUserTypeManager $USER_FIELD_MANAGER */
global $USER_FIELD_MANAGER;

if (!\Bitrix\Main\Loader::includeModule("iblock")) {
    return;
}

$boolCatalog = \Bitrix\Main\Loader::includeModule("catalog");

$arIBlockType = CIBlockParameters::GetIBlockTypes();

$arIBlock = array();
$rsIBlock = CIBlock::GetList(array("sort" => "asc"), array("TYPE" => $arCurrentValues["IBLOCK_TYPE"], "ACTIVE" => "Y"));
while ($arr = $rsIBlock->Fetch()) {
    $arIBlock[$arr["ID"]] = "[" . $arr["ID"] . "] " . $arr["NAME"];
}


$arComponentParameters = array(

    "PARAMETERS" => array(
        "IBLOCK_TYPE"     => array(
            "PARENT"  => "BASE",
            "NAME"    => 'Тип инфоблока',
            "TYPE"    => "LIST",
            "VALUES"  => $arIBlockType,
            "REFRESH" => "Y",
        ),
        "IBLOCK_ID"       => array(
            "PARENT"            => "BASE",
            "NAME"              => 'Инфоблок',
            "TYPE"              => "LIST",
            "ADDITIONAL_VALUES" => "Y",
            "VALUES"            => $arIBlock,
            "REFRESH"           => "Y",
        ),
        "NAME_LABEL"      => array(
            "PARENT"  => "BASE",
            "NAME"    => 'Заголовок поля Имя',
            "TYPE"    => "TEXT",
            "DEFAULT" => 'Ваше имя'
        ),
        "SUCCESS_MESSAGE" => array(
            "PARENT"  => "BASE",
            "NAME"    => 'Сообщение об успешной отправке',
            "TYPE"    => "TEXT",
            "DEFAULT" => 'Ваше сообщение отправлено'
        ),
        "USE_CAPTCHA"     => array(
            "PARENT"  => "BASE",
            "NAME"    => 'Использовать CAPTCHA',
            "TYPE"    => "CHECKBOX",
            "DEFAULT" => 'Y'
        ),
        "EMAIL_EVENT"     => array(
            "PARENT" => "BASE",
            "NAME"   => 'Тип почтового сообщения',
            "TYPE"   => "TEXT"
        ),

    ),
);