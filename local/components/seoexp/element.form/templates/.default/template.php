<?php
/**
 * @author Alexandr Kirshin <kirshin.as@gmail.com>
 */
/**
 * @var CBitrixComponentTemplate $this
 * @var Array                    $arResult
 * @var Element                  $model
 *
 */
use seoexp\data\Element;
use seoexp\helpers\Html;
use seoexp\helpers\SessionHelper;

$this->setFrameMode(true);

$model = $arResult['model'];
if ($_GET['article']) {
    $model->setAttributeValue('property_article', $_GET['article']);
}
?>
<div class="feedback-form">
    <form method="post">
        <? if ($message = SessionHelper::getFlash('success')): ?>
            <?= $message ?>
        <? else: ?>
            <table>
                <tbody>
                <tr class="<?= $model->hasErrors('property_article') ? 'error' : '' ?>">
                    <td class="legend"><?= Html::activeLabel($model, 'property_article'); ?> <span class="red">*</span></td>
                    <td class="input">
                        <?= Html::activeTextInput($model, 'property_article', array('readonly'=>'readonly')); ?>
                        <div class="info"><?= $model->getFirstError('property_article') ?></div>
                    </td>
                </tr>
                <tr class="<?= $model->hasErrors('name') ? 'error' : '' ?>">
                    <td class="legend"><?= Html::activeLabel($model, 'name'); ?> <span class="red">*</span></td>
                    <td class="input">
                        <?= Html::activeTextInput($model, 'name'); ?>
                        <div class="info"><?= $model->getFirstError('name') ?></div>
                    </td>
                </tr>
                <tr class="<?= $model->hasErrors('property_company') ? 'error' : '' ?>">
                    <td class="legend"><?= Html::activeLabel($model, 'property_company'); ?> <span class="red">*</span></td>
                    <td class="input">
                        <?= Html::activeTextInput($model, 'property_company'); ?>
                        <div class="info"><?= $model->getFirstError('property_company') ?></div>
                    </td>
                </tr>
                <tr class="<?= $model->hasErrors('property_city') ? 'error' : '' ?>">
                    <td class="legend"><?= Html::activeLabel($model, 'property_city'); ?>&nbsp;&nbsp;</td>
                    <td class="input">
                        <?= Html::activeTextInput($model, 'property_city'); ?>
                        <div class="info"><?= $model->getFirstError('property_city') ?></div>
                    </td>
                </tr>
                <tr class="<?= $model->hasErrors('property_phone') ? 'error' : '' ?>">
                    <td class="legend"><?= Html::activeLabel($model, 'property_phone'); ?> <span class="red">*</span></td>
                    <td class="input">
                        <?= Html::activeTextInput($model, 'property_phone', array('class'=>'telephone')); ?>
                        <div class="info"><?= $model->getFirstError('property_phone') ?></div>
                    </td>
                </tr>
                <tr class="<?= $model->hasErrors('property_email') ? 'error' : '' ?>">
                    <td class="legend"><?= Html::activeLabel($model, 'property_email'); ?> <span class="red">*</span></td>
                    <td class="input">
                        <?= Html::activeTextInput($model, 'property_email'); ?>
                        <div class="info"><?= $model->getFirstError('property_email') ?></div>
                    </td>
                </tr>
                <tr>
                    <td></td>
                    <td class="button"><a href="javascript:;" class="submit-form-link">Отправить</a></td>
                </tr>
                </tbody>
            </table>
        <? endif; ?>
    </form>

    <i class="bant"></i>
</div>