/**
 * Created by kirshin on 04.07.14.
 */
/**
 *   Селекторы Dom элементов
 *   #vm-basket-top - Контейнер малой корзины
 *   #vm-basket-full - Контейнер большой корзины
 *   #vm-basket-full-form - Форма большой корзины
 *   .vm-basket-add - Кнопка добавления в корзину
 *   .vm-basket-delete - Кнопка удаления из большой корзины
 *   .vm-basket-plus - Кнопка увеличения количества в большой корзине
 *   .vm-basket-minus - Кнопка уменьшения количества в большой корзине
 */
v.basket = function () {

    var updateTargets = function (response, targets) {
        var $response = $(response);
        for (var i = 0; i < targets.length; i++) {
            $(targets[i]).replaceWith($response.find(targets[i]));
        }
    };

    return {
        add: function (options) {
            var settings = {
                targets: ['#vm-basket-top', '#vm-basket-full'],
                dataType: 'html',
                beforeSend: false,
                success: false,
                error: false
            };

            if (options) $.extend(settings, options);

            var init = function () {
                $('.vm-basket-add').unbind('click').bind('click', function () {
                    var control = $(this);
                    if ($(this).parents('form').find("input[name='basket[quantity]']")) {
                        if (!$(this).parents('form').find("input[name='basket[quantity]']").val()) {
                            $(this).parents('form').find('.quanity-attention').show();
                            $(this).parents('form').find(".col-block").css('border', '1px solid red');
                            return false;
                        }
                    }
                    $.ajax({
                        data: $(this).parents('form').serialize(),
                        type: 'post',
                        dataType: settings.dataType,
                        beforeSend: function (jqXHR, options) {
                            if (settings.beforeSend) {
                                settings.beforeSend(control, jqXHR, options);
                            }
                        },
                        error: function (jqXHR, textStatus, errorThrown) {
                            if (settings.error) {
                                settings.error(control, jqXHR, textStatus, errorThrown);
                            }
                        }
                    }).done(function (response) {
                        if (settings.targets.length > 0) {
                            updateTargets(response, settings.targets);
                        }
                        if (settings.success) {
                            settings.success(control, response);
                        }
                    });
                    return false;
                });
            };

            init();

        },
        update: function (options) {

            var settings = {
                form: '#vm-basket-full-form',
                targets: ['#vm-basket-top', '#vm-basket-full'],
                dataType: 'html',
                beforeSerialize: false,
                beforeSend: false,
                success: false,
                error: false
            };

            var form;


            if (options) $.extend(settings, options);

            console.log(options);

            var init = function () {
                form = $(settings.form);

                $('.vm-basket-plus').unbind('click').bind('click', function () {
                    var input = $(this).parent().find('input');
                    var val = parseInt(input.val()) + 1;
                    input.val(val);
                    update(form);
                    return false;
                });
                $('.vm-basket-minus').unbind('click').bind('click', function () {
                    var input = $(this).parent().find('input');
                    var val = parseInt(input.val()) - 1;
                    if (val <= 0) val = 1;
                    input.val(val);
                    update(form);
                    return false;
                });
                $('.vm-basket-delete').unbind('click').bind('click', function () {
                    var input = $(this).parent().find('input');
                    input.val('Y');
                    update(form);
                    return false;
                });

            };

            var update = function (form) {
                if (form.length > 0) {
                    if (settings.beforeSerialize) {
                        settings.beforeSerialize(form);
                    }
                    $.ajax({
                        data: form.serialize(),
                        type: 'post',
                        dataType: settings.dataType,
                        beforeSend: function (jqXHR, options) {
                            if (settings.beforeSend) {
                                settings.beforeSend(jqXHR, options);
                            }
                        },
                        error: function (jqXHR, textStatus, errorThrown) {
                            if (settings.error) {
                                settings.error(jqXHR, textStatus, errorThrown);
                            }
                        }
                    }).done(function (response) {
                        if (settings.targets.length > 0) {
                            updateTargets(response, settings.targets);
                        }
                        if (settings.success) {
                            settings.success(response);
                        }
                        init();
                    });
                }
            };

            init();

            return {
                init: function () {
                    init();
                },
                update: function (f) {
                    if (f == undefined) {
                        f = form;
                    }
                    update(f);
                }
            }
        }
    }
}();