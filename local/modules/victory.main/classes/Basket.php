<?php
/**
 * @author Alexandr Kirshin <kirshin.as@gmail.com>
 */

namespace Victory\Main;

use \CSaleBasket;
use Bitrix\Main\HttpRequest;
use Bitrix\Main\Loader;

class Basket
{

    private $fileBasketHandler;

    public function __construct($fileBasketHandler = false)
    {
        Loader::includeModule('iblock');
        Loader::includeModule('catalog');
        Loader::includeModule('sale');
        $this->fileBasketHandler = $fileBasketHandler;
    }

    /**
     * @param $request HttpRequest
     */
    public function processingRequest($request)
    {
        if ($request->isPost()) {
            $basket = $request->getPost('basket');
            if (is_array($basket)) {
                $this->executeQuery($basket);
            }
        }
    }

    /**
     * Обработать запрос на изминение корзины
     * @param $basket Array Массив параметров
     */
    public function executeQuery($basket)
    {
        if (isset($basket['id']) && intval($basket['id'])) //Добавление в корзину
        {
            if (file_exists($this->fileBasketHandler)) {
                include($this->fileBasketHandler);
            } else {
                $basket['id'] = intval($basket['id']);
                $basket['quantity'] = $basket['quantity'] ? floatval($basket['quantity']) : 1;
                $basket['props'] = is_array($basket['props']) ? $basket['props'] : array();
                Add2BasketByProductID($basket['id'], $basket['quantity'], $basket['PROPS']);
            }
        }
        if (isset($basket['quantity']) && is_array($basket['quantity'])) //Обновление корзины
        {
            foreach ($basket['quantity'] as $id => $quantity) {
                CSaleBasket::Update($id, array('QUANTITY' => $quantity));
            }
        }
        if (isset($basket['delete']) && is_array($basket['delete'])) //Удаление из корзины
        {
            foreach ($basket['delete'] as $id => $flag) {
                if ($flag == 'Y') {
                    CSaleBasket::Delete($id);
                }
            }
        }
    }
} 