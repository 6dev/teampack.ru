<?php
/**
 * @author Alexandr Kirshin <kirshin.as@gmail.com>
 */

namespace Victory\Main;


use COption;
use Bitrix\Main\Application;

class EventListener
{

    /**
     * Обработка события перед загрузкой страницы
     */
    public static function onPageStart()
    {

    }


    /**
     * Обработка события перед прологом
     */
    public static function onProlog()
    {
        $context = Application::getInstance()->getContext();
        $request = $context->getRequest();
        $basketHandler = false;
        if (COption::GetOptionString('victory.main', 'basketHandler')) {
            $basketHandler = $_SERVER['DOCUMENT_ROOT'] . COption::GetOptionString('victory.main', 'basketHandler');
        }
        $basket = new Basket($basketHandler);
        $basket->processingRequest($request);
    }

} 