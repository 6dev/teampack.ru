<?php
/**
 * @author Alexandr Kirshin <kirshin.as@gmail.com>
 */

namespace Victory\Main;


class AutoLoader
{

    public static function load($className)
    {
        $pathParts = explode('\\', $className);
        if (count($pathParts) > 2 && $pathParts[0] == 'Victory' && $pathParts[1] == 'Main') {
            $pathParts = array_slice($pathParts, 2);
            $fileName = __DIR__ . DIRECTORY_SEPARATOR . implode(DIRECTORY_SEPARATOR, $pathParts) . '.php';
            if (file_exists($fileName)) {
                require_once($fileName);
            }
        }
    }
} 