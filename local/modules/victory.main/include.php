<?php
/**
 * @author Alexandr Kirshin <kirshin.as@gmail.com>
 */

include_once 'classes/AutoLoader.php';
spl_autoload_register(array('Victory\Main\AutoLoader', 'load'));

global $APPLICATION;
$APPLICATION->AddHeadScript('/local/modules/victory.main/js/victory.js');
$APPLICATION->AddHeadScript('/local/modules/victory.main/js/basket.js');
