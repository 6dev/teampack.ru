<?php
/**
 * @author Alexandr Kirshin <kirshin.as@gmail.com>
 */
global $USER, $APPLICATION;
if (!$USER->IsAdmin()) {
    return;
}


$arAllOptions = Array(
    Array("basketHandler", 'Файл обработчик добавления в корзину', "", Array("text", "")),
);
$aTabs = array(
    array("DIV" => "edit1", "TAB" => "Параметры", "ICON" => "ib_settings", "TITLE" => "Параметры"),
);
$tabControl = new CAdminTabControl("tabControl", $aTabs);

if ($_SERVER['REQUEST_METHOD'] == "POST" && !empty($_POST['Apply']) && check_bitrix_sessid()) {

    foreach ($arAllOptions as $arOption) {
        $name = $arOption[0];
        $val = $_REQUEST[$name];
        if ($arOption[2][0] == "checkbox" && $val != "Y") {
            $val = "N";
        }
        COption::SetOptionString("victory.main", $name, $val, $arOption[1]);
    }
    LocalRedirect($APPLICATION->GetCurPage() . "?mid=" . urlencode($mid) . "&lang=" . urlencode(LANGUAGE_ID) . "&back_url_settings=" . urlencode($_REQUEST["back_url_settings"]) . "&" . $tabControl->ActiveTabParam());
}


$tabControl->Begin();
?>
<form method="post" action="<? echo $APPLICATION->GetCurPage() ?>?mid=<?= urlencode($mid) ?>&amp;lang=<? echo LANGUAGE_ID ?>">
    <? $tabControl->BeginNextTab(); ?>
    <?
    foreach ($arAllOptions as $arOption):
        $val = COption::GetOptionString("victory.main", $arOption[0], $arOption[2]);
        $type = $arOption[3];
        ?>
        <tr>
            <td width="40%" nowrap>
                <label for="<? echo htmlspecialcharsbx($arOption[0]) ?>"><? echo $arOption[1] ?>:</label>
            <td width="60%">
                <? if ($type[0] == "checkbox"): ?>
                    <input type="checkbox" id="<? echo htmlspecialcharsbx($arOption[0]) ?>" name="<? echo htmlspecialcharsbx($arOption[0]) ?>" value="Y"<? if ($val == "Y") {
                        echo " checked";
                    } ?> class="checkbox-opt">
                <? elseif ($type[0] == "password"): ?>
                    <input type="password" size="<? echo $type[1] ?>" maxlength="255" value="<? echo htmlspecialcharsbx($val) ?>" name="<? echo htmlspecialcharsbx($arOption[0]) ?>">
                <?
                elseif ($type[0] == "text"): ?>
                    <input type="text" size="<? echo $type[1] ?>" maxlength="255" value="<? echo htmlspecialcharsbx($val) ?>" name="<? echo htmlspecialcharsbx($arOption[0]) ?>">
                <?
                elseif ($type[0] == "textarea"): ?>
                    <textarea rows="<? echo $type[1] ?>" cols="<? echo $type[2] ?>" name="<? echo htmlspecialcharsbx($arOption[0]) ?>"><? echo htmlspecialcharsbx($val) ?></textarea>
                <?endif ?>
            </td>
        </tr>
    <? endforeach ?>
    <? $tabControl->Buttons(); ?>
    <input type="submit" name="Apply" value="Сохранить">
    <?= bitrix_sessid_post(); ?>
    <? $tabControl->End(); ?>
</form>