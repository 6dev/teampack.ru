<?php

/**
 * @author Kirshin Alexandr <kirshin.as@gmail.com>
 */
class victory_main extends CModule
{
    var $MODULE_ID = "victory.main";
    var $MODULE_VERSION;
    var $MODULE_VERSION_DATE;
    var $MODULE_NAME;
    var $MODULE_DESCRIPTION;
    var $MODULE_CSS;

    function victory_main()
    {
        $arModuleVersion = array();
        include("version.php");
        if (is_array($arModuleVersion) && array_key_exists("VERSION", $arModuleVersion)) {
            $this->MODULE_VERSION = $arModuleVersion["VERSION"];
            $this->MODULE_VERSION_DATE = $arModuleVersion["VERSION_DATE"];
        }
        $this->MODULE_NAME = "victory.main";
        $this->MODULE_DESCRIPTION = "";
        $this->PARTNER_NAME = 'Victory';
        $this->PARTNER_URI = 'http://victory.su';
    }


    function DoInstall()
    {

        RegisterModuleDependences("main", "OnProlog", "victory.main", 'Victory\Main\EventListener', "onProlog");
        RegisterModule($this->MODULE_ID);
    }

    function DoUninstall()
    {
        UnRegisterModuleDependences("main", "OnProlog", "victory.main", 'Victory\Main\EventListener', "onProlog");
        UnRegisterModule($this->MODULE_ID);
    }
}