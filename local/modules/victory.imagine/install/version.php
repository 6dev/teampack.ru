<?php
/**
 * @author: Alexandr Kirshin <kirshin.as@gmail.com>
 */

$arModuleVersion = array(
    "VERSION" => "1.0.0",
    "VERSION_DATE" => "2014-05-8 10:00:00",
);