<?php
/**
 * @author: Alexandr Kirshin <kirshin.as@gmail.com>
 */
IncludeModuleLangFile(__FILE__);

class victory_imagine extends CModule
{
    var $MODULE_ID = "victory.imagine";
    public $MODULE_VERSION;
    public $MODULE_VERSION_DATE;
    public $MODULE_NAME;
    public $MODULE_DESCRIPTION;

    public function victory_imagine()
    {
        $arModuleVersion = array();
        include(__DIR__. "/version.php");
        if (is_array($arModuleVersion) && array_key_exists("VERSION", $arModuleVersion)) {
            $this->MODULE_VERSION = $arModuleVersion["VERSION"];
            $this->MODULE_VERSION_DATE = $arModuleVersion["VERSION_DATE"];
        }

        $this->MODULE_NAME = GetMessage("MODULE_NAME");
        $this->MODULE_DESCRIPTION = GetMessage("MODULE_DESC");
        $this->PARTNER_NAME = GetMessage("MODULE_PARTNER");
        $this->PARTNER_URI = GetMessage("MODULE_URI");
    }

    public function DoInstall()
    {
        RegisterModule("victory.imagine");
    }

    public function DoUninstall()
    {
        UnRegisterModule("victory.imagine");
    }
}
