<?php
/**
 * @author: Alexandr Kirshin <kirshin.as@gmail.com>
 */
namespace victory\helpers;

use Exception;
use Imagine\Gd\Imagine;
use Imagine\Image\Box;
use Imagine\Image\Point;
use Size;

class Image
{

    const THUMBNAIL_INSET = 'inset';
    const THUMBNAIL_OUTBOUND = 'outbound';

    const RESOLUTION_PIXELSPERINCH = 'ppi';
    const RESOLUTION_PIXELSPERCENTIMETER = 'ppc';

    const INTERLACE_NONE = 'none';
    const INTERLACE_LINE = 'line';
    const INTERLACE_PLANE = 'plane';
    const INTERLACE_PARTITION = 'partition';

    const FILTER_UNDEFINED = 'undefined';
    const FILTER_POINT = 'point';
    const FILTER_BOX = 'box';
    const FILTER_TRIANGLE = 'triangle';
    const FILTER_HERMITE = 'hermite';
    const FILTER_HANNING = 'hanning';
    const FILTER_HAMMING = 'hamming';
    const FILTER_BLACKMAN = 'blackman';
    const FILTER_GAUSSIAN = 'gaussian';
    const FILTER_QUADRATIC = 'quadratic';
    const FILTER_CUBIC = 'cubic';
    const FILTER_CATROM = 'catrom';
    const FILTER_MITCHELL = 'mitchell';
    const FILTER_LANCZOS = 'lanczos';
    const FILTER_BESSEL = 'bessel';
    const FILTER_SINC = 'sinc';

    const FLIP_HORIZONTALLY = 'Horizontally';
    const FLIP_VERTICALLY = 'Vertically';

    public static $cachePath;
    public static $rootPath;

    public $fileName;

    private $_manipulationStack = array();


    public static function open($fileName)
    {
        if (!self::$cachePath || !self::$rootPath) {
            throw new Exception('Не указана корневая директория и директория кеша');
        }
        if (!file_exists(self::$rootPath . $fileName)) {
            throw new Exception('Файл ' . $fileName . ' не найден.');
        }

        return new Image($fileName);
    }

    private function __construct($fileName)
    {
        $this->fileName = $fileName;
    }

    public function crop($x, $y, $w, $h)
    {
        $this->_manipulationStack["c_{$x}_{$y}_{$w}_{$h}"] = array('method' => 'crop', 'params' => array(new Point($x, $y), new Box($w, $h)));

        return $this;
    }

    public function thumbnail($w, $h, $mode = self::THUMBNAIL_INSET)
    {
        $this->_manipulationStack["t_{$w}_{$h}_{$mode}"] = array('method' => 'thumbnail', 'params' => array(new Box($w, $h), $mode));

        return $this;
    }

    public function flip($mode)
    {
        $this->_manipulationStack["f_{$mode}"] = array('method' => 'flip' . $mode, 'params' => array());

        return $this;
    }

    public function save($fileName = '')
    {
        if (!$fileName) {
            $fileName = $this->generateFileName();
        }
        if (!$this->isCached($this->fileName, $fileName)) {
            $imagine = new Imagine();
            $image = $imagine->open(self::$rootPath . $this->fileName);
            foreach ($this->_manipulationStack as $action) {
                $image = call_user_func_array(array($image, $action['method']), $action['params']);
            }
            $image->save(self::$rootPath . $fileName);
            touch(self::$rootPath . $fileName, filemtime(self::$rootPath . $this->fileName));
        }

        return $fileName;
    }

    private function generateFileName()
    {
        $pathInfo = pathinfo($this->fileName);
        $prefix = '';
        foreach ($this->_manipulationStack as $key => $action) {
            $prefix .= '_' . $key;
        }
        $prefix = ltrim($prefix, '_');
        $fileName = self::$cachePath . trim($pathInfo["dirname"], '/') . '/' . $prefix . "_" . $pathInfo["basename"];
        $fullPath = self::$rootPath . dirname($fileName);
        if (!file_exists($fullPath)) {
            mkdir($fullPath, 0777, true);
        }

        return $fileName;
    }

    private function isCached($sourceFile, $destinationFile)
    {
        if (!file_exists(self::$rootPath . $destinationFile)) {
            return false;
        }
        if (file_exists(self::$rootPath . $sourceFile)) {
            $sourceTime = filemtime(self::$rootPath . $sourceFile);
            $destinationTime = filemtime(self::$rootPath . $destinationFile);

            return $sourceTime == $destinationTime;
        }

        return true;
    }
}