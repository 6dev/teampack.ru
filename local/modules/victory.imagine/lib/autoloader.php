<?php
/**
 * @author: Alexandr Kirshin <kirshin.as@gmail.com>
 */

class Autoloader
{
    public static function load($className)
    {
        $pathParts = explode('\\', $className);
        $fileName = __DIR__.DIRECTORY_SEPARATOR.implode(DIRECTORY_SEPARATOR, $pathParts) . '.php';
        if (file_exists($fileName)) {
            require_once($fileName);
        }
    }
}