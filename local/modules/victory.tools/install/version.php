<?php
/**
 * @author Kirshin Alexandr <kirshin.as@gmail.com>
 *
 * Date: 18.12.13
 * Time: 9:02
 */
$arModuleVersion = array(
    "VERSION"      => "1.0.5",
    "VERSION_DATE" => "2014-03-31 00:00:00"
);