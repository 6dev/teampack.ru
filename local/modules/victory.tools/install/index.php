<?php

/**
 * @author Kirshin Alexandr <kirshin.as@gmail.com>
 *
 * Date: 18.12.13
 * Time: 9:01
 */
class victory_tools extends CModule
{
    var $MODULE_ID = "victory.tools";
    var $MODULE_VERSION;
    var $MODULE_VERSION_DATE;
    var $MODULE_NAME;
    var $MODULE_DESCRIPTION;
    var $MODULE_CSS;

    function victory_tools()
    {
        $arModuleVersion = array();
        include("version.php");
        if (is_array($arModuleVersion) && array_key_exists("VERSION", $arModuleVersion)) {
            $this->MODULE_VERSION = $arModuleVersion["VERSION"];
            $this->MODULE_VERSION_DATE = $arModuleVersion["VERSION_DATE"];
        }
        $this->MODULE_NAME = "Набор вспомогательных классов и компонентов";
        $this->MODULE_DESCRIPTION = "";
        $this->PARTNER_NAME = 'Victory';
        $this->PARTNER_URI = 'http://victory.su';
    }

    function InstallFiles($arParams = array())
    {
        CopyDirFiles(dirname(__FILE__) . "/components", $_SERVER["DOCUMENT_ROOT"] . "/bitrix/components/" . $this->MODULE_ID, true, true);

        return true;
    }

    function UnInstallFiles()
    {
        DeleteDirFilesEx("/bitrix/components/" . $this->MODULE_ID);

        return true;
    }

    function DoInstall()
    {
        $this->InstallFiles();
        RegisterModule($this->MODULE_ID);
    }

    function DoUninstall()
    {
        $this->UnInstallFiles();
        UnRegisterModule($this->MODULE_ID);
    }
}