<?php
/**
 * Created by PhpStorm.
 * User: Александр
 * Date: 20.02.14
 * Time: 10:42
 */
if (!CModule::IncludeModule("iblock")) {
    return;
}

$boolCatalog = CModule::IncludeModule("catalog");

$arIBlockType = CIBlockParameters::GetIBlockTypes();

$arIBlock = array();
$rsIBlock = CIBlock::GetList(Array("sort" => "asc"), Array("ACTIVE" => "Y"));
while ($arr = $rsIBlock->Fetch()) {
    $arIBlock[$arr["ID"]] = "[" . $arr["ID"] . "] " . $arr["NAME"];
}


$arComponentParameters = array(
    "PARAMETERS" => array(
        "IBLOCK_ID"      => array(
            "PARENT"            => "BASE",
            "NAME"              => GetMessage("IBLOCK_IBLOCK"),
            "TYPE"              => "LIST",
            "ADDITIONAL_VALUES" => "Y",
            "VALUES"            => $arIBlock,
            "REFRESH"           => "Y",
        ),
        "ID"             => array(
            "PARENT"  => "BASE",
            "NAME"    => GetMessage("IBLOCK_ELEMENT_ID"),
            "TYPE"    => "STRING",
            "DEFAULT" => '',
        ),
        "HIDDEN_VALUES"         => array(
            "PARENT"  => "BASE",
            "NAME"    => GetMessage("HIDDEN_VALUES"),
            "TYPE"    => "STRING",
            "DEFAULT" => 'arrValues',
        ),
        "MESSAGE_NEW"    => array(
            "PARENT"  => "BASE",
            "NAME"    => GetMessage("MESSAGE_NEW"),
            "TYPE"    => "STRING",
            "DEFAULT" => '',
        ),
        "MESSAGE_UPDATE" => array(
            "PARENT"  => "BASE",
            "NAME"    => GetMessage("MESSAGE_UPDATE"),
            "TYPE"    => "STRING",
            "DEFAULT" => '',
        ),
        "NAME_LABEL"     => array(
            "PARENT"  => "BASE",
            "NAME"    => GetMessage("NAME_LABEL"),
            "TYPE"    => "STRING",
            "DEFAULT" => '',
        ),
        "EMAIL_EVENT"    => array(
            "PARENT"  => "BASE",
            "NAME"    => GetMessage("EMAIL_EVENT"),
            "TYPE"    => "CHECKBOX",
            "DEFAULT" => '',
        ),
    )
);
