<?php
/**
 * Created by PhpStorm.
 * User: Александр
 * Date: 20.02.14
 * Time: 10:29
 */
/**
 * Class ElementFormComponent
 *
 * @property Form $form
 */
use \victory\components\AjaxComponent;
use \victory\helpers\SystemHelper;
use \victory\iblock\Form;

/**
 * Class ElementFormComponent
 *
 * @property Form $form
 */
class ElementFormComponent extends AjaxComponent
{

    public $form;

    public function getEmailTplId()
    {
        return $tplCode = 'IBLOCK_' . $this->arParams['IBLOCK_ID'] . '_EVENT_NEW';
    }

    public function createEmailTpl()
    {
        $tplCode = $this->getEmailTplId();

        $rsET = CEventType::GetByID($tplCode, "ru");
        $arET = $rsET->Fetch();
        if (!$arET) {
            $description = '';
            $description .= 'Название: #NAME#' . PHP_EOL;
            $description .= 'Ид: #ID#' . PHP_EOL;
            foreach ($this->form->fields as $arProperty) {
                $description .= $arProperty['NAME'] . ': #PROPERTY_' . $arProperty['CODE'] . '#' . PHP_EOL;
            }

            $et = new CEventType;
            $et->Add(
                array(
                    "LID"         => "ru",
                    "EVENT_NAME"  => $tplCode,
                    "NAME"        => 'Уведомление о новом элементе в инфоблоке ' . $this->arParams['IBLOCK_ID'],
                    "DESCRIPTION" => $description
                )
            );

            $arr["ACTIVE"] = "Y";
            $arr["EVENT_NAME"] = $tplCode;
            $arr["LID"] = SITE_ID;
            $arr["EMAIL_FROM"] = "#DEFAULT_EMAIL_FROM#";
            $arr["EMAIL_TO"] = "#DEFAULT_EMAIL_FROM#";
            $arr["SUBJECT"] = "#SITE_NAME#: Тема сообщения";
            $arr["BODY_TYPE"] = "text";
            $arr["MESSAGE"] = $description;

            $emess = new CEventMessage;
            $emess->Add($arr);

        }
    }

    public function sendEvent()
    {
        $tplCode = $this->getEmailTplId();
        $arEventFields = array();
        $arEventFields['ID'] = $this->form->id;
        $arEventFields['NAME'] = $this->form->getControlValue('NAME');
        foreach ($this->form->fields as $arField) {
            $arEventFields['PROPERTY_' . $arField['CODE']] = $arField['VALUE'];
        }
        CEvent::Send($tplCode, SITE_ID, $arEventFields);
    }

    public function executeAjaxComponent()
    {
        //Предустановеные значения
        $arValuesName = $this->arParams['HIDDEN_VALUES'];
        global $$arValuesName;
        $this->arResult['HIDDEN'] = $$arValuesName;

        $code = 'IBLOCK_' . $this->arParams['IBLOCK_ID'];
        $this->form = new Form($this->arParams['IBLOCK_ID'], $code, $this->arParams['ID']);
        $this->form->init();



        if ($this->arParams['EMAIL_EVENT'] == 'Y') {
            $this->createEmailTpl();
        }
        if ($this->arParams['NAME_LABEL']) {
            $this->form->nameLabel = $this->arParams['NAME_LABEL'];
        }
        if ($this->checkIdComponent() && isset($_POST[$code])) {
            $this->form->load($_POST);
            if ($this->afterValidate()) {
                if ($this->form->validate()) {
                    if ($this->beforeSave()) {
                        if ($this->form->save()) {
                            if ($this->arParams['EMAIL_EVENT'] == 'Y') {
                                $this->sendEvent();
                            }
                            if (!$this->arParams['ID']) {
                                SystemHelper::setFlash('success', $this->arParams['MESSAGE_NEW']);
                                $this->form->clear();
                            } else {
                                SystemHelper::setFlash('success', $this->arParams['MESSAGE_UPDATE']);
                            }
                        }
                    }
                }
            }

            if ($this->form->hasErrors()) {
                $message = '';
                foreach ($this->form->errors as $errors):
                    $message .= $errors[0] . '<br/>';
                endforeach;
                SystemHelper::setFlash('failure', $message);
            }
        }
        $this->arResult['FORM'] = $this->form;
        $this->includeComponentTemplate();
    }

    protected function afterValidate()
    {
        $this->initComponentTemplate();
        $folder = $this->getTemplate()->GetFolder();
        if (file_exists($_SERVER['DOCUMENT_ROOT'] . $folder . '/after_validate.php')) {
            return include $_SERVER['DOCUMENT_ROOT'] . $folder . '/after_validate.php';
        }

        return true;
    }

    protected function beforeSave()
    {
        $this->initComponentTemplate();
        $folder = $this->getTemplate()->GetFolder();
        if (file_exists($_SERVER['DOCUMENT_ROOT'] . $folder . '/before_save.php')) {
            return include $_SERVER['DOCUMENT_ROOT'] . $folder . '/before_save.php';
        }

        return true;
    }
}