<?
/**
 * @var $form Form
 */
use victory\helpers\SystemHelper;

$form = $arResult['FORM'];

?>
<p>Шаблон формы</p>
<form method="post" enctype="multipart/form-data" action="">
    <?= $arResult['COMPONENT_ID_INPUT']; ?>
    <input type="hidden" name="<?= $form->getControlName('ACTIVE'); ?>" value="Y"/>
    <? foreach ($arParams['HIDDEN'] as $name => $value): ?>
        <input type="hidden" name="<?= $form->getControlName($name); ?>" value="<?= $value ?>"/>
    <? endforeach ?>
    <? if (SystemHelper::HasFlash('success')): ?>
        <div class="success">
            <?= Helper::GetFlash('success'); ?>
        </div>
    <? endif; ?>
    <? if (SystemHelper::HasFlash('failure')): ?>
        <div class="failure">
            <?= SystemHelper::GetFlash('failure'); ?>
        </div>
    <? endif; ?>
</form>