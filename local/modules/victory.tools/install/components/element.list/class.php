<?php
/**
 * Created by PhpStorm.
 * @author Alexandr Kirshin <kirshin.as@gmail.com>
 * Date: 03.03.14
 * Time: 9:07
 */

use \Bitrix\Main\Loader;
use \victory\components\AjaxComponent;
use \victory\helpers\SystemHelper;
use \victory\iblock\Form;
use \CIBlockElement;

class ElementListComponent extends AjaxComponent
{

    public function executeAjaxComponent()
    {
        $arFilterName = $this->arParams['FILTER_NAME'];
        global $$arFilterName;
        $arFilter = $$arFilterName;
        if (Loader::includeModule('iblock')) {
            $arFilter['ACTIVE'] = 'Y';
            $arFilter['IBLOCK_ID'] = $this->arParams['IBLOCK_ID'];
            $resList = CIBlockElement::GetList(array(), $arFilter);
            while ($obElement = $resList->GetNextElement()) {
                $arElement = $obElement->GetFields();
                $arElement['PROPERTIES'] = $obElement->GetProperties();
                $this->arResult['ITEMS'][] = $arElement;
            }
        }
        $this->includeComponentTemplate();
    }
}