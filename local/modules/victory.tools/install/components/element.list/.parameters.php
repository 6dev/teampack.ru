<?php
/**
 * Created by PhpStorm.
 * User: Александр
 * Date: 20.02.14
 * Time: 10:42
 */
if (!CModule::IncludeModule("iblock")) {
    return;
}

$boolCatalog = CModule::IncludeModule("catalog");

$arIBlockType = CIBlockParameters::GetIBlockTypes();

$arIBlock = array();
$rsIBlock = CIBlock::GetList(Array("sort" => "asc"), Array("ACTIVE" => "Y"));
while ($arr = $rsIBlock->Fetch()) {
    $arIBlock[$arr["ID"]] = "[" . $arr["ID"] . "] " . $arr["NAME"];
}


$arComponentParameters = array(
    "PARAMETERS" => array(
        "IBLOCK_ID"      => array(
            "PARENT"            => "BASE",
            "NAME"              => GetMessage("IBLOCK_IBLOCK"),
            "TYPE"              => "LIST",
            "ADDITIONAL_VALUES" => "Y",
            "VALUES"            => $arIBlock,
            "REFRESH"           => "Y",
        )
    )
);
