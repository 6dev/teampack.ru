<?php

namespace victory\iblock;

use Bitrix\Main\Loader;
use \CIBlockElement;
use \CIBlockProperty;
use \CIBlockPropertyEnum;


/**
 * @author Kirshin Alexandr <kirshin.as@gmail.com>
 *
 * Date: 19.12.13
 * Time: 18:21
 */
class Form
{
    public $id;
    public $iblock;
    public $properties = array();
    public $fields = array();
    public $code;
    public $errors;
    public $nameLabel = 'Название';

    private $_files = array();

    public function __construct($iblock, $code, $id = 0)
    {
        $this->id = $id;
        $this->iblock = $iblock;
        $this->code = $code;
        $this->_files = $this->loadFilesArray();
    }

    public function init()
    {
        Loader::includeModule('iblock');
        $properties = CIBlockProperty::GetList(Array("sort" => "asc", "name" => "asc"), Array("ACTIVE" => "Y", "IBLOCK_ID" => $this->iblock));
        while ($prop_fields = $properties->GetNext()) {
            $this->fields[$prop_fields['CODE']] = $prop_fields;
            if ($prop_fields['PROPERTY_TYPE'] == 'L') {
                $this->loadEnum($prop_fields['CODE']);
            } elseif ($prop_fields['PROPERTY_TYPE'] == 'E') {
                $this->loadRelation($prop_fields['CODE']);
            }
        }
        if ($this->id) {
            $arFilter = Array("IBLOCK_ID" => $this->iblock, "ID" => $this->id);
            $res = CIBlockElement::GetList(Array('SORT'=>'ASC'), $arFilter);
            if ($obElement = $res->GetNextElement()) {
                $arElement = $obElement->GetFields();
                $arElement['PROPERTIES'] = $obElement->GetProperties();
            }
            $this->properties['NAME'] = $arElement['NAME'];
            $this->properties['ACTIVE'] = $arElement['ACTIVE'];
            foreach ($arElement['PROPERTIES'] as $code => $property) {
                if ($property['PROPERTY_TYPE'] == 'L') {
                    $this->fields[$code]['VALUE'] = $property['VALUE_ENUM_ID'];
                } else {
                    $this->fields[$code]['VALUE'] = $property['VALUE'];
                }
            }
        }
    }

    public function clear()
    {
        foreach ($this->properties as $code => $value) {
            $this->properties[$code] = '';
        }
        foreach ($this->fields as $code => $field) {
            $this->fields[$code]['VALUE'] = '';
        }
    }

    /**
     * Загрузка справочника
     * @param $code
     */
    private function loadEnum($code)
    {
        $property_enums = CIBlockPropertyEnum::GetList(Array("DEF" => "DESC", "SORT" => "ASC"), Array("IBLOCK_ID" => $this->iblock, "CODE" => $code));
        while ($enum_fields = $property_enums->GetNext()) {
            $this->fields[$code]['VALUES'][$enum_fields["ID"]] = $enum_fields["VALUE"];
        }
    }

    /**
     * Загрузка связанных элементов
     * @param $code
     */
    private function loadRelation($code)
    {
        $arSelect = Array("ID", "NAME");
        $arFilter = Array("IBLOCK_ID" => $this->fields[$code]['LINK_IBLOCK_ID'], "ACTIVE" => "Y");
        $res = CIBlockElement::GetList(Array('SORT' => 'ASC'), $arFilter, false, false, $arSelect);
        while ($arElement = $res->GetNext()) {
            $this->fields[$code]['VALUES'][$arElement['ID']] = $arElement['NAME'];
        }
    }

    /**
     * Массив описания файлов
     * @return array
     */
    private function loadFilesArray()
    {
        $result = array();
        foreach ($_FILES[$this->code] as $name => $values) {

            foreach ($values as $code => $value) {
                if (is_array($value)) {
                    foreach ($value as $i => $v) {
                        $result[$code][$i][$name] = $v;
                    }
                } else {
                    $result[$code][0][$name] = $value;
                }
            }


        }

        return $result;
    }

    /**
     * Загрузка данных
     * @param $attributes
     *
     * @return bool
     */
    public function load($attributes)
    {
        if (isset($attributes[$this->code])) {
            if (array_key_exists('NAME', $attributes[$this->code])) {
                $this->properties['NAME'] = $attributes[$this->code]['NAME'];
            }
            $this->properties['ACTIVE'] = isset($attributes[$this->code]) ? $attributes[$this->code]['ACTIVE'] : 'N';
            foreach ($attributes[$this->code] as $code => $value) {
                $this->setControlValueEx($code, $value);
            }

            return true;
        }

        return false;
        /*
        foreach ($this->fields as $code => $property) {
            $this->fields[$code]['VALUE'] = $attributes[$this->code][$code];
        }*/
    }

    /**
     * Имя поля
     * @param $code
     * @return string
     */
    public function getControlName($code)
    {
        if ($this->fields[$code]['MULTIPLE'] == 'Y') {
            return $this->code . '[' . $code . '][]';
        } else {
            return $this->code . '[' . $code . ']';
        }
    }

    /**
     * Название поля
     * @param $code
     * @return string
     */
    public function getControlLabel($code)
    {
        if ($code == 'NAME') {
            return $this->nameLabel;
        }

        return $this->fields[$code]['NAME'];
    }

    /**
     * Ид поля
     * @param $code
     * @return string
     */
    public function getControlId($code)
    {
        if ($code == 'NAME') {
            return $this->code . '-NAME';
        }
        if ($code == 'ACTIVE') {
            return $this->code . '-ACTIVE';
        }

        if (!$this->fields[$code]) {
            return $this->code . '-' . $code;
        }

        return $this->code . '-' . $this->fields[$code]['ID'];
    }

    /**
     * Значение поля
     * @param $code
     * @param $index
     * @return mixed
     */
    public function getControlValue($code, $index = 0)
    {
        if ($index == -1) {
            return '';
        }
        if ($code == 'NAME') {
            return $this->properties['NAME'];
        }
        if ($code == 'ACTIVE') {
            return $this->properties['ACTIVE'];
        }
        if (is_array($this->fields[$code]['VALUE'])) {
            return $this->fields[$code]['VALUE'][$index];
        }

        return $this->fields[$code]['VALUE'];
    }

    /**
     * Установить значение поля
     * @param $code
     * @param $value
     * @param int $index
     */
    public function setControlValue($code, $value, $index = 0)
    {
        if ($code == 'NAME') {
            $this->properties['NAME'] = $value;

            return;
        }
        if ($code == 'ACTIVE') {
            $this->properties['ACTIVE'] = $value;

            return;
        }
        if (is_array($this->fields[$code]['VALUE'])) {
            $this->fields[$code]['VALUE'][$index] = $value;
        }
        $this->fields[$code]['VALUE'] = $value;
    }

    public function setControlValueEx($code, $value, $index = 0)
    {
        if (empty($value)) {
            return;
        }
        if ($code == 'NAME') {
            $this->properties['NAME'] = $value;

            return;
        }
        if ($code == 'ACTIVE') {
            $this->properties['ACTIVE'] = $value;

            return;
        }
        if (is_array($value)) {
            foreach ($value as $i => $v) {
                $this->fields[$code]['VALUE'][$i] = $v;
            }

            return;
        }
        if (is_array($this->fields[$code]['VALUE'])) {
            $this->fields[$code]['VALUE'][$index] = $value;
        }
        $this->fields[$code]['VALUE'] = $value;
    }


    public function setControlValues($code, $values)
    {
        $this->fields[$code]['VALUES'] = $values;
    }

    /**
     * Все значения поля
     * @param $code
     * @return mixed
     */
    public function getControlValues($code)
    {
        if ($code == 'NAME') {
            return $this->properties['NAME'];
        }
        if ($code == 'ACTIVE') {
            return $this->properties['ACTIVE'];
        }

        return $this->fields[$code]['VALUE'];
    }

    /**
     * Возможные варианты поля
     * @param $code
     * @return mixed
     */
    public function getControlVariants($code)
    {
        return $this->fields[$code]['VALUES'];
    }

    /**
     * Получить описание поля
     * @param $code
     * @param array $options
     * @param bool $noControl
     * @param integer $index
     * @return string
     */
    public function getControlOption($code, $options = array(), $noControl = false, $index = 0)
    {
        if (!$noControl) {
            $options['id'] = $options['id'] ? $options['id'] : $this->getControlId($code);
            $options['name'] = $options['name'] ? $options['name'] : $this->getControlName($code);
            if ($this->hasError($code, $index)) {
                $options['class'] .= ' error';
            }
            if ($code == 'NAME' || $this->fields[$code]['IS_REQUIRED'] == 'Y') {
                $options['class'] .= ' required';
            }
        } else {
            $options['for'] = $options['for'] ? $options['for'] : $this->getControlId($code);
            if ($this->hasError($code, $index)) {
                $options['class'] .= ' error';
            }
        }
        $result = '';
        foreach ($options as $key => $value) {
            $result .= $key . '="' . $value . '" ';
        }

        return $result;
    }

    /**
     * Метка
     * @param string $code
     * @param array $options
     * @param string $postfix
     * @return string
     */
    public function renderLabel($code, $options = array(), $postfix = '')
    {
        $options['for'] = $this->getControlId($code);

        return '<label ' . $this->getControlOption($code, $options, true) . '>' . $this->getControlLabel($code) . $postfix . '</label>';
    }

    /**
     * Выпадающий список
     * @param $code
     * @param array|bool $items
     * @param int $index
     * @param array $options
     * @return string
     */
    public function renderSelect($code, $items = false, $index = 0, $options = array())
    {
        $control = '<select ' . $this->getControlOption($code, $options) . ' data-placeholder="...">';
        if (!is_array($items)) {
            $items = $this->fields[$code]['VALUES'];
        }
        if ($this->fields[$code]['IS_REQUIRED'] == 'N') {
            $control .= '<option></option>';
        }
        foreach ($items as $key => $value) {
            $control .= '<option value="' . $key . '" ' . $this->getStateOption($code, $key, $index, 'selected="selected"') . '>' . $value . '</option>';
        }

        $control .= '</select>';

        return $control;
    }

    /**
     * Текстовое поле
     * @param $code
     * @param int $index
     * @param array $options
     * @return string
     */
    public function renderInput($code, $index = 0, $options = array())
    {
        $options['type'] = 'text';
        $options['value'] = $this->getControlValue($code, $index);

        return '<input ' . $this->getControlOption($code, $options) . ' />';
    }

    /**
     * Многострочный текст
     * @param $code
     * @param int $index
     * @param array $options
     * @return string
     */
    public function renderText($code, $index = 0, $options = array())
    {
        return '<textarea ' . $this->getControlOption($code, $options) . '>' . $this->getControlValue($code, $index) . '</textarea>';
    }

    /**
     * Файл
     * @param $code
     * @param int $index
     * @param array $options
     * @return string
     */
    public function renderFileInput($code, $index = 0, $options = array())
    {
        $options['type'] = 'file';

        return '<input ' . $this->getControlOption($code, $options) . ' />' . $this->renderHiddenInput($code, $index);
    }

    /**
     * Скрытое поле
     * @param $code
     * @param int $index
     * @return string
     */
    public function renderHiddenInput($code, $index = 0)
    {
        $options['type'] = 'hidden';
        $options['value'] = $this->getControlValue($code, $index);

        return '<input ' . $this->getControlOption($code, $options) . ' />';
    }

    /**
     * Чекбокс
     * @param $code
     * @param int $index
     * @param array $options
     * @return string
     */
    public function renderCheckbox($code, $index = 0, $options = array())
    {
        $options['type'] = 'checkbox';
        $options['value'] = $this->getCheckedValue($code);

        return '<input ' . $this->getControlOption($code, $options) . ' ' . $this->getStateChecked($code, $this->getCheckedValue($code), $index) . '/>';
    }




        /**
     * Список из чекбоксов
     * @param $code
     * @param int $index
     * @param array $options
     * @return string
     */
    public function renderCheckboxList($code, $index = 0, $options = array())
    {
        $control = '';
        foreach ($this->fields[$code]['VALUES'] as $key => $label) {
            $options['type'] = 'checkbox';
            $options['value'] = $key;
            $options['id'] = 'c-' . $this->getControlId($code) . '-' . $key;
            $control .= '<label for="c-' . $this->getControlId($code) . '-' . $key . '"><input ' . $this->getControlOption($code, $options) . ' ' . $this->getStateChecked($code, $key, $index) . '/>';
            $control .= ' ' . $label . '</label>';
        }

        return $control;
    }

    public function renderHtml($code, $index = 0, $options = array())
    {
        $options['class'] .= ' ckeditor';

        return $this->renderText($code, $index, $options);
    }

    /**
     * Значение чекбокса
     * @param $code
     * @return int|string
     */
    public function getCheckedValue($code)
    {
        if ($code == 'ACTIVE') {
            return 'Y';
        }
        foreach ($this->fields[$code]['VALUES'] as $value => $label) {
            return $value;
        }
    }

    /**
     * Состояние чекбокса
     * @param $code
     * @param $value
     * @param int $index
     * @param string $mark
     * @return string
     */
    public function getStateChecked($code, $value, $index = 0, $mark = 'checked="checked"')
    {
        if ($this->fields[$code]['MULTIPLE'] == 'Y') {
            return in_array($value, $this->getControlValues($code)) ? $mark : '';
        } else {
            return $this->getControlValue($code, $index) == $value ? $mark : '';
        }
    }

    /**
     * Состояние селекта
     * @param $code
     * @param $value
     * @param $index
     * @param string $mark
     * @return string
     */
    public function getStateOption($code, $value, $index = 0, $mark = 'selected="selected"')
    {
        return $this->getControlValue($code, $index) == $value ? $mark : '';
    }

    /**
     * Добавить ошибку
     * @param $code
     * @param $message
     * @param $i
     */
    public function markError($code, $message, $i = 0)
    {
        $this->errors[$code][$i] = $message;
    }

    /**
     * Проверка наличия ошибки у поля
     * @param $code
     * @param $index
     * @return bool
     */
    public function hasError($code, $index = 0)
    {
        return isset($this->errors[$code][$index]);
    }

    /**
     * Проверка наличия ошибок
     * @return bool
     */
    public function hasErrors()
    {
        return count($this->errors) > 0;
    }

    /**
     * ПРоверка полей
     * @return bool
     */
    public function validate()
    {
        if (!$this->properties['NAME']) {
            $this->markError('NAME', 'Необходимо заполнить «' . $this->nameLabel . '».');
        }


        foreach ($this->fields as $code => $property) {

            $values = $property['VALUE'];
            if (!is_array($property['VALUE'])) {
                $values = array($property['VALUE']);
            }
            if ($this->fields[$code]['IS_REQUIRED'] == 'Y') {
                foreach ($values as $i => $v) {
                    if (empty($v)) {
                        $this->markError($code, 'Необходимо заполнить «' . $this->fields[$code]['NAME'] . '».', $i);
                    }
                }
            }
            if ($this->fields[$code]['PROPERTY_TYPE'] == 'N') {
                foreach ($values as $i => $v) {
                    if (!empty($v) && !intval($v)) {
                        $this->markError($code, 'Значение в поле  "' . $this->fields[$code]['NAME'] . '" не является числом', $i);
                    }
                }
            }
        }


        return !$this->hasErrors();
    }

    /**
     * Проверка присвоен файл полю или нет
     * @param $code код поля
     * @return bool
     */
    public function fileAssigned($code)
    {
        if (!$this->fields[$code]['VALUE']) {
            if ($this->fields[$code]['PROPERTY_TYPE'] == 'F') {
                $fileValid = false;
                foreach ($this->_files[$code] as $fileArray) {
                    if (count($fileArray) > 0 && $fileArray['name']) {
                        $fileValid = true;
                    }
                }
                if (!$fileValid) {
                    return false;
                }
            } else {
                return false;
            }
        }

        return true;
    }

    /**
     * Сохранить
     * @return bool
     */
    public function save()
    {
        if ($this->validate()) {
            global $USER;
            $el = new CIBlockElement;
            $PROP = array();

            foreach ($this->fields as $code => $property) {
                $PROP[$code] = $property['VALUE'];
                if ($this->fields[$code]['PROPERTY_TYPE'] == 'L') {
                    if (is_array($PROP[$code])) {
                        foreach ($PROP[$code] as $p => $value) {
                            if ($value && !intval($value)) {
                                $PROP[$code][$p] = $this->addEnum($property, $value);
                            }
                        }
                    } else {
                        if ($PROP[$code] && !intval($PROP[$code])) {
                            $PROP[$code] = $this->addEnum($property, $PROP[$code]);
                        }
                    }
                }
                if ($this->fields[$code]['PROPERTY_TYPE'] == 'F') {
                    $setFile = false;
                    foreach ($this->_files[$code] as $fileArray) {
                        if (count($fileArray) > 0 && $fileArray['name']) {
                            if (!is_array($PROP[$code])) {
                                $PROP[$code] = array();
                            }
                            $PROP[$code][] = CFile::SaveFile($fileArray, 'iblock');
                            $setFile = true;
                        }
                    }
                    if (!$setFile) {
                        //Если это не новый файл то ничего не присваиваем
                        unset($PROP[$code]);
                    }
                }
            }
            if (count($this->errors) == 0) {
                $properties = $this->properties;
                $properties["CREATED_BY"] = $USER->GetID();
                $properties["IBLOCK_ID"] = $this->iblock;
                $properties["PROPERTY_VALUES"] = $PROP;
                if (isset($this->id) && $this->id > 0) {
                    $el->Update($this->id, $properties);
                } else {
                    $this->id = $el->Add($properties);
                    if (!$this->id) {
                        $this->errors['global'][] = $el->LAST_ERROR;

                        return false;
                    }
                }
            } else {
                return false;
            }

            return true;
        }

        return false;
    }

    private function addEnum($property, $value)
    {
        $property_enums = CIBlockPropertyEnum::GetList(Array(), Array('PROPERTY_ID' => $property['ID'], 'VALUE' => $value));
        if ($enum_fields = $property_enums->GetNext()) {
            return $enum_fields["ID"];
        }
        $ibpenum = new CIBlockPropertyEnum;
        if ($id = $ibpenum->Add(Array('PROPERTY_ID' => $property['ID'], 'VALUE' => $value))) {
            return $id;
        }

        return false;
    }


}