<?php

namespace victory\iblock;

use \CIBlockElement;
use \CIBlockSection;
use \CIBlockProperty;
use \CIBlockPropertyEnum;

/**
 * Created by PhpStorm.
 * User: Александр
 * Date: 25.01.14
 * Time: 17:08
 *
 * Version: 1.0.3
 */
class Filter
{

    private $_iblock_id;
    private $_properties;
    private $_fields = array('NAME', 'ACTIVE', 'IBLOCK_ID', 'SECTION_ID', 'IBLOCK_SECTION_ID');

    public function __construct($iblock_id)
    {
        $this->_iblock_id = $iblock_id;
    }

    /**
     * Получить список всех свойст
     * @return mixed
     */
    public function getProperties()
    {
        if (isset($this->_properties)) {
            return $this->_properties;
        }

        $properties = CIBlockProperty::GetList(
            Array(
                'sort' => 'asc',
                'name' => 'asc'
            ),
            Array(
                'ACTIVE'    => 'Y',
                'IBLOCK_ID' => $this->_iblock_id,
            )
        );
        while ($property = $properties->GetNext()) {
            $property['FULL_CODE'] = 'PROPERTY_' . $property['CODE'];
            $this->_properties[$this->normalizePropertyCode($property['CODE'])] = $property;
        }

        return $this->_properties;
    }


    public function getProperty($code)
    {
        $code = $this->normalizePropertyCode($code);
        $properties = $this->getProperties();

        return $properties[$code];
    }


    /**
     * Получить список фильтров, свойства должны быть с отметкой использовать поиск
     * @param array $filter Фильтр
     * @param array $filterProperty Фильтр свойств
     * @return array
     */
    public function getFilters($filter = array(), $filterProperty = array('FILTRABLE' => 'Y'))
    {
        $filter['IBLOCK_ID'] = $this->_iblock_id;
        $filter['INCLUDE_SUBSECTIONS'] = 'Y';
        $filterProperty['IBLOCK_ID'] = $this->_iblock_id;
        $filterProperty['ACTIVE'] = 'Y';

        $resProperties = CIBlockProperty::GetList(Array('sort' => 'asc', 'name' => 'asc'), $filterProperty);
        while ($property = $resProperties->GetNext()) {
            $property['FULL_CODE'] = $this->normalizePropertyCode($property['CODE']);
            $properties[$property['FULL_CODE']] = $property;
        }

        $filters = array();
        $resSection = CIBlockSection::GetList(array(), $filter);
        while ($arSection = $resSection->GetNext()) {
            $filters['SECTION_ID']['PROPERTY']['FULL_CODE'] = 'SECTION_ID';
            $filters['SECTION_ID']['PROPERTY']['NAME'] = 'Раздел';
            $filters['SECTION_ID']['VALUES'][$arSection['SECTION_PAGE_URL']] = $arSection['NAME'];
        }
        $res = CIBlockElement::GetList(array(), $filter, array_keys($properties));
        while ($group = $res->GetNext()) {
            foreach ($properties as $property) {
                if ($group[$property['FULL_CODE'] . '_VALUE']) {
                    $filters[$property['FULL_CODE']]['PROPERTY'] = $property;
                    if ($property['PROPERTY_TYPE'] == 'L') {
                        $filters[$property['FULL_CODE']]['ID'][] = $group[$property['FULL_CODE'] . '_ENUM_ID'];
                    } else {
                        $filters[$property['FULL_CODE']]['ID'][] = $group[$property['FULL_CODE'] . '_VALUE'];
                    }
                }
            }
        }

        foreach ($filters as $c => $f) {
            switch ($f['PROPERTY']['PROPERTY_TYPE']) {
                case 'L':
                    $resValues = CIBlockPropertyEnum::GetList(array('SORT' => 'ASC', 'VALUE' => 'ASC'), array('IBLOCK_ID' => $this->_iblock_id, 'ID' => $f['ID']));
                    while ($arValue = $resValues->GetNext()) {
                        $filters[$c]['VALUES'][$arValue['ID']] = $arValue['VALUE'];
                        $filters[$c]['CODES'][$arValue['ID']] = $arValue['XML_ID'];
                    }
                    break;
                case 'E':
                    $resValues = CIBlockElement::GetList(array('SORT' => 'ASC'), array('IBLOCK_ID' => $f['PROPERTY']['LINK_IBLOCK_ID'], 'ACTIVE' => 'Y', 'ID' => $f['ID']));
                    while ($arValue = $resValues->GetNext()) {
                        $filters[$c]['VALUES'][$arValue['ID']] = $arValue['NAME'];
                        $filters[$c]['CODES'][$arValue['ID']] = $arValue['CODE'];
                    }
                    break;
            }
        }

        return $filters;
    }

    public function getValues($code, $filter = array())
    {
        $filter['IBLOCK_ID'] = $this->_iblock_id;
        $filter['INCLUDE_SUBSECTIONS'] = 'Y';
        $filterProperty['IBLOCK_ID'] = $this->_iblock_id;
        $filterProperty['ACTIVE'] = 'Y';

        $resProperties = CIBlockProperty::GetList(Array('sort' => 'asc', 'name' => 'asc'), array('CODE' => $code));
        while ($property = $resProperties->GetNext()) {
            $property['FULL_CODE'] = $this->normalizePropertyCode($property['CODE']);
            $properties[$property['FULL_CODE']] = $property;
        }

        $filters = array();
        $resSection = CIBlockSection::GetList(array(), $filter);
        while ($arSection = $resSection->GetNext()) {
            $filters['SECTION_ID']['PROPERTY']['FULL_CODE'] = 'SECTION_ID';
            $filters['SECTION_ID']['PROPERTY']['NAME'] = 'Раздел';
            $filters['SECTION_ID']['VALUES'][$arSection['SECTION_PAGE_URL']] = $arSection['NAME'];
        }
        $res = CIBlockElement::GetList(array(), $filter, array_keys($properties));
        while ($group = $res->GetNext()) {
            foreach ($properties as $property) {
                if ($group[$property['FULL_CODE'] . '_VALUE']) {
                    $filters[$property['FULL_CODE']]['PROPERTY'] = $property;
                    if ($property['PROPERTY_TYPE'] == 'L') {
                        $filters[$property['FULL_CODE']]['ID'][] = $group[$property['FULL_CODE'] . '_ENUM_ID'];
                    } else {
                        $filters[$property['FULL_CODE']]['ID'][] = $group[$property['FULL_CODE'] . '_VALUE'];
                    }
                }
            }
        }

        foreach ($filters as $c => $f) {
            switch ($f['PROPERTY']['PROPERTY_TYPE']) {
                case 'L':
                    $resValues = CIBlockPropertyEnum::GetList(array('SORT' => 'ASC', 'VALUE' => 'ASC'), array('IBLOCK_ID' => $this->_iblock_id, 'ID' => $f['ID']));
                    while ($arValue = $resValues->GetNext()) {
                        $filters[$c]['VALUES'][$arValue['ID']] = $arValue['VALUE'];
                        $filters[$c]['CODES'][$arValue['ID']] = $arValue['XML_ID'];
                    }
                    break;
                case 'E':
                    $resValues = CIBlockElement::GetList(array('SORT' => 'ASC'), array('IBLOCK_ID' => $f['PROPERTY']['LINK_IBLOCK_ID'], 'ACTIVE' => 'Y', 'ID' => $f['ID']));
                    while ($arValue = $resValues->GetNext()) {
                        $filters[$c]['VALUES'][$arValue['ID']] = $arValue['NAME'];
                        $filters[$c]['CODES'][$arValue['ID']] = $arValue['CODE'];
                    }
                    break;
            }
        }


        return $filters[$this->normalizePropertyCode($code)];
    }


    /**
     * Получить минимальное и максимальное значение
     * @param $fieldCode
     * @param array $filter
     * @return array
     */
    public function getRangeValue($fieldCode, $filter = array())
    {
        $filter['IBLOCK_ID'] = $this->_iblock_id;
        $filter['INCLUDE_SUBSECTIONS'] = 'Y';
        $resGroup = CIBlockElement::GetList(array(), $filter, array('PROPERTY_' . $fieldCode));
        $min = PHP_INT_MAX;
        $max = 0;
        while ($arGroup = $resGroup->GetNext()) {
            if (true || $arGroup['PROPERTY_' . strtoupper($fieldCode) . '_VALUE']) {
                if ($arGroup['PROPERTY_' . strtoupper($fieldCode) . '_VALUE'] > $max) {
                    $max = $arGroup['PROPERTY_' . strtoupper($fieldCode) . '_VALUE'];
                }
                if ($arGroup['PROPERTY_' . strtoupper($fieldCode) . '_VALUE'] < $min) {
                    $min = $arGroup['PROPERTY_' . strtoupper($fieldCode) . '_VALUE'];
                }
            }
        }
        if ($min == PHP_INT_MAX) {
            $min = 0;
        }

        return array('min' => $min, 'max' => $max);
    }


    /**
     * Построить фильтр
     * @param $attributes array Параметры фильтра
     * @return array Фильтр
     */
    public function buildArrayFilter($attributes)
    {
        $filter = array();
        foreach ($attributes as $code => $value) {
            if (!empty($value)) {
                $nCode = $this->normalizePropertyCode($code);
                if (is_array($value)) {
                    $filter[$nCode] = $value;
                } else {
                    $value = htmlspecialchars($value);
                    $count = 0;
                    if (($c = preg_replace('/_MIN$/', '', $nCode, 1, $count)) && $count) {
                        $filter['>=' . $c] = $value;
                    } elseif (($c = preg_replace('/_MAX$/', '', $nCode, 1, $count)) && $count) {
                        $filter['<=' . $c] = $value;
                    } else {
                        $filter[$nCode] = $value;
                    }
                }
            }
        }

        return $filter;
    }

    protected function normalizePropertyCode($code)
    {
        $code = strtoupper($code);
        if (in_array($this->_fields, $code)) {
            return $code;
        }
        if (strpos('PROPERTY_', $code) === 0) {
            return $code;
        }

        return 'PROPERTY_' . $code;
    }

    /**
     * Получить количество элементов
     * @param array $filter Фильтр
     * @return int
     */
    public function getCount($filter = array())
    {
        $filter['IBLOCK_ID'] = $this->_iblock_id;
        $resCount = CIBlockElement::GetList(array(), $filter);

        return $resCount->SelectedRowsCount();
    }


    public function getIds($filter = array())
    {
        $result = array();
        $filter['IBLOCK_ID'] = $this->_iblock_id;
        $resId = CIBlockElement::GetList(array(), $filter);
        while ($arId = $resId->GetNext()) {
            $result[] = $arId['ID'];
        }

        return $result;
    }

}