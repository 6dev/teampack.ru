<?php
/**
 * Created by PhpStorm.
 * User: Александр
 * Date: 20.02.14
 * Time: 14:33
 */
namespace victory\helpers;

use Imagine\Gd\Imagine;
use Imagine\Image\Box;
use Imagine\Image\Point;

/**
 * Class ImageHelper
 * @package victory\helpers
 *
 */
class ImageHelper
{
    private static $_instance = null;
    private $_root;
    private $_cache;

    public static function getInstance()
    {
        if (self::$_instance) {
            return self::$_instance;
        }
        self::$_instance = new ImageHelper();

        return self::$_instance;
    }

    public static function init($root, $cache)
    {
        $instance = self::getInstance();
        $instance->_root = $root;
        $instance->_cache = $cache;

        return $instance;
    }


    public function thumbnail($sourceFile, $width, $height, $mode)
    {
        if(!$sourceFile)
        {
            return false;
        }
        $destinationFile = self::getFileName($sourceFile, $width, $height, $mode);
        if (true || self::checkFile($sourceFile, $destinationFile)) {
            $imagine = new Imagine();
            $size = new Box($width, $height);
            $imagine->open($this->_root . $sourceFile)->thumbnail($size, $mode)->save($this->_root . $destinationFile);
            touch($this->_root . $destinationFile, filemtime($this->_root . $sourceFile));
        }

        return $destinationFile;
    }

    public function crop($sourceFile, $x, $y, $width, $height, $mode = '')
    {
        $destinationFile = self::getFileName($sourceFile, $width, $height, $x.'_'.$y.'crop_'.$mode);
        if (true || self::checkFile($sourceFile, $destinationFile)) {
            $imagine = new Imagine();
            $point = new Point($x, $y);
            $size = new Box($width, $height);
            if ($mode == 'flip') {
                $imagine->open($this->_root . $sourceFile)->crop($point, $size)->flipHorizontally()->save($this->_root . $destinationFile);
            } else {
                $imagine->open($this->_root . $sourceFile)->crop($point, $size)->save($this->_root . $destinationFile);
            }

            touch($this->_root . $destinationFile, filemtime($this->_root . $sourceFile));
        }

        return $destinationFile;
    }


    public function flip($sourceFile, $width, $height, $mode)
    {
        $destinationFile = self::getFileName($sourceFile, $width, $height, $mode . '_flip');
        if (true || self::checkFile($sourceFile, $destinationFile)) {
            $imagine = new Imagine();
            $size = new Box($width, $height);
            $imagine->open($this->_root . $sourceFile)->thumbnail($size, $mode)->flipHorizontally()->save($this->_root . $destinationFile);
            touch($this->_root . $destinationFile, filemtime($this->_root . $sourceFile));
        }

        return $destinationFile;
    }


    private function getFileName($sourceFile, $width, $height, $prefix)
    {
        $pathInfo = pathinfo($sourceFile);
        $destinationFile = $this->_cache . trim($pathInfo["dirname"], '/') . '/' . $prefix . "_" . $width . "_" . $height . "_" . $pathInfo["basename"];
        $dir = $this->_root . dirname($destinationFile);
        if (!file_exists($dir)) {
            mkdir($dir, 0777, true);
        }

        return $destinationFile;
    }

    private function checkFile($sourceFile, $destinationFile)
    {
        if (!file_exists($this->_root . $sourceFile)) {
            return false;
        }
        if (!file_exists($this->_root . $destinationFile)) {
            return true;
        }
        if (file_exists($this->_root . $sourceFile)) {
            $sourceTime = filemtime($this->_root . $sourceFile);
            $destinationTime = filemtime($this->_root . $destinationFile);

            return $sourceTime != $destinationTime;
        }

        return true;
    }
}