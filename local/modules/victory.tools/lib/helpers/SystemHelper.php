<?php

namespace victory\helpers;

/**
 * @author Kirshin Alexandr <kirshin.as@gmail.com>
 *
 * Date: 13.12.13
 * Time: 10:04
 */
class SystemHelper
{

    /**
     * Добавить сообщение
     * @param $code
     * @param $value
     */
    public static function setFlash($code, $value)
    {
        $_SESSION['flash'][$code] = $value;
    }

    /**
     * Проверить наличие сообщения
     * @param $code
     * @return bool
     */
    public static function hasFlash($code)
    {
        return isset($_SESSION['flash'][$code]);
    }

    /**
     * Получить сообщение
     * @param $code
     * @return mixed
     */
    public static function getFlash($code)
    {
        $value = $_SESSION['flash'][$code];
        unset($_SESSION['flash'][$code]);

        return $value;
    }

    /**
     * Проверить текущий запрос является он Ajax запросом
     * @return bool
     */
    public static function isAjax()
    {
        return (isset($_SERVER['HTTP_X_REQUESTED_WITH']) &&
            ($_SERVER['HTTP_X_REQUESTED_WITH'] == 'XMLHttpRequest'));
    }

}