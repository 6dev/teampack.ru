<?php
/**
 * @author Alexandr Kirshin <kirshin.as@gmail.com>
 */
namespace victory\helpers;

class Sort
{

    public $sort;
    public $order;


    public $fields;

    public function __construct($sort, $order, $dField, $dOrder)
    {
        $this->sort = $_GET[$sort] ? $_GET[$sort] : $dField;
        $this->order = $_GET[$order] ? $_GET[$order] : $dOrder;
    }

    public function add($id, $field, $label, $up = 'up', $down = 'down')
    {
        global $APPLICATION;
        $this->fields[$id] = array(
            'field'  => $field,
            'label'  => $label,
            'active' => $id == $this->sort,
            'link'   => $APPLICATION->GetCurPageParam('sort=' . $id . '&order=' . ($this->order == 'asc' ? 'desc' : 'asc'), array('sort', 'order')),
            'order'  => $this->order == 'asc' ? $up : $down
        );
    }

    public function getField()
    {
        return $this->fields[$this->sort]['field'];
    }

    public function getOrder()
    {
        return $this->order;
    }

}