<?php
/**
 * @author Alexandr Kirshin <kirshin.as@gmail.com>
 */
namespace victory\helpers;

class Paging
{

    public $countName;
    public $count;


    public $fields;

    public function __construct($countName, $dCount)
    {
        $this->countName = $countName;
        $this->count = $_GET[$countName] ? $_GET[$countName] : $dCount;
    }

    public function add($id, $label)
    {
        global $APPLICATION;
        $this->fields[$id] = array(
            'label'  => $label,
            'active' => $id == $this->count,
            'link'   => $APPLICATION->GetCurPageParam($this->countName . '=' . $id, array($this->countName))
        );
    }

    public function getCount()
    {
        return $this->count;
    }


}