<?php
/**
 * Created by PhpStorm.
 * User: Александр
 * Date: 20.02.14
 * Time: 14:35
 */

namespace victory\helpers;

class FormatHelper
{


    /**
     * Форматирование размера файла
     * @param $file
     * @param int $precision
     * @return string
     */
    public static function  formatBytes($file, $precision = 2)
    {
        $bytes = @filesize($_SERVER['DOCUMENT_ROOT'] . $file);
        $units = array('b', 'kb', 'mb', 'gb', 'tb');

        $bytes = max($bytes, 0);
        $pow = floor(($bytes ? log($bytes) : 0) / log(1024));
        $pow = min($pow, count($units) - 1);

        $bytes /= pow(1024, $pow);

        return round($bytes, $precision) . ' ' . $units[$pow];
    }

    /**
     * Форматирование множественной формы
     * @param $n
     * @param string $f1
     * @param string $f2
     * @param string $f3
     * @return string
     */
    public static function formatPlural($n, $f1 = 'сообщение', $f2 = 'сообщения', $f3 = 'сообщений')
    {
        return $n % 10 == 1 && $n % 100 != 11 ? $f1 : ($n % 10 >= 2 && $n % 10 <= 4 && ($n % 100 < 10 || $n % 100 >= 20) ? $f2 : $f3);
    }

    /**
     * @param $value
     * @param int $precision
     * @param int $mode
     * @return string
     */
    public static function  formatMoney($value, $precision = 2, $mode = 0)
    {

        $units = array('', 'тыс', 'млн', 'трл');

        $value = max($value, 0);
        $pow = floor(($value ? log($value) : 0) / log(1000));
        $pow = min($pow, count($units) - 1);

        $value /= pow(1000, $pow);
        switch ($mode) {
            case 0:
                return number_format($value, $precision, ',', ' ') . ' ' . $units[$pow];
            case 1:
                return number_format($value, $precision, ',', ' ');
            case 2:
                return
                    $units[$pow];
        }
    }
}