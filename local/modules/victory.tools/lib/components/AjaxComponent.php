<?php

namespace victory\components;

/**
 * Created by PhpStorm.
 * User: Александр
 * Date: 18.02.14
 * Time: 15:57
 */
class AjaxComponent extends \CBitrixComponent
{
    protected function isAjax()
    {
        return (isset($_SERVER['HTTP_X_REQUESTED_WITH']) &&
            ($_SERVER['HTTP_X_REQUESTED_WITH'] == 'XMLHttpRequest'));
    }

    protected function checkIdComponent()
    {
        return $_REQUEST['component_id'] == $this->getIdComponent();
    }

    protected function getIdComponent()
    {
        return md5($this->getName() . '_' . $this->getTemplateName() . '_' . json_encode($this->arParams));
    }

    public function executeComponent()
    {
        global $APPLICATION;
        if ($this->isAjax() && $this->checkIdComponent()) {
            $APPLICATION->RestartBuffer();
        }
        $this->arResult['COMPONENT_ID'] = $this->getIdComponent();
        $this->arResult['COMPONENT_ID_INPUT'] = '<input name="component_id" type="hidden" value="' . $this->getIdComponent() . '">';
        $this->executeAjaxComponent();
        if ($this->isAjax() && $this->checkIdComponent()) {
            die();
        }
    }

    public function executeAjaxComponent()
    {

    }
}