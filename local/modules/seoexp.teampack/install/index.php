<?php
/**
 * @author: Alexandr Kirshin <kirshin.as@gmail.com>
 */
IncludeModuleLangFile(__FILE__);

class seoexp_teampack extends CModule
{
    var $MODULE_ID = "seoexp.teampack";
    public $MODULE_VERSION;
    public $MODULE_VERSION_DATE;
    public $MODULE_NAME;
    public $MODULE_DESCRIPTION;

    public function seoexp_teampack()
    {
        $arModuleVersion = array();
        include(__DIR__ . "/version.php");
        if (is_array($arModuleVersion) && array_key_exists("VERSION", $arModuleVersion)) {
            $this->MODULE_VERSION = $arModuleVersion["VERSION"];
            $this->MODULE_VERSION_DATE = $arModuleVersion["VERSION_DATE"];
        }

        $this->MODULE_NAME = GetMessage("MODULE_NAME_TEAMPACK");
        $this->MODULE_DESCRIPTION = GetMessage("MODULE_DESC_TEAMPACK");
        $this->PARTNER_NAME = GetMessage("MODULE_PARTNER_TEAMPACK");
        $this->PARTNER_URI = GetMessage("MODULE_URI_TEAMPACK");
    }

    public function DoInstall()
    {
        RegisterModuleDependences('sale', 'OnSaleComponentOrderOneStepOrderProps', $this->MODULE_ID, 'AppEvents', 'OnSaleComponentOrderOneStepOrderProps');
        RegisterModuleDependences('iblock', 'OnAfterIBlockElementAdd', $this->MODULE_ID, 'AppEvents', 'OnAfterIBlockElementAddHandler');
        RegisterModuleDependences('iblock', 'OnBeforeIBlockElementUpdate', $this->MODULE_ID, 'AppEvents', 'OnBeforeIBlockElementUpdateHandler');
        RegisterModuleDependences('iblock', 'OnAfterIBlockElementUpdate', $this->MODULE_ID, 'AppEvents', 'OnAfterIBlockElementUpdateHandler');
        RegisterModuleDependences('iblock', 'OnBeforeIBlockSectionUpdate', $this->MODULE_ID, 'AppEvents', 'OnBeforeIBlockSectionUpdateHandler');
        RegisterModuleDependences('iblock', 'OnBeforeIBlockSectionDelete', $this->MODULE_ID, 'AppEvents', 'OnBeforeIBlockSectionDeleteHandler');

        RegisterModuleDependences('catalog', 'OnBeforePriceUpdate', $this->MODULE_ID, 'AppEvents', 'OnBeforePriceUpdateHandler');
        RegisterModuleDependences('sale', 'OnSaleCalculateOrderShoppingCart', $this->MODULE_ID, 'AppEvents', 'OnSaleCalculateOrderShoppingCartHandler');
        RegisterModuleDependences('sale', 'OnSaleCalculateOrderDelivery', $this->MODULE_ID, 'AppEvents', 'OnSaleCalculateOrderDeliveryHandler');


        RegisterModule($this->MODULE_ID);
    }

    public function DoUninstall()
    {
        UnRegisterModuleDependences('sale', 'OnSaleComponentOrderOneStepOrderProps', $this->MODULE_ID, 'AppEvents', 'OnSaleComponentOrderOneStepOrderProps');
        UnRegisterModuleDependences('iblock', 'OnAfterIBlockElementAdd', $this->MODULE_ID, 'AppEvents', 'OnAfterIBlockElementAddHandler');
        UnRegisterModuleDependences('iblock', 'OnAfterIBlockElementUpdate', $this->MODULE_ID, 'AppEvents', 'OnAfterIBlockElementUpdateHandler');
        UnRegisterModuleDependences('iblock', 'OnBeforeIBlockSectionUpdate', $this->MODULE_ID, 'AppEvents', 'OnBeforeIBlockSectionUpdateHandler');
        UnRegisterModuleDependences('iblock', 'OnBeforeIBlockSectionDelete', $this->MODULE_ID, 'AppEvents', 'OnBeforeIBlockSectionDeleteHandler');
        UnRegisterModuleDependences('catalog', 'OnBeforePriceUpdate', $this->MODULE_ID, 'AppEvents', 'OnBeforePriceUpdateHandler');
        UnRegisterModuleDependences('sale', 'OnSaleCalculateOrderShoppingCart', $this->MODULE_ID, 'AppEvents', 'OnSaleCalculateOrderShoppingCartHandler');
        UnRegisterModuleDependences('sale', 'OnSaleCalculateOrderDelivery', $this->MODULE_ID, 'AppEvents', 'OnSaleCalculateOrderDeliveryHandler');


        UnRegisterModule($this->MODULE_ID);
    }
}
