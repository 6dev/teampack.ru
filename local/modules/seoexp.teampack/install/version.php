<?php
/**
 * @author: Alexandr Kirshin <kirshin.as@gmail.com>
 */

$arModuleVersion = array(
    "VERSION" => "1.0.1",
    "VERSION_DATE" => "2016-08-08 10:00:00",
);