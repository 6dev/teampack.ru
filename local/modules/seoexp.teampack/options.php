<?php
/**
 * @author Alexandr Kirshin <kirshin.as@gmail.com>
 */
global $USER, $APPLICATION;
if (!$USER->IsAdmin()) {
    return;
}
CUtil::InitJSCore('jquery');

    $moduleId = 'seoexp.teampack';

CModule::IncludeModule('catalog');
$res = CCatalogGroup::GetList(array('SORT' => 'ASC'));
$groups = array();
while ($arGroup = $res->GetNext()) {
    $groups[$arGroup['ID']] = $arGroup['NAME'];
}

$aTabs = array(
    array("DIV" => "edit1", "TAB" => "Параметры", "ICON" => "ib_settings", "TITLE" => "Параметры"),
);
$tabControl = new CAdminTabControl("tabControl", $aTabs);

if ($_SERVER['REQUEST_METHOD'] == "POST" && !empty($_POST['Apply']) && check_bitrix_sessid()) {

    $ranges = array();
    foreach ($_POST['ranges'] as $code => $values) {
        foreach ($values as $i => $value) {
                $ranges[$i][$code] = $value;
        }
    }
    COption::SetOptionString($moduleId, 'ranges', json_encode($ranges));
    COption::SetOptionString($moduleId, 'sale_price_type', htmlspecialchars($_POST['sale_price_type']));

    LocalRedirect($APPLICATION->GetCurPage() . "?mid=" . urlencode($mid) . "&lang=" . urlencode(LANGUAGE_ID) . "&back_url_settings=" . urlencode($_REQUEST["back_url_settings"]) . "&" . $tabControl->ActiveTabParam());
}

$ranges = json_decode(COption::GetOptionString($moduleId, 'ranges'), true);
$ranges = $ranges ? $ranges : array(array('min' => 0, 'max' => 0, 'group' => 0));
$sale_price_type = COption::GetOptionString($moduleId, 'sale_price_type');
$tabControl->Begin();
?>
<form method="post" action="<? echo $APPLICATION->GetCurPage() ?>?mid=<?= urlencode($mid) ?>&amp;lang=<? echo LANGUAGE_ID ?>">
    <? $tabControl->BeginNextTab(); ?>
    <tr>
        <td>
            Тип цены распродажа
        </td>
        <td colspan="4">
            <select name="sale_price_type">
                <? foreach ($groups as $id => $name): ?>
                    <option value="<?= $id ?>" <? if ($sale_price_type == $id): ?>selected="selected" <? endif; ?>><?= $name ?></option>
                <? endforeach; ?>
            </select>
        </td>
    </tr>
    <tr>
        <td style="width: 100px;">
            Минимальная цена заказа
        </td>
        <td style="width: 100px;">
            Максимальная цена заказа
        </td>
        <td style="width: 100px;">
            Тип цены
        </td>
        <td>

        </td>
    </tr>

    <? foreach ($ranges as $i => $range): ?>
        <tr class="range-row">
            <td style="width: 100px;">

                <input type="text" name="ranges[min][]" value="<?= $range['min'] ?>"/>
            </td>
            <td style="width: 100px;">
                <input type="text" name="ranges[max][]" value="<?= $range['max'] ?>"/>
            </td>
            <td style="width: 100px;">
                <select name="ranges[group][]">
                    <? foreach ($groups as $id => $name): ?>
                        <option value="<?= $id ?>" <? if ($range['group'] == $id): ?>selected="selected" <? endif; ?>><?= $name ?></option>
                    <? endforeach; ?>
                </select>
            </td>
            <td>
                <input type="button" value="Удалить" class="del-btn"/>
            </td>
        </tr>
    <? endforeach ?>
    <tr>
        <td colspan="4">
            <input type="button" value="Добавить" class="add-btn"/>
        </td>
    </tr>

    <? $tabControl->Buttons(); ?>
    <input type="submit" name="Apply" value="Сохранить">
    <?= bitrix_sessid_post(); ?>
    <? $tabControl->End(); ?>
</form>
<script>
    $(document).ready(function () {
        $('.add-btn').click(function () {
            var $r = $('.range-row:last').clone();
            $r.find('input[type="text"]').val('');
            $('.range-row:last').after($r);

            delClick();

            return false;
        });

        function delClick() {
            $('.del-btn').click(function () {
                if ($('.range-row').length > 1) {
                    $(this).parent().parent().remove();
                }
                return false;
            });
        }

        delClick();

    });
</script>