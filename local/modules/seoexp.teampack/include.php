<?php
/**
 * @author Alexandr Kirshin <kirshin.as@gmail.com>
 */

define('CATALOG_IBLOCK_ID', 2);
include_once 'classes/DiscountCalculator.php';
include_once 'classes/AppEvents.php';
include_once 'classes/CatalogMapper.php';
include_once 'classes/CCatalogProductProviderCustom.php';
include_once 'classes/DeliveryCalculator.php';

CatalogMapper::init();