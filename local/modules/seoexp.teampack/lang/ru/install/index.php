<?php
/**
  * @author: Alexandr Kirshin <kirshin.as@gmail.com> 
 */
$MESS['MODULE_NAME_TEAMPACK'] = "TeamPack";
$MESS['MODULE_DESC_TEAMPACK'] = "Модуль TeamPack";
$MESS['MODULE_PARTNER_TEAMPACK'] = "СЕО Эксперт, Россия";
$MESS['MODULE_URI_TEAMPACK'] = "http://seo-experts.com";