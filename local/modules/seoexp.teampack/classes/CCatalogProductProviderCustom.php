<?php
/**
 * @author Alexandr Kirshin <kirshin.as@gmail.com>
 */

if (!CModule::IncludeModule("sale") || !CModule::IncludeModule("catalog")) {
    return false;
}

IncludeModuleLangFile(__FILE__);

class CCatalogProductProviderCustom extends CCatalogProductProvider
{

}