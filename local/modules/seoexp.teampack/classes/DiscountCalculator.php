<?php


class DiscountCalculator
{
    public static function getDiscounts($items)
    {
        global $USER;
        $sum = self::getBaseSum($items);
        $priceGroup = self::getPriceGroup($sum);
        foreach ($items as &$item) {
            if ($obProduct = CIBlockElement::GetList(array(), array('ID' => $item["PRODUCT_ID"]))->GetNextElement()) {
                $priceGroupProduct = null;
                $arProduct = $obProduct->GetFields();
                $arProduct['PROPERTIES'] = $obProduct->GetProperties();
                if ($arProduct['PROPERTIES']['sale']['VALUE'] == 'Y') {
                    $priceGroupProduct = CCatalogGroup::GetByID(
                        COption::GetOptionString('seoexp.teampack', 'sale_price_type')
                    );
                }
                $priceGroupProduct = $priceGroupProduct ? $priceGroupProduct : $priceGroup;
                $db_res = CPrice::GetList(
                    array(),
                    array(
                        "PRODUCT_ID"       => $item["PRODUCT_ID"],
                        "CATALOG_GROUP_ID" => $priceGroupProduct['ID'],
                    )
                );
                if ($ar_res = $db_res->Fetch()) {
                    $item['PRODUCT_PRICE_ID'] = $ar_res["ID"];
                    $item['PRICE'] = $ar_res["PRICE"];
                    $item['NOTES'] = $priceGroupProduct['NAME'];
                    $item['~PRICE'] = $ar_res["PRICE"];
                    $item['~NOTES'] = $priceGroupProduct['NAME'];
                    $sum += $ar_res["PRICE"] * $item['QUANTITY'];
                    $basePrice = CPrice::GetBasePrice($item["PRODUCT_ID"]);
                    if ($arProduct['PROPERTIES']['sale']['VALUE'] == 'Y') {
                        $item['DISCOUNT_PRICE'] = 0;
                    } else {
                        $item['DISCOUNT_PRICE'] = $basePrice['PRICE'] - $ar_res["PRICE"];
                    }
                }
            }
        }

        return $items;
    }

    public static function getBaseSum($items)
    {
        $baseSum = 0;
        foreach ($items as $item) {
            $arBasePrice = CPrice::GetBasePrice($item['PRODUCT_ID']);
            $baseSum += $arBasePrice['PRICE'] * $item['QUANTITY'];
        }

        return $baseSum;
    }

    public static function getPriceGroup($price)
    {
        $ranges = json_decode(COption::GetOptionString('seoexp.teampack', 'ranges'), true);

        foreach ($ranges as $r) {
            if ($r['max'] == 0) {
                $r['max'] = PHP_INT_MAX;
            }
            if ($price >= $r['min'] && $price < $r['max']) {

                return CCatalogGroup::GetByID($r['group']);
            }
        }

        return false;
    }

    public static function getSum($items)
    {
        $sum = 0;
        foreach ($items as $item) {
            $sum += $item["PRICE"] * $item['QUANTITY'];
        }

        return $sum;
    }
}