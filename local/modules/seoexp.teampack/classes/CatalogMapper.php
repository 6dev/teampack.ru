<?php

/**
 * @author Alexandr Kirshin <kirshin.as@gmail.com>
 */
class CatalogMapper
{

    private static $_cache = array();

    public static $propertyMap;

    public static function init()
    {
        self::$propertyMap = array(
            'article'  => 'CML2_ARTICLE',
            'capacity' => 'VES_VLOZHENIYA_G_NOMENKLATURA',
            'hit'      => 'hit',
            'novelty'  => 'novelty',
            'sale'     => 'sale',
            'discount' => 'discount'
        );
    }


    public static function getPropertyValue($arElement, $property)
    {
        $property = self::$propertyMap[$property] ? self::$propertyMap[$property] : $property;

        return $arElement['PROPERTIES'][$property]['VALUE'];
    }
    
	private static function __replace_prepare_callback($matches){
		//var_dump($matches);
		
		return '&nbsp;'.$matches[1].'&nbsp;'.$matches[2].'&nbsp;р.';
	}
    
    private static function preparePrice($str){
		return preg_replace_callback('/\s([0-9]+)\s([0-9]+)\sр\./i', array('CatalogMapper','__replace_prepare_callback'), $str);
    }

    public static function getPriceTypeNameByCode($priceCode)
    {
        if (self::$_cache['price_type'][$priceCode]) {
            return self::preparePrice(self::$_cache['price_type'][$priceCode]['NAME_LANG']);
        }
        $resGroups = CCatalogGroup::GetList(array(), array());
        while ($arGroup = $resGroups->GetNext()) {
            self::$_cache['price_type'][$arGroup['NAME']] = $arGroup;
        }
        
        //var_dump(self::$_cache['price_type'][$priceCode]['NAME_LANG']);
        
        return self::preparePrice(self::$_cache['price_type'][$priceCode]['NAME_LANG']);
    }
}