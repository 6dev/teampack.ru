<?php

/**
 * @author Alexandr Kirshin <kirshin.as@gmail.com>
 */
class DeliveryCalculator
{

    const KM_PRICE = 25;

    public static function getPriceMatrix()
    {
        return array(
            array('min' => 0, 'max' => 1, 'price' => 600),
            array('min' => 1, 'max' => 8, 'price' => 3000),
            array('min' => 8, 'max' => 36, 'price' => 5400),
            array('min' => 36, 'max' => PHP_INT_MAX, 'price' => 9500),
        );
    }

    /**
     * Расчет стоимости доставки и массы заказа
     * @param $deliveryId int Способ доставки
     * @param $basketItems [] Позициив корзине
     * @param $distance int Расстояние за мкад
     * @return array
     */
    public static function calc($deliveryId, $basketItems, $distance = 0)
    {
        $prices = self::getPriceMatrix();
        $mass = self::calcMass($basketItems);
        $volume = self::calcVolume($basketItems);
        $delivery = 0;
        switch ($deliveryId) {
            case 1:
                foreach ($prices as $p) {
                    if ($volume >= $p['min'] && $volume < $p['max']) {
                        $delivery = $p['price'];
                        break;
                    }
                }
                break;
            case 2:
                foreach ($prices as $p) {
                    if ($volume >= $p['min'] && $volume < $p['max']) {
                        $delivery = $p['price'];
                        break;
                    }
                }
                $delivery += $distance * self::KM_PRICE;
                break;
        }

        return array($delivery, $mass);
    }

    /**
     * Расчет объема груза
     * @param $basketItems
     * @return int
     */
    public static function calcVolume($basketItems)
    {
        $volume = 0;
        foreach ($basketItems as $arItem) {
            $arProduct = CIBlockElement::GetList(
                [],
                ['ID' => $arItem['PRODUCT_ID']],
                false,
                false,
                ['PROPERTY_OBEM_KOROBKI_TEKST_NOMENKLATURA']
            )->GetNext();
            $volumeBox = floatval(
                str_replace(',', '.', $arProduct['PROPERTY_OBEM_KOROBKI_TEKST_NOMENKLATURA_VALUE'])
            );
            $volume += $volumeBox * $arItem['QUANTITY'];
        }

        return $volume;
    }

    /**
     * Расчет массы груза
     * @param $basketItems
     * @return int
     */
    public static function calcMass($basketItems)
    {
        $mass = 0;
        foreach ($basketItems as $arItem) {
            $arProduct = CIBlockElement::GetList(
                [],
                ['ID' => $arItem['PRODUCT_ID']],
                false,
                false,
                ['PROPERTY_VES_KOROBKI_NOMENKLATURA']
            )->GetNext();
            $massBox = floatval(str_replace(',', '.', $arProduct['PROPERTY_VES_KOROBKI_NOMENKLATURA_VALUE']));
            $mass += $massBox * $arItem['QUANTITY'];
        }

        return $mass;
    }
} 