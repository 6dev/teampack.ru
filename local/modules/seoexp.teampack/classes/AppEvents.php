<?php
use Bitrix\Main\Application;
use victory\helpers\IBlockHelper;

/**
 * @author Alexandr Kirshin <kirshin.as@gmail.com>
 */
class AppEvents
{
    private static $_cache;

    function OnSaleComponentOrderOneStepOrderProps(&$arResult, &$arUserResult)
    {
        //Местоположение по умолчанию
        $arUserResult['DELIVERY_LOCATION'] = 1;
    }

    function OnAfterIBlockElementAddHandler(&$arFields)
    {
        if ($arFields['IBLOCK_ID'] == CATALOG_IBLOCK_ID) {
            self::updateCatalogElement($arFields['ID']);
        }
    }

    function OnBeforeIBlockElementUpdateHandler(&$arFields)
    {
        if ($arFields['IBLOCK_ID'] == CATALOG_IBLOCK_ID) {
            if (self::isSyncProcess()) {
                if (is_array($arFields['IBLOCK_SECTION'])) {
                    $arOldGroups = array();
                    $resGroups = CIBlockElement::GetElementGroups($arFields['ID'], true);
                    while ($arOldGroup = $resGroups->Fetch()) {
                        $arOldGroups[] = $arOldGroup["ID"];
                    }
                    $arFields['IBLOCK_SECTION'] = array_merge($arFields['IBLOCK_SECTION'], $arOldGroups);
                }
            }
        }
    }

    function OnAfterIBlockElementUpdateHandler(&$arFields)
    {
        if ($arFields['IBLOCK_ID'] == CATALOG_IBLOCK_ID) {
            self::updateCatalogElement($arFields['ID']);
        }
    }

    function updateCatalogElement($ID)
    {
        if (self::isSyncProcess()) {

            $properties = CIBlockElement::GetList(array(), array('ID' => $ID))->GetNextElement()->GetProperties();
            if ($properties['CML2_ARTICLE']['VALUE']) {

                foreach (self::getMaterials() as $code => $idM) {

                    if (strpos($properties['CML2_ARTICLE']['VALUE'], $code) === 0) {

                        CIBlockElement::SetPropertyValuesEx($ID, CATALOG_IBLOCK_ID, array(38 => $idM));

                    }
                }
            }

            foreach (self::getWeightCategory() as $weight) {

                $min = intval($weight['PROPERTY_MIN_VALUE']);
                $max = intval($weight['PROPERTY_MAX_VALUE']);
                if (!$properties['VES_VLOZHENIYA_G_NOMENKLATURA']['VALUE']) {
                    $properties['VES_VLOZHENIYA_G_NOMENKLATURA']['VALUE'] = 1;
                }

                $value = intval(preg_replace("/[^0-9]/", "", $properties['VES_VLOZHENIYA_G_NOMENKLATURA']['VALUE']));

                if ($value > $min && $value <= $max) {
                    CIBlockElement::SetPropertyValuesEx($ID, CATALOG_IBLOCK_ID, array(39 => $weight['ID']));
                }
                CIBlockElement::SetPropertyValuesEx(
                    $ID,
                    CATALOG_IBLOCK_ID,
                    array('VES_VLOZHENIYA_G_NOMENKLATURA' => $value)
                );
            }


            /*
        * TODO: Раскомментировать если нужна синхронизация из 1С признак продано
               $arProduct = CCatalogProduct::GetByID($ID);
               if ($arProduct['QUANTITY'] <= 0) {
                   CIBlockElement::SetPropertyValuesEx($ID, CATALOG_IBLOCK_ID, array(44 => 15));
               } else {
                   CIBlockElement::SetPropertyValuesEx($ID, CATALOG_IBLOCK_ID, array(44 => false));
               }
       */

        }


    }

    function getMaterials()
    {
        if (self::$_cache['materials']) {
            return self::$_cache['materials'];
        }
        $res = CIBlockElement::GetList(array(), array('IBLOCK_ID' => 5));
        while ($arMaterial = $res->GetNext()) {
            self::$_cache['materials'][$arMaterial['CODE']] = $arMaterial['ID'];
        }

        return self::$_cache['materials'];
    }

    function getWeightCategory()
    {
        if (self::$_cache['weight']) {
            return self::$_cache['weight'];
        }
        $res = CIBlockElement::GetList(
            array('PROPERTY_min' => 'asc'),
            array('IBLOCK_ID' => 6),
            false,
            false,
            array('ID', 'PROPERTY_min', 'PROPERTY_max')
        );
        while ($arWeight = $res->GetNext()) {

            if (!$arWeight['PROPERTY_MAX_VALUE']) {
                $arWeight['PROPERTY_MAX_VALUE'] = PHP_INT_MAX;
            }
            self::$_cache['weight'][$arWeight['ID']] = $arWeight;
        }

        return self::$_cache['weight'];
    }

    /**
     * Обработка обновления раздела, если стоит признак не синхронизировать  с 1С раздел игнорируеться
     *
     * @param $arFields
     *
     * @return bool
     */
    function OnBeforeIBlockSectionUpdateHandler(&$arFields)
    {
        if (self::isSyncProcess()) {
            $arSection = CIBlockSection::GetByID($arFields['ID'])->Fetch();
            while ($arSection) {
                $notSync = IBlockHelper::getUserFieldSection(CATALOG_IBLOCK_ID, $arSection['ID'], 'UF_SYNC');
                if ($notSync) {
                    $arFields = $arSection;

                    return true;
                }
                if ($arSection['IBLOCK_SECTION_ID']) {
                    $arSection = CIBlockSection::GetByID($arSection['IBLOCK_SECTION_ID'])->Fetch();
                } else {
                    return true;
                }
            }
        }
    }

    /**
     * Обработка удаления раздела, если стоит признак не синхронизировать  с 1С раздел игнорируеться
     *
     * @param $ID
     *
     * @return bool
     */
    function OnBeforeIBlockSectionDeleteHandler($ID)
    {
        if (self::isSyncProcess()) {
            $arSection = CIBlockSection::GetByID($ID)->Fetch();
            while ($arSection) {
                $notSync = IBlockHelper::getUserFieldSection(CATALOG_IBLOCK_ID, $arSection['ID'], 'UF_SYNC');
                if ($notSync) {
                    return false;
                }
                if ($arSection['IBLOCK_SECTION_ID']) {
                    $arSection = CIBlockSection::GetByID($arSection['IBLOCK_SECTION_ID'])->Fetch();
                } else {
                    return true;
                }
            }
        }
    }

    /**
     * Оброботка обновления цены, если раздел которому принадлжеит элемент не должен синхронизироваться изминения игнорируються
     *
     * @param $ID
     * @param $arFields
     *
     * @return bool
     */
    function OnBeforePriceUpdateHandler($ID, &$arFields)
    {
        if (self::isSyncProcess()) {
            $arElement = CIBlockElement::GetByID($arFields['PRODUCT_ID'])->Fetch();
            $arSection = CIBlockSection::GetByID($arElement['IBLOCK_SECTION_ID'])->Fetch();
            while ($arSection) {
                $notSync = IBlockHelper::getUserFieldSection(CATALOG_IBLOCK_ID, $arSection['ID'], 'UF_SYNC');
                if ($notSync) {
                    return false;
                }
                if ($arSection['IBLOCK_SECTION_ID']) {
                    $arSection = CIBlockSection::GetByID($arSection['IBLOCK_SECTION_ID'])->Fetch();
                } else {
                    return true;
                }
            }
        }
    }

    /**
     * Проверка на выполнение процесса синхронизации
     * @return bool
     */
    public function isSyncProcess()
    {
        return $_SERVER['SCRIPT_NAME'] == '/bitrix/admin/1c_exchange.php';
    }

    public function OnBeforeBasketAddHandler(&$arFields)
    {
        $arFields["MODULE"] = "seoexp.teampack";
        $arFields["PRODUCT_PRICE_ID"] = 0;
        $arFields["PRODUCT_PROVIDER_CLASS"] = 'CCatalogProductProviderCustom';
    }

    /**
     * Расчет скидок
     */
    public function OnSaleCalculateOrderShoppingCartHandler(&$arOrder)
    {
        $arOrder['BASKET_ITEMS'] = DiscountCalculator::getDiscounts($arOrder['BASKET_ITEMS']);
        $arOrder['ORDER_PRICE'] = DiscountCalculator::getSum($arOrder['BASKET_ITEMS']);
        $arOrder['VAT_SUM'] = 0;
    }

    /***
     * Расчет стоимости доставки
     */
    public function OnSaleCalculateOrderDeliveryHandler(&$arOrder)
    {
        $distance = $arOrder['ORDER_PROP'][26];
        if ($orderId = Application::getInstance()->getContext()->getRequest()->get('id')) {
            $arDistance = CSaleOrderPropsValue::GetList([], ['ORDER_ID' => $orderId, 'ORDER_PROPS_ID' => 26])->GetNext(
            );
            $distance = $arDistance['VALUE'];
        }
        list($price, $weight) = DeliveryCalculator::calc(
            $arOrder['DELIVERY_ID'],
            $arOrder['BASKET_ITEMS'],
            $distance
        );
        $arOrder['PRICE_DELIVERY'] = $price;
        $arOrder['DELIVERY_PRICE'] = $price;

    }

}