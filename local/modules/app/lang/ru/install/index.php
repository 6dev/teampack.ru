<?php
/**
 * @author Alexandr Kirshin <kirshin.as@gmail.com>
 */
$MESS['MODULE_APP_NAME'] = 'Модуль сайта';
$MESS['MODULE_APP_DESC'] = '';
$MESS['MODULE_APP_VENDOR_NAME'] = 'СЕО Эксперт, Россия';
$MESS['MODULE_APP_VENDOR_URL'] = 'http://seo-experts.com';