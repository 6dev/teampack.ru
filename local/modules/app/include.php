<?php
/**
 * @author Alexandr Kirshin <kirshin.as@gmail.com>
 */
define('PHPEXCEL_ROOT', dirname(__FILE__) . '/vendor/phpoffice/phpexcel/Classes/');

include_once __DIR__ . '/vendor/autoload.php';
