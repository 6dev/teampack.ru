<?php


use Bitrix\Main\EventManager;

IncludeModuleLangFile(__FILE__);

/**
 * @author Kirshin Alexandr <kirshin.as@gmail.com>
 */
class app extends CModule
{
    var  $MODULE_ID = 'app';
    var $PARTNER_NAME = 'SEO Experts, Russia';
    var $PARTNER_URI = 'http://seo-experts.com';

    function app()
    {
        $arModuleVersion = array();
        include("version.php");
        if (is_array($arModuleVersion) && array_key_exists("VERSION", $arModuleVersion)) {
            $this->MODULE_VERSION = $arModuleVersion["VERSION"];
            $this->MODULE_VERSION_DATE = $arModuleVersion["VERSION_DATE"];
        }
        $this->MODULE_NAME =GetMessage('MODULE_APP_NAME');
        $this->MODULE_DESCRIPTION = GetMessage('MODULE_APP_DESC');
        $this->PARTNER_NAME = GetMessage('MODULE_APP_VENDOR_NAME');
        $this->PARTNER_URI = GetMessage('MODULE_APP_VENDOR_URL');
    }

    function DoInstall()
    {
        RegisterModule($this->MODULE_ID);

    }

    function DoUninstall()
    {

        UnRegisterModule($this->MODULE_ID);
    }
}