<?php

namespace seoexp\data;

/**
 * @author Alexandr Kirshin <kirshin.as@gmail.com>
 */
class Model
{

    public $scope;
    public $attributes = array();
    public $errors = array();
    public $rules = array();

    public function __construct($scope = 'model')
    {
        $this->scope = $scope;
        $this->init();
    }

    /**
     * Инициализация модели
     */
    protected function init()
    {

    }

    /**
     * Валидация модели
     * @return bool
     */
    public function validate()
    {
        foreach ($this->rules as $rule) {
            $attribute = $this->attributes[$rule[0]];
            $params['{name}'] = $attribute['name'];
            $params['{label}'] = $attribute['label'];

            switch ($rule[1]) {
                case 'required':
                    if (empty($attribute['value'])) {
                        $this->addError($attribute['name'], isset($rule['message']) ? strtr($rule['message'],
                                $params) : "Необходимо заполнить «{$attribute['label']}».");
                    }
                    break;
                case 'integer':
                    if (!empty($attribute['value']) && !preg_match('/^[\d]+$/', $attribute['value'])) {
                        $this->addError($attribute['name'], isset($rule['message']) ? strtr($rule['message'],
                                $params) : "Поле «{$attribute['label']}» не является числом.");
                    }
                    break;
                case 'datetime':
                    if (!empty($attribute['value']) && !strtotime($attribute['value'])) {
                        $this->addError($attribute['name'], isset($rule['message']) ? strtr($rule['message'],
                                $params) : "Поле «{$attribute['label']}» не является датой.");
                    }
                    break;
                case 'email':
                    if (!empty($attribute['value']) && !check_email($attribute['value'])) {
                        $this->addError($attribute['name'], isset($rule['message']) ? strtr($rule['message'],
                                $params) : "Значение «{$attribute['label']}» не является правильным email адресом.");
                    }
                    break;
                case 'captcha':
                    global $APPLICATION;
                    if (!empty($attribute['value']) && !$APPLICATION->CaptchaCheckCode($this->getAttributeValue('captcha_word'),
                            $this->getAttributeValue('captcha_code'))
                    ) {
                        $this->addError('captcha_word',
                            isset($rule['message']) ? strtr($rule['message'], $params) : 'Неверный код проверки');
                    }
                    break;
                case 'callable':
                    if (!call_user_func($rule['function'], $this, $attribute)) {
                        $this->addError($attribute['name'], strtr($rule['message'], $params));
                    }
                    break;
            }
        }

        return count($this->errors) == 0;
    }

    /**
     * Загрузка данных из массива
     *
     * @param $attributes
     *
     * @return bool
     */
    public function load($attributes)
    {
        if (isset($attributes[$this->scope])) {
            foreach ($attributes[$this->scope] as $attribute => $value) {
                $this->attributes[$attribute]['value'] = $value;
            }

            return true;
        }

        return false;
    }

    /**
     * Проверить наличие атрибута
     *
     * @param $attribute
     *
     * @return bool
     */
    public function hasAttribute($attribute)
    {
        return isset($this->attributes[$attribute]);
    }

    /**
     * Метка атрибута
     *
     * @param $attribute
     *
     * @return mixed
     */
    public function getAttributeLabel($attribute)
    {
        return $this->attributes[$attribute]['label'];
    }

    /**
     * Установить метку атрибута
     *
     * @param $attribute
     * @param $label
     */
    public function setAttributeLabel($attribute, $label)
    {
        $this->attributes[$attribute]['label'] = $label;
    }

    /**
     * Установить значение атрибута
     *
     * @param $attribute
     * @param $value
     */
    public function setAttributeValue($attribute, $value)
    {
        $this->attributes[$attribute]['value'] = $value;
    }

    /**
     * Установить значение атрибутов
     *
     * @param $attributes
     */
    public function setAttributeValues($attributes)
    {
        foreach ($attributes as $attribute => $value) {
            if ($this->hasAttribute($attribute)) {
                $this->setAttributeValue($attribute, $value);
            }
        }
    }

    /**
     * Получить значение атрибута
     *
     * @param $attribute
     *
     * @return mixed
     */
    public function getAttributeValue($attribute)
    {
        return $this->attributes[$attribute]['value'];
    }

    /**
     * Получить значение атрибутов
     * @return array
     */
    public function getAttributeValues()
    {
        $values = array();
        foreach ($this->attributes as $code => $attribute) {
            $values[$code] = $this->getAttributeValue($code);
        }

        return $values;
    }

    /**
     * Прикрепить ошибку атрибуту
     *
     * @param $attribute
     * @param $message
     */
    public function addError($attribute, $message)
    {
        $this->errors[$attribute][] = $message;
    }

    /**
     * Получить список первых ошибок
     * @return array
     */
    public function getFirstErrors()
    {
        $result = array();
        foreach ($this->errors as $attribute => $errors) {
            $result[$attribute] = current($errors);
        }

        return $result;
    }

    /**
     * Получить первую ошибку атрибута
     *
     * @param $attribute
     *
     * @return bool|mixed
     */
    public function getFirstError($attribute)
    {
        return isset($this->errors[$attribute]) ? current($this->errors[$attribute]) : false;
    }

    /**
     * Проверить существование ошибок у модели или атрибута
     *
     * @param bool $attribute
     *
     * @return bool
     */
    public function hasErrors($attribute = false)
    {
        if ($attribute) {
            return count($this->errors[$attribute]) > 0;
        } else {
            return count($this->errors) > 0;
        }
    }

    /**
     * Добавить атрибут
     *
     * @param       $name
     * @param       $label
     * @param array $validators
     */
    public function addAttribute($name, $label, $validators = array())
    {
        $this->attributes[$name]['name'] = $name;
        $this->attributes[$name]['label'] = $label;
        foreach ($validators as $validator) {
            $this->addValidateRule($validator[0], $name, $validator);
        }
    }

    /**
     * Добавить правило валидации
     *
     * @param       $validator
     * @param       $attribute
     * @param array $params
     */
    public function addValidateRule($validator, $attribute, $params = array())
    {
        $this->rules[] = array_merge(array($attribute, $validator), $params);
    }

    /**
     * Удалить правила валидации
     *
     * @param $attribute
     */
    public function deleteValidateRules($attribute)
    {
        $rules = $this->rules;

        foreach ($rules as $i => $rule) {
            if ($rule[0] == $attribute) {
                unset($this->rules[$i]);
            }
        }
    }

    /**
     * Получить правила валидации для атрибута
     *
     * @param $attribute
     *
     * @return array
     */
    public function getAttributeValidators($attribute)
    {
        $validators = array();
        foreach ($this->rules as $rule) {
            if ($rule[0] == $attribute) {
                $validators[] = $rule;
            }
        }

        return $validators;
    }

    /**
     * Проверить атрибут является обязательным или нет
     *
     * @param $attribute
     *
     * @return bool
     */
    public function isRequiredAttribute($attribute)
    {
        foreach ($this->getAttributeValidators($attribute) as $rule) {
            if ($rule[1] == 'required') {
                return true;
            }
        }

        return false;
    }

    public function __get($name)
    {
        if ($this->hasAttribute($name)) {
            return $this->getAttributeValue($name);
        }

        return null;
    }

    public function __set($name, $value)
    {
        if ($this->hasAttribute($name)) {
            $this->setAttributeValue($name, $value);
        }
    }

    public function __isset($name)
    {
        try {
            return $this->__get($name) !== null;
        } catch (\Exception $e) {
            return false;
        }
    }

    public function __unset($name)
    {
        if ($this->hasAttribute($name)) {
            unset($this->attributes[$name]['value']);
        }
    }
} 