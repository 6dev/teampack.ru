<?php
/**
 * @author Alexandr Kirshin <kirshin.as@gmail.com>
 */

namespace seoexp\data;


use seoexp\helpers\ArrayHelper;
use CModule;
use CIBlock;
use CIBlockProperty;
use CIBlockElement;

/**
 * Class Element
 * @package seoexp\data
 *
 * @property integer $id Ид элемента
 * @property integer $iblock Ид инфоблока
 * @property bool $isNew Это новая запись
 * @property integer $iblock_section Привязка к разделам
 * @property string $active Активность элемента
 * @property string $name Название элемента
 * @property string $active_from Дата начала активности
 * @property string $active_to Дата окончания активности
 * @property integer $sort Сортировка
 * @property string $preview_text Описание для анонса
 * @property string $detail_text  Описание для детального просмотра
 * @property integer $preview_picture Картинка для анонса
 * @property integer $detail_picture  Картинка для детального просмотра
 * @property string $code Код
 * @property string $xml_id Внешний код
 * @property string $tmp_id Временный код
 */
class Element extends Model
{

    public $iblock;
    public $id;
    public $isNew = true;

    private static $_schemes = array();

    public function __construct($iblock)
    {
        $this->iblock = $iblock;
        $this->scope = 's' . $this->iblock;
        parent::__construct($this->scope);
    }


    protected function init()
    {
        CModule::IncludeModule('iblock');
        foreach ($this->getSchema($this->iblock) as $code => $field) {
            $this->addAttribute($code, $field['NAME']);
            if (isset($field['DEFAULT_VALUE'])) {
                $this->setAttributeValue($code, $field['DEFAULT_VALUE']);
            }
            if ($field['IS_REQUIRED'] == 'Y') {
                $this->addValidateRule('required', $code);
            }
            if ($field['USER_TYPE'] == 'DateTime') {
                $this->addValidateRule('datetime', $code);
            }
        }
        parent::init();
    }

    /**
     * Получить схему инфоблока
     * @param $iblock
     * @return mixed
     */
    public static function getSchema($iblock)
    {
        if (!isset(static::$_schemes[$iblock])) {
            $schema = array();
            $arFields = CIBlock::GetFields($iblock);
            foreach ($arFields as $code => $field) {
                if (strpos($code, 'SECTION_') === 0) {
                    continue;
                }
                $field['CODE'] = $code;
                $code = strtolower($code);
                $schema[$code] = $field;
            }
            $resProperties = CIBlockProperty::GetList(array('SORT' => 'ASC'), array('IBLOCK_ID' => $iblock, 'ACTIVE' => 'Y'));
            while ($arProperty = $resProperties->GetNext()) {
                $code = strtolower('PROPERTY_' . $arProperty['CODE']);
                $arProperty['IS_PROPERTY'] = true;
                $schema[$code] = $arProperty;
            }
            static::$_schemes[$iblock] = $schema;
        }

        return static::$_schemes[$iblock];
    }


    /**
     * Сохранить элемент
     * @return bool
     */
    public function save()
    {
        if ($this->validate()) {

            $el = new CIBlockElement();
            $schema = static::getSchema($this->iblock);
            $arFields = array('IBLOCK_ID' => $this->iblock);
            $files = ArrayHelper::normalizeFileArray($_FILES);
            foreach ($this->attributes as $code => $attribute) {
                $field = $schema[$code];
                if ($field['IS_PROPERTY']) {
                    $arFields['PROPERTY_VALUES'][$field['ID']] = $attribute['value'];
                } else {
                    if ($field['CODE'] == 'DETAIL_PICTURE' || $field['CODE'] == 'PREVIEW_PICTURE') {
                        if ($files[$this->scope][$code]) {
                            $arFields[$field['CODE']] = $files[$this->scope][$code];
                        }
                    } else {

                        $arFields[$field['CODE']] = $attribute['value'];

                    }
                }
            }
            if ($this->isNew) {
                if (!($this->id = $el->Add($arFields))) {
                    $this->addError('global', $el->LAST_ERROR);

                    return false;
                }
            } else {
                $properties = $arFields['PROPERTY_VALUES'];
                unset($arFields['PROPERTY_VALUES']);
                if (!$el->Update($this->id, $arFields)) {
                    $this->addError('global', $el->LAST_ERROR);

                    return false;
                }
                $el->SetPropertyValuesEx($this->id, $this->iblock, $properties);
                if ($el->LAST_ERROR) {
                    $this->addError('global', $el->LAST_ERROR);

                    return false;
                }
            }

            return true;
        }

        return false;
    }


    /**
     * Получить элемент инфоблока
     * @param $condition
     * @return bool|Element
     */
    public static function findOne($condition)
    {
        $where = $condition;
        if (!is_array($condition)) {
            $where = array('ID' => $condition);
        }
        if ($obElement = CIBlockElement::GetList(array(), $where)->GetNextElement()) {
            $arElement = $obElement->GetFields();
            $arElement['PROPERTIES'] = $obElement->GetProperties();
            $element = new Element($arElement['IBLOCK_ID']);
            foreach ($arElement as $key => $value) {
                if ($key != 'PROPERTIES' && $element->hasAttribute(strtolower($key))) {
                    $element->setAttributeValue(strtolower($key), htmlspecialchars_decode($value));
                }
            }
            foreach ($arElement['PROPERTIES'] as $key => $property) {
                if ($element->hasAttribute('property_' . strtolower($key))) {
                    if ($property['PROPERTY_TYPE'] == 'L') {
                        $element->setAttributeValue('property_' . strtolower($key), $property['VALUE_ENUM_ID']);
                    } else {
                        $element->setAttributeValue('property_' . strtolower($key), $property['VALUE']);
                    }
                }
            }
            $element->id = $arElement['ID'];
            $element->isNew = false;

            return $element;
        }

        return false;
    }
} 