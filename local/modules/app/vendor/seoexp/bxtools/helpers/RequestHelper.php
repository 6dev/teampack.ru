<?php
/**
 * @author Alexandr Kirshin <kirshin.as@gmail.com>
 */

namespace seoexp\helpers;

/**
 * @author Kirshin Alexandr <kirshin.as@gmail.com>
 *
 * Date: 13.12.13
 * Time: 10:04
 */
class RequestHelper
{

    /**
     * Проверить текущий запрос является он Ajax запросом
     * @return bool
     */
    public static function isAjax()
    {
        return (isset($_SERVER['HTTP_X_REQUESTED_WITH']) &&
            ($_SERVER['HTTP_X_REQUESTED_WITH'] == 'XMLHttpRequest'));
    }

}